<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Login/Logout Routes
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::group(['middleware' => ['auth']], function () {
    /*
     * Main
     */
    Route::get('/', 'PagesController@dashboard');
    Route::get('dashboard', 'PagesController@dashboard')->name('dashboard');

    /*
     * Users
     */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/data', 'UsersController@anyData')->name('users.data');
        Route::get('/taskdata/{id}', 'UsersController@taskData')->name('users.taskdata');
        Route::get('/leaddata/{id}', 'UsersController@leadData')->name('users.leaddata');
        Route::get('/clientdata/{id}', 'UsersController@clientData')->name('users.clientdata');
        Route::get('/users', 'UsersController@users')->name('users.users');
        
    });
    Route::resource('users', 'UsersController', ['middleware' => 'App\Http\Middleware\User\CanUserRead' ]);
    Route::get('/unactive_user/{id}','UsersController@unactive_user');
    Route::get('/active_user/{id}','UsersController@active_user');
    Route::get('/users/{id}/delete','UsersController@destroy');

    /*
    * Roles
    */
    Route::resource('roles', 'RolesController');

    /*
     * Clients
     */
    Route::group(['prefix' => 'clients'], function () {
        Route::get('/data', 'ClientsController@anyData')->name('clients.data');
        Route::get('/my', 'ClientsController@my')->name('clients.my');
        Route::get('/mydata', 'ClientsController@myData')->name('clients.mydata');
        Route::post('/create/cvrapi', 'ClientsController@cvrapiStart');
        Route::post('/upload/{id}', 'DocumentsController@upload');
        Route::patch('/updateassign/{id}', 'ClientsController@updateAssign');
        Route::get('masseClient', 'MasseClientsController@masseClient');

        Route::post('importExcel', 'MasseClientsController@importExcel');
        Route::get('masseUpdate', 'ClientsController@masseUpdate');
        Route::post('updateMasse', 'ClientsController@updateMasse');

        

    });
    Route::resource('clients', 'ClientsController', ['middleware' => 'App\Http\Middleware\Client\CanClientRead' ]);
    Route::resource('documents', 'DocumentsController');

    /*
     * Contacts
     */
    Route::group(['prefix' => 'contacts'], function () {
    Route::get('/data', 'ContactsController@anyData')->name('contacts.data');
    Route::get('/my', 'ContactsController@my')->name('contacts.my');
    Route::get('/mydata', 'ContactsController@myData')->name('contacts.mydata');
    Route::get('masseEquipement', 'MasseEquipementsController@masseEquipement');
    Route::get('teaser', 'ContactsController@teaser');

    Route::post('importExcelme', 'MasseEquipementsController@importExcelme');
    Route::post('exportExcelme', 'MasseEquipementsController@getExport');
    
    Route::get('masseUpdate', 'MasseEquipementsController@masseUpdate');
    Route::post('updateMasse', 'MasseEquipementsController@updateMasse');
    
    //V2
    Route::get('/ajouter', 'V2Controller@create')->name('equipement.abonn');
    Route::post('/ajouter', 'V2Controller@store')->name('equipement.abonnement');
    Route::post('/valider/{id}', 'V2Controller@valider')->name('valider');
    Route::post('/refuser/{id}', 'V2Controller@refuser')->name('refuser');
    Route::get('/equipement/{id}/edit', 'V2Controller@edit')->name('equipement.edit');
    Route::patch('/equipement/{id}', 'V2Controller@update')->name('equipement.update');
    Route::get('/filter', 'V2Controller@filter')->name('filter');
    Route::get('/filterfac', 'V2Controller@filterfac')->name('filterfac');
    Route::get('/facturation', 'V2Controller@facturation')->name('facturation');
    Route::post('/destroyabo/{id}', 'V2Controller@destroyabo')->name('destroy.abo');
    /* Route::get('/autocomplete', 'V2Controller@autocomplete')->name('autocomplete'); */





    });
    Route::resource('contacts', 'ContactsController', ['middleware' => 'App\Http\Middleware\Contact\CanContactRead' ]);

    /*
     * Tasks
     */
    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/data', 'TasksController@anyData')->name('tasks.data');
        Route::get('/my', 'TasksController@my')->name('tasks.my');
        Route::get('/mydata', 'TasksController@myData')->name('tasks.mydata');
        Route::patch('/updatestatus/{id}', 'TasksController@updateStatus');
        Route::patch('/updateassign/{id}', 'TasksController@updateAssign');
        Route::post('/updatetime/{id}', 'TasksController@updateTime');
    });
    Route::resource('tasks', 'TasksController');

    /*
     * Leads
     */
    Route::group(['prefix' => 'leads'], function () {
        Route::get('/data', 'LeadsController@anyData')->name('leads.data');
        Route::get('/my', 'LeadsController@my')->name('leads.my');
        Route::get('/mydata', 'LeadsController@myData')->name('leads.mydata');
        Route::patch('/updateassign/{id}', 'LeadsController@updateAssign');
        Route::patch('/updatestatus/{id}', 'LeadsController@updateStatus');
        Route::patch('/updatefollowup/{id}', 'LeadsController@updateFollowup')->name('leads.followup');
    });
    Route::resource('leads', 'LeadsController');
    Route::post('/comments/{type}/{id}', 'CommentController@store');
    /*
     * Settings
     */
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'SettingsController@index')->name('settings.index');
        Route::patch('/permissionsUpdate', 'SettingsController@permissionsUpdate');
        Route::patch('/overall', 'SettingsController@updateOverall');
    });

    /*
     * Departments
     */
    Route::resource('departments', 'DepartmentsController');

    /*
     * Pays
     */
     Route::resource('pays', 'PaysController');
     Route::resource('chargeds', 'ChargedController');

    /*
     * Abonnements
     */
     Route::resource('abonnements', 'AbonnementsController', ['middleware' => 'App\Http\Middleware\Abonnement\CanAbonnementRead' ]);
     Route::get('/unactive_abonnement/{id}','AbonnementsController@unactive_abonnement');
     Route::get('/active_abonnement/{id}','AbonnementsController@active_abonnement');
     Route::get('/exporte','AbonnementsController@exporte');

    /*
     * Boitiers
     */
     Route::resource('boitiers', 'BoitiersController' , ['middleware' => 'App\Http\Middleware\Boitier\CanBoitierRead' ]);
     Route::get('/unactive_boitier/{id}','BoitiersController@unactive_boitier');
     Route::get('/active_boitier/{id}','BoitiersController@active_boitier');




    /*
     * Integrations
     */
    Route::group(['prefix' => 'integrations'], function () {
        Route::get('Integration/slack', 'IntegrationsController@slack');
    });
    Route::resource('integrations', 'IntegrationsController');

    /*
     * Notifications
     */
    Route::group(['prefix' => 'notifications'], function () {
        Route::post('/markread', 'NotificationsController@markRead')->name('notification.read');
        Route::get('/markall', 'NotificationsController@markAll');
        Route::get('/{id}', 'NotificationsController@markRead');
    });

    /*
     * Invoices
     */
    Route::group(['prefix' => 'invoices'], function () {
        Route::post('/updatepayment/{id}', 'InvoicesController@updatePayment')->name('invoice.payment.date');
        Route::post('/reopenpayment/{id}', 'InvoicesController@reopenPayment')->name('invoice.payment.reopen');
        Route::post('/sentinvoice/{id}', 'InvoicesController@updateSentStatus')->name('invoice.sent');
        Route::post('/newitem/{id}', 'InvoicesController@newItem')->name('invoice.new.item');
    });
    Route::resource('invoices', 'InvoicesController');
});
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

    /*
    * Excel
    */
    // Route::get('importExport', 'MaatwebsiteDemoController@importExport');
    // Route::get('downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');
    // Route::post('importExcel', 'MaatwebsiteDemoController@importExcel');

    /*
    * Masse CLIENT
    */
    
   

    /*
    * Masse Equipement
    */
    
    