-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 02 déc. 2021 à 18:07
-- Version du serveur :  10.3.31-MariaDB-cll-lve
-- Version de PHP : 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `c1685999c_macc`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnements`
--

CREATE TABLE `abonnements` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `interface1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `interface2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interface3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tarif` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statut` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `abonnements`
--

INSERT INTO `abonnements` (`id`, `nom`, `type`, `interface1`, `interface2`, `interface3`, `tarif`, `statut`, `created_at`, `updated_at`) VALUES
(1, 'Select Subscription', 'null', 'null', NULL, NULL, '0', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(2, 'Starter', 'Permanent', 'null', '', '', '12000', 1, '2019-06-04 11:42:19', '2021-09-09 06:47:05'),
(3, 'Inform', 'null', 'null', NULL, NULL, '0', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(4, 'Inform+', 'null', 'null', NULL, NULL, '15000', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(5, 'New subcription1', 'null', 'null', NULL, NULL, '20000', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(6, 'New subcription2', 'null', 'null', NULL, NULL, '25000', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(7, 'New subcription3', 'null', 'null', NULL, NULL, '0', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(8, 'New subcription 4', 'Permanent', 'null', '', '', '30000', 0, '2019-06-04 11:42:19', '2021-09-24 14:56:37'),
(9, 'New subcription 5', 'null', 'null', NULL, NULL, '0', 0, '2019-06-04 11:42:19', '2019-09-04 11:42:19'),
(10, 'testes', 'Permanent', '23467876543', '', '', '600000', 0, '2021-09-08 12:56:07', '2021-09-16 07:39:40'),
(11, 'TESTER', 'Permanent', '234678765', '', '', '600000', 1, '2021-09-09 06:47:45', '2021-09-09 06:47:45'),
(12, 'Signature bc', 'Permanent', '23467876543', '', '', '200000', 1, '2021-09-15 14:49:35', '2021-09-15 14:49:58'),
(13, 'Fournisseu Reporting', 'Promotionnel', '098736577879', '', '', '0', 0, '2021-09-16 07:40:25', '2021-09-16 07:40:25'),
(14, 'Fournisseu Reportings', 'Promotionnel', '234678765', '12', '43', '12000', 0, '2021-09-20 06:37:11', '2021-09-21 20:57:28'),
(15, 'Command Fournisseur', 'Promotionnel', '098786748', '', '', '500000', 1, '2021-09-23 08:14:09', '2021-09-23 08:14:29'),
(16, 'ndioufa ndiaye', 'Promotionnel', '0987654565789', '', '', '10000', 0, '2021-09-28 07:20:11', '2021-09-28 07:22:20'),
(17, 'Abonnement 2', 'Permanent', 'MCC', 'VL', '', '1000', 0, '2021-10-29 09:52:15', '2021-10-29 09:52:36');

-- --------------------------------------------------------

--
-- Structure de la table `abo_equipements`
--

CREATE TABLE `abo_equipements` (
  `id` int(10) UNSIGNED NOT NULL,
  `chargerto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_statut` tinyint(1) NOT NULL DEFAULT 0,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `abonnement_id` int(11) DEFAULT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `charged_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `abo_equipements`
--

INSERT INTO `abo_equipements` (`id`, `chargerto`, `end_statut`, `date_debut`, `date_fin`, `contact_id`, `client_id`, `abonnement_id`, `pays_id`, `charged_id`, `created_at`, `updated_at`) VALUES
(3, 'ndioufa ndiaye', 1, '2021-10-30', '2021-10-31', 2, 1, 2, 2, NULL, '2021-09-16 00:55:40', '2021-09-16 00:55:41'),
(4, 'ndioufa ndiaye', 1, '2021-09-15', '2021-09-18', 3, 4, 11, 2, NULL, '2021-09-16 06:22:32', '2021-09-16 06:22:32'),
(6, '', 1, '0000-00-00', '0000-00-00', 6, 4, 11, 2, NULL, '2021-09-16 06:47:37', '2021-09-16 06:47:37'),
(7, 'ndioufa', 1, '2021-09-15', '2021-09-24', 8, 4, 13, 2, 15, '2021-09-16 07:47:16', '2021-09-16 07:47:16'),
(11, 'ndioufa', 1, '2021-09-17', '2021-10-01', 13, 4, 10, 1, 15, '2021-09-16 08:44:49', '2021-09-16 08:44:49'),
(12, 'ndioufa', 1, '2021-09-14', '2021-09-30', 14, 4, 10, 1, 15, '2021-09-16 09:02:37', '2021-09-16 09:02:37'),
(14, 'ndioufa', 1, '2021-09-28', '2021-10-05', 16, 1, 11, NULL, 15, '2021-09-20 06:49:27', '2021-09-20 06:49:27'),
(16, 'codou', 1, '2021-10-05', '2021-10-10', 18, 7, 14, 1, NULL, '2021-09-20 07:05:45', '2021-09-20 07:05:45'),
(17, 'codou', 1, '2021-09-14', '2021-10-01', 19, 7, 14, 3, NULL, '2021-09-20 07:17:50', '2021-09-20 07:17:50'),
(20, 'ndioufa', 1, '2021-09-15', '2021-09-25', 11, 4, 2, 2, 15, '2021-09-21 07:24:49', '2021-09-16 08:13:29'),
(21, 'codou', 1, '2021-09-21', '2021-09-30', 25, 8, 9, 2, NULL, '2021-09-21 07:40:26', '2021-09-21 07:40:26'),
(24, 'codou', 1, '2021-09-22', '2021-09-30', 24, 7, 14, 2, NULL, '2021-09-21 08:47:33', '2021-09-21 20:47:33'),
(25, 'Anna Ba', 1, '2021-09-23', '2021-10-01', 26, 15, 13, 1, NULL, '2021-09-23 08:27:16', '2021-09-23 08:27:16'),
(26, 'ndioufa', 1, '2021-10-07', '2021-10-10', 26, 15, 15, 1, 15, '2021-09-23 08:27:16', '2021-09-23 08:27:16'),
(27, 'ndioufa', 1, '2021-09-22', '2021-09-30', 27, 15, 12, 3, 15, '2021-09-23 08:32:17', '2021-09-23 08:32:17'),
(28, 'Anna Ba', 1, '2021-09-20', '2021-09-24', 27, 15, 15, 3, NULL, '2021-09-23 08:32:17', '2021-09-23 08:32:17'),
(33, 'ndioufa', 1, '2021-09-22', '2021-09-25', 28, 12, 8, 2, 15, '2021-09-23 08:39:30', '2021-09-23 08:39:30'),
(34, 'Anna Ba', 1, '2021-09-20', '2021-09-23', 30, 8, 7, 2, NULL, '2021-09-23 02:37:15', '2021-09-23 14:37:15'),
(35, 'Anna Ba', 1, '2021-08-09', '2021-09-23', 31, 15, 7, 3, NULL, '2021-09-23 02:41:35', '2021-09-23 14:41:35'),
(36, 'Anna Ba', 1, '2021-09-01', '2021-09-21', 32, 12, 5, 3, NULL, '2021-09-23 02:49:21', '2021-09-23 14:49:21'),
(37, 'ndioufa', 1, '2021-10-01', '2021-10-10', 33, 19, 2, 1, 15, '2021-09-23 08:07:44', '2021-09-23 20:07:44'),
(38, 'ndioufa', 1, '2021-10-11', '2021-10-23', 33, 19, 3, 1, 15, '2021-09-23 08:07:44', '2021-09-23 20:07:44'),
(39, 'ndioufa', 1, '0000-00-00', '0000-00-00', 34, 17, 2, 2, 15, '2021-09-23 08:09:46', '2021-09-23 20:09:46'),
(44, 'ndioufa', 1, '2021-09-23', '2021-09-25', 35, 17, 2, 2, 15, '2021-09-23 08:13:25', '2021-09-23 20:10:56'),
(45, 'ndioufa', 1, '2021-09-26', '2021-09-29', 35, 17, 4, 2, 15, '2021-09-23 08:13:25', '2021-09-23 20:10:56'),
(48, 'ndioufa', 1, '2021-09-15', '2021-09-25', 41, 15, 5, 2, 15, '2021-09-24 01:31:35', '2021-09-24 13:31:35'),
(50, 'Anna Ba', 1, '2021-09-14', '2021-10-02', 42, 19, 4, 2, NULL, '2021-09-24 02:07:32', '2021-09-24 14:07:32'),
(51, 'ndioufa', 1, '2021-09-22', '2021-09-29', 44, 12, 14, 2, 15, '2021-09-27 06:53:44', '2021-09-27 18:53:44'),
(52, 'ndioufa', 1, '2021-09-29', '2021-09-28', 44, 12, 13, 2, 15, '2021-09-27 06:53:44', '2021-09-27 18:53:44'),
(62, 'ndioufa', 1, '2021-09-25', '2021-10-08', 50, 48, 2, 1, 15, '2021-09-29 08:15:52', '2021-09-29 08:15:52'),
(63, 'ndioufa', 1, '2021-10-09', '2021-10-22', 50, 48, 3, 1, 15, '2021-09-29 08:15:52', '2021-09-29 08:15:52'),
(66, 'ndioufa', 0, '2021-10-30', '2021-12-05', 56, 45, 12, NULL, 15, '2021-10-01 01:52:49', '2021-10-01 13:52:49'),
(67, 'ndioufa', 1, '2021-10-01', '2021-10-22', 57, 53, 15, 1, 15, '2021-10-01 02:12:17', '2021-10-01 14:12:17'),
(68, 'ndioufa', 0, '2021-10-31', '2021-11-27', 57, 53, 4, 1, 15, '2021-10-01 02:12:17', '2021-10-01 14:12:17'),
(69, 'ndioufa', 1, '2021-10-22', '2021-11-07', 59, 55, 2, NULL, 15, '2021-10-01 02:56:44', '2021-10-01 14:56:44'),
(70, 'ndioufa', 0, '2021-11-16', '2021-12-05', 59, 55, 3, NULL, 15, '2021-10-01 02:56:44', '2021-10-01 14:56:44'),
(80, 'ndioufa', 1, '2021-10-27', '2021-10-31', 63, 8, 7, NULL, 15, '2021-10-12 08:07:06', '2021-10-12 20:07:06'),
(81, 'ndioufa', 1, '2021-10-13', '2021-10-17', 64, 57, 3, NULL, 15, '2021-10-12 08:09:25', '2021-10-12 20:09:25'),
(83, 'ndioufa', 1, '2021-10-14', '2021-10-15', 67, 57, 3, 1, 15, '2021-10-13 09:53:27', '2021-10-13 09:53:27'),
(84, 'ndioufa', 1, '2021-10-13', '2021-10-31', 68, 4, 13, 1, 15, '2021-10-15 00:21:09', '2021-10-15 12:21:09'),
(85, 'ndioufa', 1, '2021-10-12', '2021-10-30', 69, 4, 16, 1, 15, '2021-10-15 00:28:19', '2021-10-15 12:28:19'),
(88, 'SDQ', 1, '2021-10-01', '2021-10-31', 73, 59, 17, NULL, 21, '2021-10-29 10:06:34', '2021-10-29 10:06:34'),
(90, 'ndioufa', 1, '2021-09-23', '2021-09-24', 49, 47, 2, 1, 15, '2021-10-29 10:10:57', '2021-10-29 10:10:57'),
(91, '', 1, '0000-00-00', '0000-00-00', 49, 47, 3, 1, NULL, '2021-10-29 10:10:57', '2021-10-29 10:10:57'),
(95, '', 1, '0000-00-00', '0000-00-00', 74, 1, 0, 1, NULL, '2021-10-31 03:51:05', '2021-10-31 03:51:05'),
(97, 'ndioufa', 0, '2021-12-01', '2021-12-31', 75, 46, 5, NULL, 15, '2021-11-01 09:06:06', '2021-11-01 21:06:06'),
(108, 'ndioufa', 1, '2021-10-01', '2021-10-31', 78, 7, 4, 7, 15, '2021-11-10 11:18:45', '2021-11-05 14:30:25'),
(109, 'ndioufa', 0, '2021-11-01', '2021-11-30', 78, 7, 6, 7, 15, '2021-11-10 11:18:45', '2021-11-05 14:30:25'),
(110, 'ndioufa', 0, '2021-12-01', '2021-12-31', 78, 7, 9, 7, 15, '2021-11-10 11:18:45', '2021-11-05 14:30:25'),
(111, 'ndioufa', 0, '2021-12-31', '2022-01-31', 78, 7, 4, 7, 15, '2021-11-10 11:18:45', '2021-11-05 14:30:25'),
(125, 'SDQ', 0, '2021-11-28', '2021-11-30', 76, 59, 17, 7, 21, '2021-11-13 07:35:19', '2021-11-05 14:27:12'),
(126, 'SDQ', 0, '2021-11-15', '2021-11-27', 76, 59, 4, 7, 21, '2021-11-13 07:35:19', '2021-11-05 14:27:12'),
(127, 'SDQ', 0, '2021-12-04', '2021-12-11', 76, 59, 5, 7, 21, '2021-11-13 07:35:19', '2021-11-05 14:27:12'),
(128, 'ndioufa', 0, '2021-12-12', '2021-12-19', 76, 59, 4, 7, 15, '2021-11-13 07:35:19', '2021-11-05 14:27:12'),
(129, 'SDQ', 0, '2021-11-29', '2021-12-05', 76, 59, 4, 7, 21, '2021-11-13 07:35:19', '2021-11-05 14:27:12'),
(131, 'ndioufa ndiaye', 1, '2021-09-13', '2021-09-17', 4, 1, 11, 3, NULL, '2021-11-13 07:52:51', '2021-09-16 06:43:44'),
(132, 'SDQ', 0, '2021-11-22', '2021-12-11', 4, 1, 5, 3, 21, '2021-11-13 07:52:51', '2021-09-16 06:43:44'),
(133, 'SDQ', 0, '2026-02-28', '2027-06-17', 1, 1, 2, 1, 21, '2021-11-13 07:54:39', '2021-10-09 13:03:32'),
(134, 'SDQ', 1, '2021-07-30', '2021-10-31', 1, 1, 3, 1, 21, '2021-11-13 07:54:39', '2021-10-09 13:03:32'),
(135, 'SDQ', 1, '2021-10-01', '2021-10-31', 1, 1, 17, 1, 21, '2021-11-13 07:54:39', '2021-10-09 13:03:32'),
(136, 'ndioufa', 0, '2027-03-28', '2027-04-28', 1, 1, 5, 1, 15, '2021-11-13 07:54:39', '2021-10-09 13:03:32'),
(139, 'SDQ', 0, '2021-11-19', '2021-11-27', 77, 7, 3, 7, 21, '2021-11-15 00:54:29', '2021-11-05 14:28:31'),
(140, 'ndioufa', 0, '2021-12-00', '2021-12-05', 77, 7, 7, 7, 15, '2021-11-15 00:54:29', '2021-11-05 14:28:31'),
(141, 'ndioufa', 0, '2021-12-09', '2021-12-12', 77, 7, 8, 7, 15, '2021-11-15 00:54:29', '2021-11-05 14:28:31'),
(142, 'ndioufa', 1, '2021-09-16', '2021-09-26', 12, 4, 2, 1, 15, '2021-11-22 01:07:23', '2021-09-16 08:38:32'),
(143, 'SDQ', 0, '2021-11-01', '2021-11-30', 12, 4, 5, 1, 21, '2021-11-22 01:07:23', '2021-09-16 08:38:32');

-- --------------------------------------------------------

--
-- Structure de la table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `boitiers`
--

CREATE TABLE `boitiers` (
  `id` int(10) UNSIGNED NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `communication_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statut` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `boitiers`
--

INSERT INTO `boitiers` (`id`, `reference`, `communication_type`, `statut`, `created_at`, `updated_at`) VALUES
(1, 'Select PL', 'null', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(2, 'PLE641', 'Sm', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(3, 'PLE6312', 'Sm', 0, '2016-06-04 11:42:19', '2021-09-09 06:46:07'),
(4, 'PLE601', 'SM', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(5, 'PLE641+PL631', 'SM', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(6, 'New PL1', 'SM', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(7, 'New PL2', 'SM', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(8, 'New PL3', 'SM', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(9, 'New PL4', 'SM', 0, '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(10, 'teste', 'tester', 0, '2021-09-08 12:54:56', '2021-09-08 12:55:09'),
(11, 'PLE612', 'tester', 1, '2021-09-09 06:46:29', '2021-09-09 06:46:29'),
(12, 'tester12', 'tester', 0, '2021-09-15 14:48:15', '2021-09-15 14:48:38'),
(13, 'test', 'tester', 0, '2021-09-16 07:38:24', '2021-09-16 07:38:56'),
(14, 'tester2309', 'tester23', 1, '2021-09-23 08:12:54', '2021-09-23 08:13:15'),
(15, 'PLO687', 'FEAUHZIS', 1, '2021-09-28 07:26:44', '2021-09-28 07:27:35'),
(16, 'PL800', 'WIFI', 1, '2021-10-29 09:54:34', '2021-10-29 09:54:53');

-- --------------------------------------------------------

--
-- Structure de la table `chargeds`
--

CREATE TABLE `chargeds` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `chargeds`
--

INSERT INTO `chargeds` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'No', '2021-08-07 20:36:42', '2021-09-09 00:13:32'),
(15, 'ndioufa', '2021-09-20 06:31:29', '2021-09-20 06:31:29'),
(21, 'SDQ', '2021-10-29 10:02:16', '2021-10-29 10:03:01'),
(22, 'pas', '2021-11-02 17:07:06', '2021-11-02 17:07:06');

-- --------------------------------------------------------

--
-- Structure de la table `charged_to`
--

CREATE TABLE `charged_to` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `pay_id` int(11) DEFAULT NULL,
  `business` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numidint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numidext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numidext2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `pay_id`, `business`, `primary_email`, `numidint`, `numidext`, `numidext2`, `primary_number`, `secondary_number`, `name`, `industry`, `billing_address1`, `billing_address2`, `billing_zipcode`, `billing_country`, `shipping_address1`, `shipping_address2`, `shipping_city`, `shipping_state`, `shipping_zipcode`, `shipping_country`, `billing_city`, `billing_state`, `vat`, `pays`, `company_type`, `industry_id`, `created_at`, `updated_at`) VALUES
(1, 2, '332244455', 'you@gmail.com', '0997364', '6555567', '555434', NULL, NULL, 'You and me', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-15 13:33:04', '2021-09-29 08:02:45'),
(4, 1, 'test', 'ndioufa@gmail.com', '7860584004', '467487', '987676', NULL, NULL, 'ndioufa ndiaye', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-15 14:18:32', '2021-09-16 06:32:15'),
(7, 3, 'test', 'dou@gmail.com', '098765432', '098765432443', '4567890', NULL, NULL, 'DOUDOU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-20 06:57:11', '2021-09-21 07:31:49'),
(8, 2, 'vfdh', 'test@test.com', '123', '321', '213', NULL, NULL, 'sococim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-20 09:58:12', '2021-09-20 12:47:25'),
(12, 1, 'test', 'adji@gmail.com', '097678999', '210987777', '123457667', NULL, NULL, 'adjis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-21 07:59:02', '2021-09-21 20:51:13'),
(15, 3, 'test', 'doudou@gmail.com', '0987574789098', '67854878784', '9654587384986', NULL, NULL, 'doudou diop', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-23 08:16:08', '2021-09-23 08:17:24'),
(17, 1, 'tester', 'tesfft@test.comdd', '2111889', '2229321', '22229993', NULL, NULL, 'Lindor2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-23 19:32:39', '2021-09-24 14:55:55'),
(19, 1, 'ddddds', 'tesfft@test.comdds', '1222119', '3319321', '2229022', NULL, NULL, 'Lindor2ab', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-23 19:42:35', '2021-09-28 07:17:01'),
(40, 3, 'vfdh', 'tafas@wintech.sn', '45889', '878321', '5213', NULL, NULL, 'tene', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 07:59:06', '2021-10-09 12:35:58'),
(45, 2, 'vfdh', 'fas@wintech,sn', '4589', '8321', '5213', NULL, NULL, 'tata', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 08:02:38', '2021-10-09 12:35:40'),
(46, 1, 'vfdh', 'taf@wintech,sn', '4889', '78321', '5213', NULL, NULL, 'dada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 08:02:38', '2021-09-28 08:02:38'),
(47, 1, 'vfdh', 'tafa@wintech,sn', '44945889', '444889', '8485213', NULL, NULL, 'tenetest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-29 07:59:41', '2021-09-29 07:59:41'),
(48, 1, 'vfdh', 'tafa@wintechddddd.sn', '3', '333', '38485213', NULL, NULL, 'tenetest1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-29 08:12:20', '2021-09-29 08:12:20'),
(50, 1, 'vfdh', 'Hhhdfj@kj.co', '38833', '44444', '444444', NULL, NULL, 'tenetest2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-29 08:22:26', '2021-09-29 08:22:26'),
(53, 2, 'vfdh', 'testafa@wintech,sn', '22022', '8722321', '5213', NULL, NULL, 'Manioukkkk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-01 13:25:50', '2021-10-01 13:25:50'),
(55, 2, 'azerty', 'next@next.next', '2208832', '111100008', '52213', NULL, NULL, 'next', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-01 14:51:58', '2021-10-01 14:51:58'),
(57, 1, 'vfdh', 'tafa@wintech,snD', '15889', '77878321', '529913', NULL, NULL, 'Last', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-12 19:42:55', '2021-10-12 19:42:55'),
(59, 1, 'Mines', 'client@mail.com', '11', '33', '22', NULL, NULL, 'Client SDQ 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-29 09:49:24', '2021-10-29 09:49:24');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(10) UNSIGNED NOT NULL,
  `source_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `constructeur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `famille` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_serie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sous_famille` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modele` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `compteur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_mise_en_service` date NOT NULL DEFAULT current_timestamp(),
  `pays` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_number` date DEFAULT NULL,
  `job_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(10) UNSIGNED DEFAULT NULL,
  `boitier_id` int(10) UNSIGNED DEFAULT NULL,
  `abonnement_id` int(10) UNSIGNED DEFAULT NULL,
  `datedebut` date DEFAULT NULL,
  `datefin` date DEFAULT NULL,
  `chargerto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`id`, `constructeur`, `famille`, `numero_serie`, `sous_famille`, `segment`, `modele`, `compteur`, `date_mise_en_service`, `pays`, `pay_id`, `type`, `zipcode`, `secondary_number`, `city`, `primary_number`, `job_title`, `name`, `address1`, `address2`, `state`, `country`, `client_id`, `boitier_id`, `abonnement_id`, `datedebut`, `datefin`, `chargerto`, `created_at`, `updated_at`) VALUES
(1, 'f', 'g', '78', 's', NULL, 'm', '', '2021-10-30', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, NULL, NULL, NULL, NULL, '2021-09-15 23:56:27', '2021-10-09 13:03:32'),
(2, 'h', 'fg', '98', 'yui', NULL, 'mo', '', '2021-09-30', NULL, 2, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, 2, NULL, NULL, NULL, '2021-09-16 00:55:41', '2021-09-16 00:55:41'),
(3, 'TEST', 'TEST', '3456789', 'TEST', NULL, 'TEST', '', '2021-09-15', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 12, 11, NULL, NULL, NULL, '2021-09-16 06:22:32', '2021-09-16 06:22:32'),
(4, 'TEST', 'TEST', '345678934', 'TEST', NULL, 'TEST', '', '2021-09-16', NULL, 3, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12, NULL, NULL, NULL, NULL, '2021-09-16 06:43:44', '2021-09-16 06:43:44'),
(6, 'TEST', 'TEST', '345678912', 'TEST', NULL, 'TEST', '', '2021-09-17', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 12, 11, NULL, NULL, NULL, '2021-09-16 06:47:37', '2021-09-16 06:47:37'),
(8, 'TEST', 'TEST', '345678909', 'TEST', NULL, 'TEST', '', '2021-09-22', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 11, 13, NULL, NULL, NULL, '2021-09-16 07:47:16', '2021-09-16 07:47:16'),
(11, 'tester', 'tester', '09957464', 'tester', NULL, 'tester', '', '2021-09-23', NULL, 2, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 11, 2, NULL, NULL, NULL, '2021-09-16 08:13:29', '2021-09-16 08:13:29'),
(12, 'tester', 'TEST', '679950', 'TEST', NULL, 'TEST', '', '2021-09-18', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 12, 2, NULL, NULL, NULL, '2021-09-16 08:38:32', '2021-09-16 08:38:32'),
(13, 'TEST1', 'TEST1', '209876', 'TEST1', NULL, 'TEST1', '', '2021-10-01', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 10, 10, NULL, NULL, NULL, '2021-09-16 08:44:49', '2021-09-16 08:44:49'),
(14, 'TEST1', 'TEST1', '387908', 'TEST1', NULL, 'TEST1', '', '2021-09-19', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 10, 10, NULL, NULL, NULL, '2021-09-16 09:02:37', '2021-09-16 09:02:37'),
(18, 'TEST Mali', 'TEST Mali', '387908096', 'TEST Mali', NULL, 'TEST Mali', '', '2021-09-22', NULL, 1, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 12, 14, NULL, NULL, NULL, '2021-09-20 07:05:45', '2021-09-20 07:05:45'),
(19, 'TEST', 'TEST', '345678913', 'TEST', NULL, 'TEST', '', '2021-09-21', NULL, 3, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 13, 14, NULL, NULL, NULL, '2021-09-20 07:17:50', '2021-09-20 07:17:50'),
(20, 'tenebuilder1', 'tenelindor1', '12100022140', 'lindor', 'ten', 'or', NULL, '0000-00-00', 'sénégal', NULL, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, 3, NULL, NULL, NULL, '2021-09-20 10:47:18', '2021-09-20 10:47:18'),
(22, 'tenebuilder1', 'tenelindor1', '12100022', 'lindor', 'ten', 'or', NULL, '0000-00-00', 'sénégal', NULL, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 3, NULL, NULL, NULL, '2021-09-20 11:45:07', '2021-09-20 11:45:07'),
(24, 'TEST Mali', 'TEST Mali', '567890912', 'TEST Mali', NULL, 'TEST Malis', NULL, '2021-09-23', NULL, 2, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 6, 14, NULL, NULL, NULL, '2021-09-21 07:05:37', '2021-09-21 20:47:33'),
(25, 'TEST Mali', 'TEST Mali', '567890', 'TEST Mali', NULL, 'TEST Mali', NULL, '2021-09-23', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 2, 9, NULL, NULL, NULL, '2021-09-21 07:40:26', '2021-09-21 07:40:26'),
(26, 'TEST senegal', 'TEST senegal', '5678909123', 'TEST senegal', NULL, 'TEST senegal', NULL, '2021-09-23', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 14, 15, NULL, NULL, NULL, '2021-09-23 08:27:16', '2021-09-23 08:27:16'),
(27, 'LINDOR', 'LINDOR', '3456789433', 'LINDOR', NULL, 'LINDOR', NULL, '2021-09-30', NULL, 3, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 13, 15, NULL, NULL, NULL, '2021-09-23 08:32:17', '2021-09-23 08:32:17'),
(28, 'LINDOR3', 'LINDOR3', '34567896544', 'LINDOR3', NULL, 'LINDOR3', NULL, '2021-09-24', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 12, 8, NULL, NULL, NULL, '2021-09-23 08:37:30', '2021-09-23 08:39:30'),
(29, 'tenebuilder1', 'tenelindor1', '1210002', 'lindor', 'ten', 'or', NULL, '0000-00-00', 'Sénégal', NULL, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 4, 3, NULL, NULL, NULL, '2021-09-23 08:42:15', '2021-09-23 08:42:15'),
(30, 'Aujourd\'hui', 'Aujourd\'hui', 'Aujourd\'hui', 'Aujourd\'hui', NULL, 'Aujourd\'hui', NULL, '2021-09-22', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 6, 7, NULL, NULL, NULL, '2021-09-23 14:37:15', '2021-09-23 14:37:15'),
(31, 'Aujourd\'hui', 'Aujourd\'hui', '3456789233', 'Aujourd\'hui', NULL, 'Aujourd\'hui', NULL, '2021-09-15', NULL, 3, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 14, 7, NULL, NULL, NULL, '2021-09-23 14:41:35', '2021-09-23 14:41:35'),
(32, 'tester', 'tester', '567890932', 'tester', NULL, 'tester', NULL, '2021-09-21', NULL, 3, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 9, 5, NULL, NULL, NULL, '2021-09-23 14:49:21', '2021-09-23 14:49:21'),
(33, 'zzzzzzz', 'zzzzzz', '12239009993', 'zzzz', NULL, 'sfff333', NULL, '2021-09-15', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, 3, 3, NULL, NULL, NULL, '2021-09-23 20:07:44', '2021-09-23 20:07:44'),
(34, 'azdafkjn', 'qqsfqsfd', '190', 'qsfk', NULL, 'efze', NULL, '2021-09-30', NULL, 2, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, 2, 2, NULL, NULL, NULL, '2021-09-23 20:09:46', '2021-09-23 20:09:46'),
(35, 'djhfgdqjhf', 'sqfbhqhsfj', '12333333333', 'qsfb', NULL, 'Jhbhjnjk', NULL, '2021-09-24', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, 4, 4, NULL, NULL, NULL, '2021-09-23 20:10:56', '2021-09-23 20:10:56'),
(36, 'KJHD', 'KJHD', '12107770022140', 'KJHD', 'KJHD', 'KJHD', NULL, '0000-00-00', 'Sénégal', NULL, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 4, 3, NULL, NULL, NULL, '2021-09-23 21:22:29', '2021-09-23 21:22:29'),
(37, 'tenebuilder1', 'wintech', '121000220', 'winmacc', 'ten', 'or', NULL, '0000-00-00', 'sénégal', NULL, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 4, 3, NULL, NULL, NULL, '2021-09-23 21:31:55', '2021-09-23 21:31:55'),
(41, 'Aujourd\'hui', 'Aujourd\'hui', '56789091200', 'Aujourd\'hui', NULL, 'Aujourd\'hui', NULL, '2021-09-23', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 6, 5, NULL, NULL, NULL, '2021-09-24 13:31:35', '2021-09-24 13:31:35'),
(42, 'TEST senegal', 'TEST senegal', 'TEST senegal', 'TEST senegal', NULL, 'TEST senegal', NULL, '2021-09-30', NULL, 2, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, 14, 4, NULL, NULL, NULL, '2021-09-24 13:59:54', '2021-09-24 14:07:32'),
(44, 'TEST Mali', 'Aujourd\'hui', '5678909109', 'tester', NULL, 'ndioufa teste', NULL, '2021-09-21', NULL, 2, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 14, 13, NULL, NULL, NULL, '2021-09-27 18:53:44', '2021-09-27 18:53:44'),
(46, 'testen', 'tene', '22025', 'tafa', 'azerty', 'zeaa', NULL, '0000-00-00', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 2, NULL, NULL, NULL, NULL, '2021-09-28 06:12:13', '2021-09-28 06:12:13'),
(49, 'builderrr', 'lami', '1232234', 'r', NULL, 'DHJHJ', NULL, '2021-09-14', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 47, 16, 3, NULL, NULL, NULL, '2021-09-29 08:08:46', '2021-10-29 10:10:57'),
(50, 'builder', 'gr', '1232332344', 'sub', NULL, 'DHJHJr', NULL, '2021-09-16', NULL, 1, 'Moteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 3, 3, NULL, NULL, NULL, '2021-09-29 08:15:52', '2021-09-29 08:15:52'),
(51, 'testen', 'tene', '33980', 'tafa', 'azerty', 'zeaa', NULL, '0000-00-00', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 2, NULL, NULL, NULL, NULL, '2021-09-29 08:24:38', '2021-09-29 08:24:38'),
(52, 'tenebuilder1', 'Ttbuilder', '100017', 'zircon', 'ten', 'or', NULL, '0000-00-00', NULL, NULL, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 3, NULL, NULL, NULL, '2021-10-01 13:15:35', '2021-10-01 13:15:35'),
(53, 'tenebuilder1', 'Manioukkkk', '10002317', 'zircon', 'tens', 'or', NULL, '0000-00-00', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, 3, NULL, NULL, NULL, '2021-10-01 13:27:47', '2021-10-01 13:27:47'),
(54, 'tenebuilder1', 'testeur', '1010', 'zircon', 'tens', 'or', NULL, '0000-00-00', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 4, 3, NULL, NULL, NULL, '2021-10-01 13:35:39', '2021-10-01 13:35:39'),
(57, 'vvvv', 'vvvvv', '12387', 'vvvv', NULL, 'test', NULL, '2021-10-14', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 2, 4, NULL, NULL, NULL, '2021-10-01 14:12:17', '2021-10-01 14:12:17'),
(62, 'tenebuilder1', 'testeurs', '2312300', 'zircon', 'tens', 'or', NULL, '2021-10-01', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55, 4, 3, NULL, NULL, NULL, '2021-10-01 15:15:10', '2021-10-01 15:15:10'),
(65, 'tenebuilder1', 'testeuse', '11010', 'zircon', 'tens', 'or', NULL, '2021-12-13', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 4, 3, NULL, NULL, NULL, '2021-10-13 08:30:19', '2021-10-13 08:30:19'),
(66, 'tenebuilder1', 'testeuse', '3411010', 'zircon', 'tens', 'or', NULL, '2021-12-13', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 4, 3, NULL, NULL, NULL, '2021-10-13 08:44:41', '2021-10-13 08:44:41'),
(67, 'a', 'dza', '12333311', 'adz', NULL, '133', NULL, '2021-10-15', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 2, 3, NULL, NULL, NULL, '2021-10-13 09:53:27', '2021-10-13 09:53:27'),
(68, 'TEST Mali', 'TEST Mali', '64548978676566', 'TEST Mali', NULL, 'TEST Mali', NULL, '2021-10-14', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 15, 13, NULL, NULL, NULL, '2021-10-15 12:21:09', '2021-10-15 12:21:09'),
(69, 'TEST Mali', 'TEST Mali', '781024616', 'TEST Mali', NULL, 'TEST Mali', NULL, '2021-10-15', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 9, 16, NULL, NULL, NULL, '2021-10-15 12:28:19', '2021-10-15 12:28:19'),
(71, 'tenebuilder1', 'testeuse', '1102310', 'zircon', 'tens', 'or', NULL, '2021-12-13', NULL, 1, 'machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 4, 3, NULL, NULL, NULL, '2021-10-15 12:40:44', '2021-10-15 12:45:16'),
(74, 'f', 'd', 'h89', 'gh', NULL, 'mtest', NULL, '2021-10-31', NULL, 1, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, NULL, NULL, NULL, '2021-10-31 03:51:05', '2021-10-31 03:51:05'),
(76, 'Cat', 'Family', '9886T', 'Subfam', NULL, 'XXX', NULL, '2021-11-09', NULL, 7, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 5, NULL, NULL, NULL, NULL, '2021-11-05 14:21:29', '2021-11-05 14:27:12'),
(77, 'CAT', 'Pelle', '88665T', 'Petite pelle', NULL, '950K', NULL, '2021-10-05', NULL, 7, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 5, NULL, NULL, NULL, NULL, '2021-11-05 14:28:31', '2021-11-05 14:28:31'),
(78, 'CAT', 'Chargeuse', '644T', 'Petite chargeuse', NULL, 'XXX', NULL, '2021-11-05', NULL, 7, 'Machine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 6, NULL, NULL, NULL, NULL, '2021-11-05 14:30:25', '2021-11-05 14:30:25');

-- --------------------------------------------------------

--
-- Structure de la table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Management', NULL, '2021-07-27 19:40:55', '2021-07-27 19:40:55'),
(2, 'Inside Sale', '', '2021-07-27 19:40:55', '2021-09-24 14:13:02'),
(4, ' Lindor ndiaye', '', '2021-09-23 19:22:31', '2021-09-28 06:56:07'),
(6, 'informatique', 'j', '2021-09-24 14:12:07', '2021-10-12 19:25:10'),
(7, 'Testh', 'testetest', '2021-10-01 13:40:10', '2021-10-12 19:24:59');

-- --------------------------------------------------------

--
-- Structure de la table `departmentsss`
--

CREATE TABLE `departmentsss` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `department_user`
--

CREATE TABLE `department_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `department_user`
--

INSERT INTO `department_user` (`id`, `department_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(0, 1, 7, NULL, NULL),
(0, 1, 8, NULL, NULL),
(0, 1, 9, NULL, NULL),
(0, 2, 10, NULL, NULL),
(0, 2, 11, NULL, NULL),
(0, 1, 12, NULL, NULL),
(0, 2, 13, NULL, NULL),
(0, 2, 14, NULL, NULL),
(0, 4, 15, NULL, NULL),
(0, 6, 16, NULL, NULL),
(0, 7, 17, NULL, NULL),
(0, 1, 18, NULL, NULL),
(0, 1, 19, NULL, NULL),
(0, 6, 20, NULL, NULL),
(0, 2, 21, NULL, NULL),
(0, 2, 23, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_display` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `industries`
--

CREATE TABLE `industries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `integrations`
--

CREATE TABLE `integrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_secret` int(11) DEFAULT NULL,
  `api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `org_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `payment_received_at` datetime DEFAULT NULL,
  `due_at` datetime DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `invoice_lines`
--

CREATE TABLE `invoice_lines` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `leads`
--

CREATE TABLE `leads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_assigned_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `user_created_id` int(10) UNSIGNED NOT NULL,
  `contact_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_06_04_124835_create_industries_table', 1),
(4, '2015_12_28_163028_create_clients_table', 1),
(5, '2015_12_29_150049_create_invoice_table', 1),
(6, '2015_12_29_204031_tasks_table', 1),
(7, '2016_01_10_204413_create_comments_table', 1),
(8, '2016_01_18_113656_create_leads_table', 1),
(9, '2016_01_23_144854_settings', 1),
(10, '2016_01_26_003903_documents', 1),
(11, '2016_01_31_211926_invoice_lines_table', 1),
(12, '2016_03_21_171847_create_department_table', 1),
(13, '2016_03_21_172416_create_department_user_table', 1),
(14, '2016_04_06_230504_integrations', 1),
(15, '2016_05_21_205532_create_activity_log_table', 1),
(16, '2016_08_26_205017_entrust_setup_tables', 1),
(17, '2016_11_04_200855_create_notifications_table', 1),
(18, '2019_05_05_233153_change_contact_field_name', 1),
(19, '2019_05_07_092658_create_abonnements_table', 1),
(20, '2019_05_07_092659_create_boitiers_table', 1),
(21, '2019_05_07_152709_create_contacts_table', 1),
(22, '2019_05_07_153812_migrate_primary_contacts', 1),
(23, '2019_05_29_210436_add_contact_permissions', 1),
(24, '2019_06_05_123646_client_addresses', 1),
(25, '2019_06_05_163149_contact_address_changes', 1),
(26, '2019_09_30_153727_create_items_table', 1),
(27, '2019_10_07_093339_create_pays_table', 1),
(28, '2019_10_14_210496_add_boitier_permissions', 1),
(29, '2019_10_14_280436_add_abonnement_permissions', 1),
(30, '2019_10_30_122228_set_boitier_id_nullable', 1),
(31, '2019_10_30_122250_set_abonnement_id_nullable', 1),
(32, '2021_06_27_221712_create_abo_equipements_table', 1),
(33, '2021_07_30_023822_create_charged_to_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`id`, `nom`, `created_at`, `updated_at`) VALUES
(1, 'Sénégal', '2021-07-27 23:24:48', '2021-07-30 00:54:07'),
(2, 'Mali', '2021-07-28 10:10:34', '2021-08-03 07:13:08'),
(3, 'Ghambie', '2021-08-11 13:09:51', '2021-09-28 06:59:08'),
(4, 'USA', '2021-09-28 06:58:57', '2021-09-28 06:58:57'),
(5, 'ozer', '2021-10-14 14:16:42', '2021-10-14 14:16:42'),
(7, 'Zimbabwe', '2021-11-05 14:26:13', '2021-11-05 14:26:13');

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'equipement-create', 'Create equipment', 'Permission to create equipment', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(2, 'equipement-read', 'Read equipment', 'Permission to read equipment', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(3, 'equipement-update', 'Update equipment', 'Permission to update equipment', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(4, 'equipement-delete', 'Delete equipment', 'Permission to delete equipment', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(5, 'boitier-create', 'Create PL', 'Permission to create boitier', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(6, 'boitier-read', 'Read PL', 'Permission to read boitier', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(7, 'boitier-update', 'Update PL', 'Permission to update boitier', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(8, 'boitier-delete', 'Delete PL', 'Permission to delete boitier', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(9, 'abonnement-create', 'Create subscription', 'Permission to create abonnement', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(10, 'abonnement-read', 'Read subscription', 'Permission to read abonnement', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(11, 'abonnement-update', 'Update subscription', 'Permission to update abonnement', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(12, 'abonnement-delete', 'Delete subscription', 'Permission to delete abonnement', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(13, 'user-create', 'Create user', 'Permission to create user', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(14, 'user-read', 'Read user', 'Permission to read user', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(15, 'user-update', 'Update user', 'Permission to update user', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(16, 'user-delete', 'Delete user', 'Permission to update delete', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(17, 'client-create', 'Create customer', 'Permission to create client', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(18, 'client-read', 'Read customer', 'Permission to create read', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(19, 'client-update', 'Update customer', 'Permission to update customer', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(20, 'client-delete', 'Delete customer', 'Permission to delete customer', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(21, 'task-create', 'Create task', 'Permission to create task', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(22, 'task-update', 'Update task', 'Permission to update task', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(23, 'lead-create', 'Create lead', 'Permission to create lead', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(24, 'lead-update', 'Update lead', 'Permission to update lead', '2021-08-31 11:35:58', '2021-08-31 11:35:58');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 8),
(2, 1),
(2, 2),
(2, 3),
(2, 8),
(3, 1),
(3, 2),
(3, 3),
(3, 8),
(4, 1),
(4, 2),
(4, 3),
(4, 8),
(5, 1),
(5, 2),
(5, 8),
(6, 1),
(6, 2),
(6, 8),
(7, 1),
(7, 2),
(7, 8),
(8, 1),
(8, 2),
(8, 8),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(18, 2),
(19, 1),
(19, 2),
(20, 1),
(20, 2),
(21, 1),
(21, 2),
(22, 1),
(22, 2),
(23, 1),
(23, 2),
(24, 1),
(24, 2);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Administrator', 'Administrators have superuser access', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(2, 'Admin', 'Admin', 'Admin du systeme', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(3, 'Suport CM', 'Support CM', '', '2021-08-31 11:35:58', '2021-08-31 11:35:58'),
(8, 'controleur ', 'Controleur ', 'il sera chargé de geré le controle de la plateforme', '2021-09-24 13:19:42', '2021-09-24 13:19:42');

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(14, 3),
(16, 8),
(17, 3),
(19, 8),
(20, 3),
(21, 8),
(23, 8);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_complete_allowed` int(11) NOT NULL,
  `task_assign_allowed` int(11) NOT NULL,
  `lead_complete_allowed` int(11) NOT NULL,
  `lead_assign_allowed` int(11) NOT NULL,
  `time_change_allowed` int(11) NOT NULL,
  `comment_allowed` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `task_complete_allowed`, `task_assign_allowed`, `lead_complete_allowed`, `lead_assign_allowed`, `time_change_allowed`, `comment_allowed`, `country`, `company`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 2, 2, 2, 2, 'en', 'Media', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_assigned_id` int(10) UNSIGNED NOT NULL,
  `user_created_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED DEFAULT NULL,
  `deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `matricule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statut` int(11) NOT NULL DEFAULT 1,
  `work_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `matricule`, `pays`, `personal_number`, `statut`, `work_number`, `image_path`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$Kb4t26TX5DdIuwXSuU.6LOrsb6S64pWD7U7a8PvahtvCkp7X0dR8G', '', 'Reseau', '0', 0, '0', '', '', 'KF2qAVD9qTiQcGnDxartBDmBF5n88uJzmRwyuJTHnCr2gQHskpzHyQbvXnmg', '2016-06-04 11:42:19', '2016-06-04 11:42:19'),
(14, 'User', 'user@gmail.com', '$2y$10$KW9QRI9iP.kwNTRwPF.wjOAAjKzD125b6VYGUUoDjfAohSih/ahla', '986789580438', 'Ghambie', '70695843748684785', 0, NULL, NULL, NULL, NULL, '2021-09-23 08:11:52', '2021-09-23 08:12:21'),
(16, 'Users', 'users@gmail.com', '$2y$10$71ZEE0.LJLzuPOj7QSEO/./swuYDIdgyK6bbx8yjyIWU1nHnXU43O', '09885444', 'USA', '2345688999', 1, NULL, NULL, NULL, 'FqY4eoqsJjO8p5svnN3V3n3VYK00f3eTLMwKPeKU9d6H9eummb9jf49xFleZ', '2021-09-28 07:04:33', '2021-09-28 07:04:55'),
(17, 'test', 'test@test1.com', '$2y$10$AuiSW5.kxkNaF6bKhfUm3u1QwVZUua28MIiPSySPvmjyE.fQt7dpW', '12', 'Sénégal', '775893076', 1, NULL, NULL, NULL, NULL, '2021-10-01 15:55:16', '2021-10-01 15:55:16'),
(19, 'Amadou Lindor', 'lindoramadou@gmail.com', '$2y$10$8YTIXL07JNRdwTePlMurruL0uGsGvBQCZBW1h3R/3qAoFaON2tY/G', '098390', 'Sénégal', '775893076', 0, NULL, NULL, NULL, 'uvyOhnOtZDXOwUaCmuCPkA1ktY2vIfVFgyLCIJsXTfXAZbnx95PYmsQCHbtW', '2021-10-13 09:50:19', '2021-10-13 09:50:19'),
(20, 'doudou', 'dou@gmail.com', '$2y$10$Zs07kr4S.fllEQagUx.nHO43EnV5YbDjgZD05Y9ucdddrDFRwWhmq', '090769765', 'Sénégal', '3456789088765', 1, NULL, NULL, NULL, 'z2myy3th0Mp5BBIkDnLtsX9C2M0GMCOwa9ThhKXXSeXcbxhhgG6vEpY63T2Q', '2021-10-15 12:17:56', '2021-10-15 12:30:51'),
(21, 'Test user SDQ', 'user@mail.com', '$2y$10$4LZN5CNQIz6q5zf15HNnRu9Jf5bKOvXsG2wPHco6X6J5YBwWX0oP6', '2425', 'Zimbabwe', '1345', 0, NULL, NULL, NULL, 'KnBRzJD61IXGysO1VNgQTN29JdxwfTEvYrxPiNVQBGIzDaPa1tkLuQgxjGno', '2021-10-29 09:35:08', '2021-11-05 14:26:36'),
(23, 'tafa', 'tenetafa@wintech.sn', '$2y$10$vUottzLtmztDANOqIpisxuBAZcy4mdlkNZX4CLLxYn2CVkrwPWbDi', '89764', 'Sénégal', '775893076', 1, NULL, NULL, NULL, 'BJvFyhVQHUm5PHeOHbCAz55W5fe4ffffQElzkEhyHuIDqFUs0CiycVILQhKc', '2021-11-07 22:18:01', '2021-11-07 22:18:01');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `abonnements`
--
ALTER TABLE `abonnements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `abo_equipements`
--
ALTER TABLE `abo_equipements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `boitiers`
--
ALTER TABLE `boitiers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `chargeds`
--
ALTER TABLE `chargeds`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `charged_to`
--
ALTER TABLE `charged_to`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clients_primary_email_unique` (`primary_email`),
  ADD UNIQUE KEY `clients_numidint_unique` (`numidint`),
  ADD UNIQUE KEY `clients_numidext_unique` (`numidext`),
  ADD UNIQUE KEY `clients_primary_number_unique` (`primary_number`),
  ADD UNIQUE KEY `clients_secondary_number_unique` (`secondary_number`),
  ADD KEY `clients_industry_id_foreign` (`industry_id`),
  ADD KEY `pay_id` (`pay_id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_source_id_source_type_index` (`source_id`,`source_type`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Index pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contacts_numero_serie_unique` (`numero_serie`),
  ADD KEY `contacts_client_id_foreign` (`client_id`),
  ADD KEY `contacts_boitier_id_foreign` (`boitier_id`),
  ADD KEY `contacts_abonnement_id_foreign` (`abonnement_id`);

--
-- Index pour la table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_client_id_foreign` (`client_id`);

--
-- Index pour la table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `integrations`
--
ALTER TABLE `integrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_client_id_foreign` (`client_id`);

--
-- Index pour la table `invoice_lines`
--
ALTER TABLE `invoice_lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_lines_invoice_id_foreign` (`invoice_id`);

--
-- Index pour la table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leads_user_assigned_id_foreign` (`user_assigned_id`),
  ADD KEY `leads_client_id_foreign` (`client_id`),
  ADD KEY `leads_user_created_id_foreign` (`user_created_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Index pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Index pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_user_assigned_id_foreign` (`user_assigned_id`),
  ADD KEY `tasks_user_created_id_foreign` (`user_created_id`),
  ADD KEY `tasks_client_id_foreign` (`client_id`),
  ADD KEY `tasks_invoice_id_foreign` (`invoice_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_matricule_unique` (`matricule`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `abonnements`
--
ALTER TABLE `abonnements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `abo_equipements`
--
ALTER TABLE `abo_equipements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT pour la table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `boitiers`
--
ALTER TABLE `boitiers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `chargeds`
--
ALTER TABLE `chargeds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `charged_to`
--
ALTER TABLE `charged_to`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT pour la table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `integrations`
--
ALTER TABLE `integrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `invoice_lines`
--
ALTER TABLE `invoice_lines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`);

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_abonnement_id_foreign` FOREIGN KEY (`abonnement_id`) REFERENCES `abonnements` (`id`),
  ADD CONSTRAINT `contacts_boitier_id_foreign` FOREIGN KEY (`boitier_id`) REFERENCES `boitiers` (`id`),
  ADD CONSTRAINT `contacts_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Contraintes pour la table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Contraintes pour la table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Contraintes pour la table `invoice_lines`
--
ALTER TABLE `invoice_lines`
  ADD CONSTRAINT `invoice_lines_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`);

--
-- Contraintes pour la table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `leads_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `leads_user_assigned_id_foreign` FOREIGN KEY (`user_assigned_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `leads_user_created_id_foreign` FOREIGN KEY (`user_created_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `tasks_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `tasks_user_assigned_id_foreign` FOREIGN KEY (`user_assigned_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `tasks_user_created_id_foreign` FOREIGN KEY (`user_created_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
