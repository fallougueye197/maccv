<?php 

//Francais
return [
    'Profile' => 'Profil',

    'Role' => 'Gestion des rôles',
    'Settings' => 'Reglages',
    'AllRole' => 'Tous les rôles',

    'Country' => 'Pays',
    'AllCountry' => 'Tous les pays',
    'NewCountry' => 'Ajouter un pays',

    'Charged'=>'Chargé à',
    'AllCharged'=>"Tous les 'chargé à' ",
    'NewCharged'=>"Ajouter un 'Chargé à'",
    
    'Departments' => 'Departements',
    'AllDepartments' => 'Tous les Departements',
    'NewDepartment' => 'Ajouter un Departement',

    'Users' => 'Utilisateur',
    'AllUsers' => 'Tous les utilisateurs',
    'NewUser' => 'Ajouter un utilisateur',

    'ProductLink' => 'Boîtiers',
    'AllProductLink' => 'Tous les boîtiers',
    'NewPL' => 'Ajouter un Boîtier',

    'Subscriptions' => 'Abonnements',
    'AllSubscriptions' => 'Tous les Abonnements',
    'NewSubscriptions' => 'Ajouter un Abonnements',

    'Customers' => 'Clients',
    'AllCustomers' => 'Tous les clients',
    'NewCustomer' => 'Ajouter un client',
    'ImportCustomer' => 'Importer un client',

    'Equipments' => 'Equipements',
    'AllEquipments' => 'Tous les Equipements',
    'Newequipment' => 'Ajouter équipement',
    'ImportEquipment' => 'Importer Equipement',
    'Billing' => 'Facturation',

    'SignOut' => 'Deconnexion',
    'Dashboard' => 'Tableau de bord',




   
]

?>