<?php 

//English
return [
    'Profile' => 'Profile',

    'Role' => 'Role Management',
    'Settings' => 'Settings',
    'AllRole' => 'All Role',

    'Country' => 'Country',
    'AllCountry' => 'All Country',
    'NewCountry' => 'New Country',
    
    'Charged'=>'Charged To',
    'AllCharged'=>'All charged to',
    'NewCharged'=>'New Charged to',
    
    

    'Departments' => 'Departments',
    'AllDepartments' => 'All Departments',
    'NewDepartment' => 'New Department',

    'Users' => 'Users',
    'AllUsers' => 'All Users',
    'NewUser' => 'New User',

    'ProductLink' => 'Product Link',
    'AllProductLink' => 'All Product Link',
    'NewPL' => 'New PL',

    'Subscriptions' => 'Subscriptions',
    'AllSubscriptions' => 'All Subscriptions',
    'NewSubscriptions' => 'New Subscriptions',

    'Customers' => 'Customers',
    'AllCustomers' => 'All Customers',
    'NewCustomer' => 'New Customer',
    'ImportCustomer' => 'Import Customer',

    'Equipments' => 'Equipments',
    'AllEquipments' => 'All Equipments',
    'Newequipment' => 'New equipment',
    'ImportEquipment' => 'Import Equipment',
    'Billing' => 'Billing',

    'SignOut' => 'Sign Out',
    'Dashboard' => 'Dashboard',
    

    
  
]
?>