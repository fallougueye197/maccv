<div class="container-fluid">
    <div class="row">
        <div class="form-group col-md-6">
            {!! Form::label('name', __('Name'), ['class' => 'control-label']) !!} <span class="text-danger">*</span>
           <!-- {!! Form::text('name', $data['name'] ?? null, ['class' => 'form-control']) !!} -->
            <input name="name" type="text" class="form-control" value="{{$client->name}}" >
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('pays', __('Country'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
            <?php $payss = DB::table('pays')->get() ?>
            <?php $pa = DB::table('pays')->where('id', $client->pay_id)->first() ?>
            <select id="inputState" name="pay_id" class="form-control">
            <option value="{{$client->pay_id}}">{{$pa->nom}}</option>
            @foreach($payss as $pays)
                <option value="{{$pays->id}}">{{$pays->nom}}</option>
            @endforeach
            </select>
            
        </div>
       
    </div>

    {{-- <div class="row">
        <div class="col-md-6">

            <div class="row">
                <div class="form-group col-md-12">
                    {!! Form::label('billing_address1', __('Billing Address 1'), ['class' => 'control-label']) !!}
                    {!! Form::text('billing_address1', $data['billing_address1'] ?? null, ['class' => 'form-control']) !!}
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-12">
                    {!! Form::label('billing_address2', __('Billing Address 2'), ['class' => 'control-label']) !!}
                    {!! Form::text('billing_address2', $data['billing_address2'] ?? null, ['class' => 'form-control']) !!}
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-4 col-sm-6">
                    {!! Form::label('billing_city', __('Billing City'), ['class' => 'control-label']) !!}
                    {!! Form::text('billing_city', $data['billing_city'] ?? null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4 col-sm-6">
                    {!! Form::label('billing_state', __('Billing State/Region'), ['class' => 'control-label']) !!}
                    {!! Form::text('billing_state', $data['billing_state'] ?? null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4 col-sm-6">
                    {!! Form::label('billing_zipcode', __('Billing ZIP'), ['class' => 'control-label']) !!}
                    {!! Form::text('billing_zipcode',  $data['billing_zipcode'] ?? null,  ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-12 col-sm-6">
                    {!! Form::label('billing_country', __('Billing Country'), ['class' => 'control-label']) !!}
                    {!! Form::text('billing_country', $data['billing_country'] ?? null, ['class' => 'form-control']) !!}
                </div>
            </div>

        </div>
        <div class="col-md-6">
            
            <div class="row">
                <div class="form-group col-md-12">
                    {!! Form::label('shipping_address1', __('Shipping Address 1'), ['class' => 'control-label']) !!}
                    {!! Form::text('shipping_address1', $data['shipping_address1'] ?? null, ['class' => 'form-control']) !!}
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-12">
                    {!! Form::label('shipping_address2', __('Shipping Address 2'), ['class' => 'control-label']) !!}
                    {!! Form::text('shipping_address2', $data['shipping_address2'] ?? null, ['class' => 'form-control']) !!}
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col-md-4 col-sm-6">
                    {!! Form::label('shipping_city', __('Shipping City'), ['class' => 'control-label']) !!}
                    {!! Form::text('shipping_city', $data['shipping_city'] ?? null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4 col-sm-6">
                    {!! Form::label('shipping_state', __('Shipping State/Region'), ['class' => 'control-label']) !!}
                    {!! Form::text('shipping_state', $data['shipping_state'] ?? null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-4 col-sm-6">
                    {!! Form::label('shipping_zipcode', __('Shipping ZIP'), ['class' => 'control-label']) !!}
                    {!! Form::text('shipping_zipcode',  $data['shipping_zipcode'] ?? null,  ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-12 col-sm-6">
                    {!! Form::label('shipping_country', __('Shipping Country'), ['class' => 'control-label']) !!}
                    {!! Form::text('shipping_country', $data['shipping_country'] ?? null, ['class' => 'form-control']) !!}
                </div>
            </div>
            
        </div>
    </div> --}}

    <div class="row">
        <div class="form-group col-sm-6">
            {!! Form::label('numidint', __('Internal identification number'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
            <!--{!! Form::text('numidint', $data['numidint'] ?? null, ['class' => 'form-control']) !!} -->
            
            <input name="numidint" type="text" class="form-control" value="{{$client->numidint}}" >

        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('numidext', __('External identification number'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
           <!-- {!! Form::text('numidext', $data['numidext'] ?? null, ['class' => 'form-control']) !!} -->
            
            <input name="numidext" type="text" class="form-control" value="{{$client->numidext}}" >
        </div>
    </div>

    <div class="row">
    <div class="form-group col-sm-6">
            {!! Form::label('numidext2', __('External identification number 2'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
            <!--{!! Form::text('numidext2', $data['numidext2'] ?? null, ['class' => 'form-control']) !!} -->
            
            <input name="numidext2" type="text" class="form-control" value="{{$client->numidext2}}" >
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('primary_email', __('Mail address'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
            <!--{!! Form::email('primary_email', $data['primary_email'] ?? null, ['class' => 'form-control']) !!} -->
            
            <input name="primary_email" type="text" class="form-control" value="{{$client->primary_email}}" >
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            {!! Form::label('business', __('Business Unit'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
            <!--{!! Form::text('business', $data['business'] ?? null, ['class' => 'form-control']) !!} -->
            
            <input name="business" type="text" class="form-control" value="{{$client->business}}" >
        </div>
        {{-- <div class="form-group col-sm-12">
            {!! Form::label('industry_id', __('Pays'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
            {!! Form::select('industry_id', $industries, $data['industry_id'] ?? null, ['class' => 'form-control ui search selection top right pointing search-select', 'id' => 'search-select', 'placeholder' => 'Select countries...']) !!}
        </div> --}}
    </div>

    

    {!! Form::submit($submitButtonText, ['class' => 'btn btn-lg btn-primary']) !!}    

</div>
