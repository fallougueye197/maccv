@extends('layouts.master')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

@section('heading')
    <h1>{{ __('My Customers') }}</h1>
@stop

@section('content')

    <table class="table table-striped table-bordered" id="clients-table">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Pays') }}</th>
            <th>{{ __('Business Unit') }}</th>
            <th>{{ __('ID internal 1') }}</th>
            <th>{{ __('ID external 2') }}</th>        
            <th>{{ __('Actions') }}</th>
        </tr>
        </thead>
    </table>

@stop

@push('scripts')
<script>
    $(function () {
        $('#clients-table').DataTable({
            processing: true,
            serverSide: true,

            ajax: '{!! route('clients.mydata') !!}',
            columns: [

                {data: 'namelink', name: 'name'},
                {data: 'pays', name: 'pays'},
                {data: 'business', name: 'business'},
                {data: 'numIdInt', name: 'numIdInt'},
                {data: 'numIdExt', name: 'numIdExt_1'},
                {data: 'numIdExt', name: 'numIdExt_2'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush
