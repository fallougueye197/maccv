<div id="contact" class="tab-pane fade in active" role="tabpanel">
    <div class="boxspace">
        <table class="table table-striped table-bordered">
            <h4>{{ __('Tous les equipements') }}</h4>
            <thead>
            <tr>
                <th>{{ __('Constructeur') }}</th>
                <th>{{ __('Famille') }}</th>
                <th>{{ __('Numero de serie') }}</th>
                <th>{{ __('Date de mise en service') }}</th>
                <th>
                    <a href="{{ route('contacts.create', ['client' => $client->id])}}">
                        <button class="btn btn-xs btn-success">{{ __('Nouvel equipement') }}</button>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($client->contacts as $contact)
                <tr>
                    <td>
                        <a href="{{ route('contacts.show', $contact->id) }}">{{$contact->constructeur}}</a>
                    </td>
                    <td>
                        {{$contact->famille}}
                    </td>
                    <td>
                       {{$contact->numero_serie}}
                    </td>
                    <td>
                        {{$contact->date_mise_en_service}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>