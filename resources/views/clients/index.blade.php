@extends('layouts.master')
@section('heading')
{{-- cdn for extraction --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
{{--  --}}
    <h1>{{ __('All Customers') }}</h1>
@stop

@section('content')

    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>{{ __('ID') }}</th>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Country') }}</th>
            <th>{{ __('Business Unit') }}</th>
            <th>{{ __('ID internal') }}</th>
            <th>{{ __('ID external 1') }}</th>
            <th>{{ __('ID external 2') }}</th>        
            <th>{{ __('Actions') }}</th>
        </tr>
        </thead>
        <tbody>
                @foreach($clients as $client)
                                  @if(Auth::user()->pays == 'Reseau')

                    <tr>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->name }}</td>
                         <?php $payss = DB::table('pays')->where('id',$client->pay_id)->get() ?>
                        @foreach($payss as $pa)
                        <td>{{ $pa->nom }}</td>
                        @endforeach
                        <td>{{ $client->business }}</td>
                        <td>{{ $client->numidint }}</td>
                        <td>{{ $client->numidext }}</td>
                        <td>{{ $client->numidext2 }}</td>

                       <td>
                        <a class="btn btn-sm btn-warning" href="/clients/{{$client->id}}/edit">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                        @if(!$eqipclient = DB::table('contacts')->pluck('client_id')->contains($client->id))

                        {!!Form::open(['action' => ['ClientsController@destroy', $client->id], 'method'=> 'POST'])!!}    
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('delete', ['class'=> 'btn btn-sm btn-warning'])}}
                        {!!Form::close()!!}

                        @endif
                      </td>

                    
                    </tr>
                    @elseif($client->pays == Auth::user()->pays)
                        <tr>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->name }}</td>
                       <?php $payss = DB::table('pays')->where('id',$client->pay_id)->get() ?>
                        @foreach($payss as $pa)
                        <td>{{ $pa->nom }}</td>
                        @endforeach
                        <td>{{ $client->business }}</td>
                        <td>{{ $client->numidint }}</td>
                        <td>{{ $client->numidext }}</td>
                        <td>{{ $client->numidext2 }}</td>

                       <td>
                        <a class="btn btn-sm btn-warning" href="/clients/{{$client->id}}/edit">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                        @if(!$eqipclient = DB::table('contacts')->pluck('client_id')->contains($client->id))

                        {!!Form::open(['action' => ['ClientsController@destroy', $client->id], 'method'=> 'POST'])!!}    
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('delete', ['class'=> 'btn btn-sm btn-warning'])}}
                        {!!Form::close()!!}

                        @endif
                      </td>

                    
                    </tr>
                    @endif
                @endforeach
        </tbody>

    </table>
 <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/clients/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
       
    
       <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
@stop

@push('scripts')
{{-- <script>
    $(function () {
        $('#clients-table').DataTable({
            processing: true,
            serverSide: true,

            ajax: '{!! route('clients.data') !!}',
            columns: [

                {data: 'namelink', name: 'name'},
                {data: 'pays', name: 'pays'},
                {data: 'business', name: 'business'},
                {data: 'numIdInt', name: 'numIdInt'},
                {data: 'numIdExt', name: 'numIdExt_1'},
                {data: 'numIdExt', name: 'numIdExt_2'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false},

            ]
        });
    });
</script> --}}
@endpush
