@extends('layouts.master')

@section('content')
{!! Form::open(['action' => ['DepartmentsController@update', $department->id],'method'=> 'POST']) !!}

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label( 'name', __('Department name'), ['class' => 'control-label']) !!}
        {!! Form::text('name', $department->name,['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label( 'description', __('Department description'), ['class' => 'control-label']) !!}
        {!! Form::textarea('description', $department->description, ['class' => 'form-control']) !!}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {!! Form::submit('Update', ['class'=>'btn btn-lg btn-primary']) !!}

    {!! Form::close() !!}

   

    {!! Form::close() !!}

@endsection
