@extends('layouts.master')

@section('content')
    <div class="col-lg-12 currenttask">
        <table class="table table-striped ">
            <h3>{{ __("header.AllDepartments") }}</h3>
            <thead>
            <thead>
            <tr>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Description') }}</th>
                @if(Entrust::hasRole('administrator'))
                    <th>{{ __('Edit') }}</th>
                    <th>{{ __('Delete') }}</th>
                @endif
            </tr>
            </thead>
            <tbody>

            @foreach($department as $dep)
                <tr>
                    <td>{{$dep->name}}</td>
                    <td>{{Str_limit($dep->description, 50)}}</td>
                    @if(Entrust::hasRole('administrator'))
                    <td><a class="btn btn-sm btn-warning" href="/departments/{{$dep->id}}/edit"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                    </td>
                    @if(!$department_user = DB::table('department_user')->pluck('department_id')->contains($dep->id))
                        <td>   {!! Form::open(['method' => 'DELETE','route' => ['departments.destroy', $dep->id]
                                                    ]); !!}

                            {!! Form::submit( __('Delete'), ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure?")']); !!}

                            {!! Form::close(); !!}</td></td>
                    @endif
                    @endif
                </tr>
            @endforeach

            </tbody>
        </table>
 <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/departments/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>
    </div>

@stop
