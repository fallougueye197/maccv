
 @extends('layouts.master')

 @section('content')
     <div class="col-lg-12 currenttask">
         <table class="table table-striped table-bordered">
             <h3>{{ __("header.AllCountries") }}</h3>
             <thead>
             <thead>
             <tr>
                 <th>{{ __('Name') }}</th>
                 @if(Entrust::hasRole('administrator'))
                     <th>{{ __('Update') }}</th>
                     <th>{{ __('Delete') }}</th>
                 @endif
             </tr>
             </thead>
             <tbody>
 
             @foreach($pays as $pays)
                 <tr>
                     <td>{{$pays->nom}}</td>
                     @if(Entrust::hasRole('administrator')) 
                     <td><a class="btn btn-md btn-warning" href="/pays/{{$pays->id}}/edit"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                     <td>   {!! Form::open(['method' => 'DELETE','route' => ['pays.destroy', $pays->id]]); !!}
                         @if(!$clients = DB::table('clients')->pluck('pays')->contains($pays->nom) 
                            AND !$users = DB::table('users')->pluck('pays')->contains($pays->nom)
                            AND !$contacts = DB::table('contacts')->pluck('pays')->contains($pays->nom))
                        {!! Form::submit( __('Delete'), ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure?")']); !!}
                        {!! Form::close(); !!}
                    </td>
                     @endif
                     @endif
                 </tr>
             @endforeach
 
             </tbody>
         </table>
         <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/pays/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>
 
     </div>
 
 @stop
 
    