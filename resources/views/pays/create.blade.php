@extends('layouts.master')

@section('content')
             <h3>{{ __("header.Createnewcountry") }}</h3>

    {!! Form::open([
            'route' => 'pays.store',
            ]) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label( 'nom', __('Country name'), ['class' => 'control-label']) !!}
        {!! Form::text('nom', null,['class' => 'form-control']) !!}
    </div>
    {!! Form::submit(__("Create"), ['class' => 'btn btn-md btn-ptimary']) !!}

    {!! Form::close() !!}

@endsection
