@extends('layouts.master')
@section('content')

<h1>{{ __('header.Update') }}</h1>
{!! Form::open(['action' => ['PaysController@update', $pays->id],'method'=> 'POST']) !!}
<div class="form-group">
    {{ Form::label('nom', 'Country name') }}
    {{ Form::text('nom', $pays->nom, ['class'=>'form-control']) }}
    


    
</div>
{{Form::hidden('_method', 'PUT')}}
{!! Form::submit('Update', ['class'=>'btn btn-lg btn-primary']) !!}
{!! Form::close() !!}
@endsection
   