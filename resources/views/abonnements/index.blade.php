@extends('layouts.master')
@section('heading')
{{-- cdn for extraction --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
{{--  --}}
    <h1>{{ __('All Subscription') }}</h1>
@stop


@section('content')
    <div class="col-lg-12 currenttask">
        <table  id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>{{ __("ID ") }}</th>
                <th>{{ __("Subscription Name") }}</th>
                <th>{{ __('Type of Subscription') }}</th>
                <th>{{ __('Interface1') }}</th>
                <th>{{ __('Interface2') }}</th>
                <th>{{ __('Interface3') }}</th>
                <th>{{ __('Price') }}</th>
                <th>{{ __('Status') }}</td>





                @if(Entrust::hasRole('administrator'))
                <th>{{__('Action')}}</th>
                @endif 
            </tr>
            </thead>
            <tbody>

            @foreach($abonnements as $abonnement)
            @if($abonnement->id != 1)
                <tr>
                    <td>{{$abonnement->id}}</td>
                    <td>{{$abonnement->nom}}</td>
                    <td>{{$abonnement->type}}</td>
                    <td>{{$abonnement->interface1}}</td>
                    <td>{{$abonnement->interface2}}</td>
                     <td>{{$abonnement->interface3}}</td>
                    <td>{{$abonnement->tarif}}  CFA</td>
                     <td class="center">
							@if($abonnement->statut==0)
							<span class="label label-warning">Active</span>
							@else
                                <span class="label label-default">Unactive</span>
							@endif
					</td> 
                    @if(Entrust::hasRole('administrator'))
                    <td class="center">
                            @if($abonnement->statut==1)
							<a class="btn btn btn-default" href="{{URL::to('/unactive_abonnement/'.$abonnement->id)}}">
                                <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span>							</a>
                            @else
                             <a class="btn btn-warning" href="{{URL::to('/active_abonnement/'.$abonnement->id)}}">
                                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>							</a>
                            @endif
						<a class = "btn btn-sm btn-warning" href=" /abonnements/{{$abonnement->id}}/edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span><a>
                         @if(!$eqipabon = DB::table('contacts')->pluck('abonnement_id')->contains($abonnement->id))
                        {{-- {!!Form::open(['action' => ['AbonnementsController@destroy', $abonnement->id], 'method'=> 'POST'])!!}    
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Supprimer', ['class'=> 'btn btn-sm btn-warning'])}}
                        {!!Form::close()!!} --}}
                         @endif
                    </td>
                        @endif
         

                    {{-- @if(Entrust::hasRole('administrator'))
                        <td>   {!! Form::open([
            'method' => 'DELETE',
            'route' => ['abonnements.destroy', $abon->id]
        ]); !!}
                            {!! Form::submit( __('Delete'), ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure?")']); !!}

                            {!! Form::close(); !!}</td></td>
                    @endif --}}
                </tr>
                @endif
            @endforeach

            </tbody>
        </table>
        </div>
        <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/abonnements/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>

        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
   

    <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>

    </div>
   

@stop
