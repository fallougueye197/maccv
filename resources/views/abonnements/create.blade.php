@extends('layouts.master')

@section('content')
    {!! Form::open([
            'route' => 'abonnements.store',
            ]) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label( 'nom', __("Subscription Name"), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::text('nom', null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'type', __("Type of subscription"), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::select('type',['Permanent' => 'Permanent','Promotionnel'=>'Promotionnel'],null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'interface1', __("Interface 1 "), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::text('interface1', null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'interface', __("Interface 2 "), ['class' => 'control-label']) !!}
        {!! Form::text('interface2', null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'interface3', __("Interface 3 "), ['class' => 'control-label']) !!}
        {!! Form::text('interface3', null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'tarif', __("Price in CFA"), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::text('tarif', null,['class' => 'form-control']) !!}
    </div>

    {!! Form::submit(__("Create subscription"), ['class' => 'btn btn-warning']) !!}

    {!! Form::close() !!}

@endsection
