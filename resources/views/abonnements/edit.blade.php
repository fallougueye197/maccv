@extends('layouts.master')

@section('content')
{!! Form::open(['action' => ['AbonnementsController@update', $abonnement->id],'method'=> 'POST']) !!}

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label( 'nom', __("Subscription Name"), ['class' => 'control-label']) !!}
        {!! Form::text('nom', $abonnement->nom,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'type', __("Type of Subscription "), ['class' => 'control-label']) !!}
        {!! Form::select('type',['Permanent' => 'Permanent','Promotionnel'=>'Promotionnel'],$abonnement->type, ['class'=>'form-control']) !!}

    </div>
    <div class="form-group">
        {!! Form::label( 'interface1', __("Interface 1 "), ['class' => 'control-label']) !!}
        {!! Form::text('interface1', $abonnement->interface1,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'interface', __("Interface 2 "), ['class' => 'control-label']) !!}
        {!! Form::text('interface2', $abonnement->interface2,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'interface3', __("Interface 3 "), ['class' => 'control-label']) !!}
        {!! Form::text('interface3', $abonnement->interface3,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label( 'tarif', __("Tarif "), ['class' => 'control-label']) !!}
        {!! Form::text('tarif', $abonnement->tarif,['class' => 'form-control']) !!}<span class="text-danger">CFA</span>
    </div>

    {{-- <div class="form-group">
        {!! Form::checkbox('statut', null) !!}
    </div>
     --}}
     {{Form::hidden('_method', 'PUT')}}
     {!! Form::submit('Update subscription', ['class'=>'btn btn-lg btn-ptimary']) !!}
     {!! Form::close() !!}

@endsection
