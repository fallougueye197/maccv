@extends('layouts.master_form')
@section('heading')
    <h1>{{ __('Create Equipment') }}</h1>
@stop
@section('content')    
    <!-- <form action="{{route('equipement.update', $contact->id)}}" method="post" class="form-horizontal" id="demo1-upload">
        {{ csrf_field() }}
        @method('PATCH') -->
        {!! Form::model($contact, ['method' => 'PATCH', 'route' => ['equipement.update', $contact->id]]) !!}
    
        @include('contacts.editequipform')  

        {!! Form::close() !!}  
    <!-- </form> -->
@stop