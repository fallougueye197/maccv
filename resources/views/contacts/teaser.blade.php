@extends('layouts.master')
@section('heading')
{{-- cdn for extraction --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
{{--  --}}
    <h1>{{__('Alert ')}}</h1>
@stop

@section('content')
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead> 
    <tr>
            <th>{{ __("Type Of Equipement") }}</th>
            <th>{{ __('Date of commissioning') }}</th>
            <th>{{ __('Serial number') }}</th>
            <th>{{ __("Client's name") }}</th>
<<<<<<< HEAD
            <th>{{ __("Subscription's name") }}</th>
=======
            <th>{{ __("Reference PL") }}</th>
>>>>>>> d4c2a6ab3014ee592df59f02abbeff191bcefc6b
            <th>{{ __('Start date') }}</th>
            <th>{{ __('End date') }}</th>
            <th>{{ __('Country') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
        </thead>
            <tbody>
                @foreach($contacts as $contact)
              @if($contact->datefin < date('Y-m-d', strtotime("+30 days")))
              
                    <tr>
                    <td>{{$contact->type}}</td>
                    <td>{{$contact->date_mise_en_service}}</td>
                    <td>{{$contact->numero_serie}}</td>
                    <td>{{$contact->client->name}}</td>
<<<<<<< HEAD
                    <td>{{$contact->abonnements->nom}}</td>
=======
                    <td>{{$contact->boitiers->reference}}</td>
>>>>>>> d4c2a6ab3014ee592df59f02abbeff191bcefc6b
                     <td>{{$contact->datedebut}}</td>
                     <td>{{$contact->datefin}}</td> 
                    <td>{{$contact->pays}}</td>

                    <td>
                        @if (Auth::user()->can('equipement-update'))
						<a href="{{ route('contacts.edit', ['contact' => $contact]) }}" class="btn btn-sm btn-warning">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                        <a href="{{ route('contacts.show', ['contact' => $contact]) }}" class="btn btn-sm btn-warning">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
						@endif
                        
                    </td>

                    
                    </tr>
                @endif
   
                @endforeach
            </tbody>
    </table>
     <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/contacts/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
       
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
    
@stop

@push('scripts')
<script>



    // $(function () {
    //     $('#contacts-table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax: '{!! route('contacts.data') !!}',
    //         columns: [
    //             {data: 'numero_serie', name: 'numero_serie'},
    //             {data: 'modele', name: 'modele'},
    //             {data: 'client_name', name: 'client_name'},
    //             {data: 'pays', name: 'pays',},
              

    //             {data: 'actions', name: 'actions', orderable: false, searchable: false},
    //         ]
    //     });
    // });
   
</script>



@endpush

