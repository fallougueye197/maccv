@extends('layouts.master')
@section('heading')
	{{-- <h1><img src="{{ URL::asset('frontend/images/image001.png')}}" alt="" style="width:100px ; height: 75px ;  box-shadow: 4px 4px 0px #aaa;" class="thumb-md img-circle">  --}}
		Serial number:
		{{ $contact->numero_serie }}</h1>
@endsection

@section('content')


	<div class="container-fluid">
		<div class="row">
				<div class="col-sm-12 col-lg-6">
						<div class="mini-stat clearfix bx-shadow bg-white" style="height: 250px">
							<span class="mini-stat-icon bg-warning"><i style="color:black" class="fa fa-truck"></i></span>
							<div class="mini-stat-info text-right text-dark" >
								<span  style="color:black">Equipment information</span><br> 
							</div>
							<div class="tiles-progress" >
								<div class="m-t-20" >
								<h5 class="text-uppercase"style="color:black; ">FAMILY: <span style="margin-left: 140px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ $contact->famille }}</span></h5>
								<h5 class="text-uppercase"style="color:black; ">COMMISSIONING DATE: <span style="margin-left: 38px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ $contact->date_mise_en_service }}</span></h5>
								<h5 class="text-uppercase"style="color:black; ">Model:  <span style="margin-left: 140px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ $contact->modele}}</span></h5>
								<h5 class="text-uppercase"style="color:black; ">SERIAL NUMBER: <span style="margin-left: 78px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ $contact->numero_serie }}</span></h5>
								
										@if (Auth::user()->can('equipement-update'))
										<a href="{{ route('equipement.edit', $contact->id) }}" class="btn btn-warning btn-xs">Edit equipment</a>
										<!-- <a href="{{ route('contacts.edit', ['contact' => $contact]) }}" class="btn btn-warning btn-xs">Edit equipment</a> -->
										@endif
										{{-- @if (Auth::user()->can('contact-delete'))
										<form action="{{ route('clients.destroy', $contact->id) }}" method="POST">
											<input type="hidden" name="_method" value="DELETE">
								<input type="submit" name="submit" value="Delete Contact" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure?')">
								{{ csrf_field() }}
							</form>
										@endif --}}
									
									{{-- <div class="progress progress-sm m-0">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
											<span class="sr-only">57% Complete</span>
										</div>
									</div> --}}
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-lg-6" >
							<div class="mini-stat clearfix bx-shadow bg-white" style="height: 250px">
								<span class="mini-stat-icon bg-warning"><i style="color:black" class="ion-android-contacts"></i></span><br>
								<div class="mini-stat-info text-right text-dark" >
									<span  style="color:black">Customer information</span><br>
								</div>
								<div class="tiles-progress">
									<div class="m-t-20">
									    <?php $client = DB::table('clients')->where('id',$contact->client_id)->first(); ?>
											<h5 class="text-uppercase"style="color:black; ">CLIENT NAME: <span style="margin-left: 70px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ ($client) ? $client->name : '--'}}</span></h5>
											<h5 class="text-uppercase"style="color:black; ">E-MAIL:  <span style="margin-left: 115px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ ($client) ? $client->primary_email : '--' }}</span></h5>
											<?php $payss = DB::table('pays')->where('id', ($client) ? $client->pay_id : '--')->first() ?>
											<h5 class="text-uppercase"style="color:black; ">Country: <span style="margin-left: 100px" class="mini-stat-info text-right text-dark" style="text-align:right">{{ ($payss) ? $payss->nom : '--' }}</span></h5>
											<a href="{{ route('clients.edit', ['contact' => $contact]) }}" class="btn btn-warning btn-xs">Edit Customer</a>

									</div>
								</div>
							</div>
						</div>
		</div>
		<?php $abonnemntsabf = DB::table('abo_equipements')->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->get() ?>
        @if($abonnemntsabf->isEmpty())
        <div><p></p></div>
        @else
		<div class="col-sm-12 col-lg-12">
				<div class="mini-stat clearfix bx-shadow bg-white">
						<span class="mini-stat-icon bg-warning"><i style="color:black" class="fa  fa-file"></i></span>
						<div class="mini-stat-info text-right text-dark" >
							<span  style="color:black">Subscription Information</span><br>
							<?php $abonnemnts = DB::table('abo_equipements')->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->get() ?>
              <!-- @foreach($abonnemnts as $abonnemnt)
							@if($abonnemnt->end_statut == 0)
							<?php $abonnemensss = DB::table('abonnements')->where('id', $abonnemnt->abonnement_id)->get() ?>
              @foreach($abonnemensss as $abos)
							<h5 class="text-uppercase"style="color:black; ">Subscription name:  
							<span style="margin-left: 17px" class="mini-stat-info text-right text-dark" style="text-align:right">{{$abos->nom}} </span></h5>
							<b><span style="margin-left: 160px; color:green; text-align:right">Next</span></b> 
							@endforeach
							@else
							<?php $abonnemensss = DB::table('abonnements')->where('id', $abonnemnt->abonnement_id)->get() ?>
              @foreach($abonnemensss as $abos)
							<h5 class="text-uppercase"style="color:black; ">Subscription name:  
							<span style="margin-left: 17px" class="mini-stat-info text-right text-dark" style="text-align:right">{{$abos->nom}} </span></h5>
							<b><span style="margin-left: 160px; color:red; text-align:right">Expiré</span></b> 
							@endforeach
							@endif
							@endforeach -->
						</div>
					<div class="tiles-progress" >
						<div class="m-t-20" >

						<div class="col-sm-12 col-lg-6">
						<h5>En cours</h5>
						<table id="example" class="table table-striped table-bordered" style="width:100%">
						<thead>
						<tr>
						<th>Nom</th> 
						<th>Charged To</th> 
						<th>Date de debut</th> 
						<th>Date de fin</th> 
						</tr>
						</thead>
						<tbody>
						<?php $abonnemnts = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->paginate(1); $today = date('Y-m-d'); ?>
						@if($abonnemnts->isEmpty())
						<td style="margin-left: 160px; color:black; text-align:left">No subscription en cours</td>
						@else
						@foreach($abonnemnts as $abonnemnt)
						@if($today >= $abonnemnt->date_debut && $today <= $abonnemnt->date_fin)
						<tr>
						<!-- @if($abonnemnt->end_statut == 0) -->
						<?php $abonnemensss = DB::table('abonnements')->where('id', $abonnemnt->abonnement_id)->get() ?>
						@foreach($abonnemensss as $abonnemenss)
						<td style="margin-left: 160px; color:green; text-align:left">{{$abonnemenss->nom}} </td>	
						<td style="margin-left: 160px; color:green; text-align:left">{{$abonnemnt->chargerto}}</td>					
						<td style="margin-left: 160px; color:green; text-align:left">{{$abonnemnt->date_debut}} </td>	
						<td style="margin-left: 160px; color:green; text-align:left">{{$abonnemnt->date_fin}} </td>
					  @endforeach
					<!-- 	@endif -->
					
						@endif
						@endforeach
						@endif
						<?php $abnts = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->paginate(1) ?>
						@foreach($abnts as $abnt)
						<?php $abonmnts = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->where('id','!=', $abnt->id)->orderBy('date_fin', 'ASC')->get() ?>
            
						@foreach($abonmnts as $abonmnt)
						<tr>
						<!-- @if($abonmnt->end_statut == 0) -->
						<?php $abonemensss = DB::table('abonnements')->where('id', $abonmnt->abonnement_id)->get() ?>
						@foreach($abonemensss as $abonemenss)
						<td style="margin-left: 160px; color:gray; text-align:left">{{$abonemenss->nom}}</td>	
						<td style="margin-left: 160px; color:gray; text-align:left">{{$abonnemnt->chargerto}}</td>					
						<td style="margin-left: 160px; color:gray; text-align:left">{{$abonmnt->date_debut}}</td>	
						<td style="margin-left: 160px; color:gray; text-align:left">{{$abonmnt->date_fin}}</td>	

					  @endforeach
						<!-- @endif -->
						</tr>
						@endforeach
						@endforeach
						</tbody>
						</table>
          </div>

					<div class="col-sm-12 col-lg-6">
					<h5>Expiré</h5>
						<table id="example" class="table table-striped table-bordered" style="width:100%">
						<thead>
						<tr>
						<th>Nom</th> 
						<th>Charged To</th> 
						<th>Date de debut</th> 
						<th>Date de fin</th>
						</tr>
						</thead>
						<tbody>
						<?php $abonnemnts = DB::table('abo_equipements')->where('end_statut', '=', 1)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->get() ?>
						@if($abonnemnts->isEmpty())
						<td style="margin-left: 160px; color:black; text-align:left">No subscription expiré</td>
						@else
						@foreach($abonnemnts as $abonnemnt)
						<tr>
						<?php $abonnemensss = DB::table('abonnements')->where('id', $abonnemnt->abonnement_id)->get() ?>
						@foreach($abonnemensss as $abonnemenss)
						<td style="margin-left: 160px; color:red; text-align:left">{{$abonnemenss->nom}}</td>	
						<td style="margin-left: 160px; color:red; text-align:left">{{$abonnemnt->chargerto}}</td>					
						<td style="margin-left: 160px; color:red; text-align:left">{{$abonnemnt->date_debut}}</td>	
						<td style="margin-left: 160px; color:red; text-align:left">{{$abonnemnt->date_fin}}</td>	
					  @endforeach				
						</tr>
						@endforeach
						@endif
						</tbody>
						</table>
					</div>


						</div>
					</div>
				</div>			
		    </div>
@endif

	
	</div>

@endsection