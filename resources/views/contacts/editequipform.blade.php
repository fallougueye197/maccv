<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

<div class="row">
    <div class="form-group col-md-6" style="margin-left:5px;">
        {!! Form::label('constructeur', __('Builder'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
        <input type="text" name="constructeur" value="{{$contact->constructeur}}" class="form-control" placeholder="Builder" aria-label="First name" required>
    </div>
    <div class="form-group col-md-6" style="margin-left:5px;">
        {!! Form::label('famille', __('Family'), ['class' => 'control-label']) !!} <span class="text-danger">*</span>
        <input type="text" name="famille" value="{{$contact->famille}}" class="form-control" placeholder="Family" aria-label="First name" required>
    </div>
</div>  
<div class="row">
    <div class="form-group col-md-6 col-sm-6" style="margin-left:5px;">
        {!! Form::label('sous_famille', __('subfamily'), ['class' => 'control-label']) !!}
        <input type="text" name="sous_famille" value="{{$contact->sous_famille}}" class="form-control" placeholder="subfamily" aria-label="First name">
    </div>
     <div class="form-group col-md-6 col-sm-6" style="margin-left:5px;">
        {!! Form::label('numero_serie', __('Serial number'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        <input type="text" name="numero_serie" value="{{$contact->numero_serie}}" class="form-control" placeholder="Serial number" aria-label="First name">
    </div>
    <!-- <div class="form-group col-md-6 col-sm-6" style="margin-left:5px;">
        {!! Form::label('segment', __('Charged to'), ['class' => 'control-label']) !!}
        <input type="text" name="segment" value="{{$contact->segment}}" class="form-control" placeholder="Charged to" aria-label="First name">
    </div> -->
    <div class="form-group col-md-6 col-sm-6" style="margin-left:5px;">
        {!! Form::label('compteur', __('Hours'), ['class' => 'control-label']) !!}
        <input type="text" name="compteur" value="{{$contact->compteur}}" class="form-control" placeholder="Hours" aria-label="First name">
    </div>
</div>
<div class="row">    <div class="form-group col-md-6" style="margin-left:5px;">
        {!! Form::label('modele', __('Model'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        <input type="text" name="modele" value="{{$contact->modele}}" class="form-control" placeholder="Model" aria-label="First name">
    </div>
    <div class="form-group col-md-6" style="margin-left:5px;">
        {!! Form::label('pays', __('Country'), ['class' => 'control-label']) !!} <span class="text-danger">*</span>
        <?php $payss = DB::table('pays')->get() ?>
        <?php $pa = DB::table('pays')->where('id',$contact->pay_id)->first() ?>
            <select id="inputState" name="pay_id" class="form-select">
            <option value="{{$contact->pay_id}}">{{ ($pa) ? $pa->nom : '--'}}</option>
        @foreach($payss as $pay)
            <option value="{{$pay->id}}">{{$pay->nom}}</option>
        @endforeach
            </select>
    </div></div>
        <div class="row">
            <div class="form-group col-md-6" style="margin-left:5px;">
        {!! Form::label('date_mise_en_service', __('Date of commissioning'), ['class' => 'control-label']) !!}
        <input type="date" name="date_mise_en_service" value="{{date('Y-m-d', strtotime($contact->date_mise_en_service))}}" class="form-control" placeholder="Date of commissioning" aria-label="First name">
    </div>            
    <div class="form-group col-md-6" style="margin-left:5px;">
                {!! Form::label('client_id', __('Assign Client'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
                <?php $clientss = DB::table('clients')->get() ?>
                <?php $cli = DB::table('clients')->where('id',$contact->client_id)->first() ?>
                <select id="gettState" name="client_id" class="form-select">
                <option value="{{$contact->client_id}}">{{$cli->name}}</option>
            @foreach($clientss as $clien)                
            <option value="{{$clien->id}}" id="getvalue">{{$clien->name}}</option>         
                @endforeach        
            </select>
            </div>
        </div>
  <div class="row">        
         <div class="form-group col-sm-6" style="margin-left:5px;">
            {!! Form::label('boitier_id', __('PL'), ['class' => 'control-label']) !!}<span class="text-danger"></span>
            <?php $boitierss = DB::table('boitiers')->where('statut', '=', 0)->get() ?>
            <?php $boi = DB::table('boitiers')->where('id',$contact->boitier_id)->first() ?>
                <select id="inputState" name="boitier_id" class="form-select">
                <option value="{{$contact->boitier_id}}">{{$boi->reference}}</option>
            @foreach($boitierss as $boit)
                <option value="{{$boit->id}}">{{$boit->reference}}</option>
            @endforeach
                </select>
        </div>
        <!--<div class="form-group col-sm-6" style="margin-left:5px;">
            {!! Form::label('abonnement', __('Subscriptions'), ['class' => 'control-label']) !!}<span class="text-danger"></span>
            <?php $abonnementss = DB::table('abonnements')->get() ?>
                
        </div>-->
        <div class="form-group col-sm-6" style="margin-left:5px;">
        {!! Form::label( 'type', __("Type of Equipment "), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
           <select id="inputState" name="type" class="form-select">
                <option value="{{$contact->type}}">{{$contact->type}}</option>
                <option value="Machine">Machine</option>
                <option value="Moteur">Moteur</option>
            </select>
        </div>
  </div>
 
    <div class="tab-pane" id="tab2">
           <h5 class="card-title" style="font-family: 'poppins', sans-serif; font-size : 16px; color : black;">Ajouter une tâche à cette activité</h5>
            <br>
            <table class="table">
                <thead>
                    <tr>
                    <th  class="text-nice">Abonnement<span class="red">*</span></th>
                        <th  class="text-nice">Charged to<span class="red"></span></th>
                     
                        <th  class="text-nice">Date de début<span class="red">*</span></th>
                        <th  class="text-nice">Date de fin<span class="red">*</span></th>
                        <th><a href="#" class=" btn btn-dark addRow"><i class="bi bi-plus-circle-fill"></i> Ajouter plus de abonnements </a></th>
                    </tr>
                </thead>
                <tbody>
                <?php $abonnementssss = DB::table('abo_equipements')->where('contact_id',$contact->id)->get() ?>
                @foreach($abonnementssss as $abo)
                    <tr>
                    <td>
                       
                        <?php $abonnementss = DB::table('abonnements')->where('statut', '=', 0)->get() ?>
                        <?php $abonne = DB::table('abonnements')->where('id',$abo->abonnement_id)->get() ?>
                        
                                <select id="inputState" name="abonnement_id[]" class="form-select">
                                @foreach($abonne as $ab)
                                <option value="{{$abo->abonnement_id}}">{{ ($ab) ? $ab->nom : '--'}}</option>
                                @endforeach
                            @foreach($abonnementss as $abon)
                                <option value="{{$abon->id}}">{{ ($abon) ? $abon->nom : '--'}}</option>
                            @endforeach
                           
                                </select>
                        </td>
                        <td>
                        <?php $chargeds = DB::table('chargeds')->where('nom', '!=', 'pas')->where('nom', '!=', 'No')->get() ?>
                         <select id="inputState" name="chargerto[]" class="form-select">
                                <option value="{{$abo->chargerto}}">{{ ($abo) ? $abo->chargerto : '--' }}</option>
                                @foreach($chargeds as $charged)
                                <option value="{{$charged->nom}}">{{ ($charged) ? $charged->nom : '--'}}</option>
                                 @endforeach
                                </select>
                        </td>
                       
                        <td>
                        <input type="date" name="date_debut[]" value="{{$abo->date_debut}}" class="form-control" placeholder="Date de début" aria-label="First name">
                        </td>
                        <td>
                        <input type="date" name="date_fin[]" value="{{$abo->date_fin}}" class="form-control" placeholder="Date de fin" aria-label="First name">
                        <input type="hidden" id="task" value="{{$abo->id}}">

                        </td>
                        <td>
                             <form action="{{route('destroy.abo', $abo->id)}}" id="demo-upload" method="POST">
                             {{csrf_field()}}
                              <button class="btn btn-danger" onclick="shareabo()"><i class="bi bi-trash"></i></button>
                              </form>
                        </td>
                    </tr>
                    @endforeach
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td style="border: none"></td>
                        <td style="border: none"></td>
                        <td style="border: none"></td>
                        <td style="border: none"></td>
                        <td style="border: none"></td>
                        <td style="border: none"></td>
                    </tr>
                </tfoot>
            </table>
                <hr>
        </div>        <button type="submit" class="btn btn-primary">Create New Equipment</button>