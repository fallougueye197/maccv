@extends('layouts.master')
@section('heading')
    <h1>{{__('My Contacts')}}</h1>
@stop

@section('content')
    <table class="table table-striped table-bordered" id="contacts-table">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Job Title') }}</th>
            <th>{{ __('Client') }}</th>
            <th>{{ __('Email') }}</th>
            <th>{{ __('Primary Number') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
        </thead>
        <tbody>
         @foreach($contacts as $contact)
              
                    
              
                    <tr>
                    <td>{{$contact->numero_serie}}</td>
                    <td>{{$contact->modele}}</td>
                    <td>{{$contact->client->name}}</td> 
                    <td>{{$contact->pays}}</td>
                    <td>
                        @if (Auth::user()->can('equipement-update'))
						<a href="{{ route('contacts.edit', ['contact' => $contact]) }}" class="btn btn-sm btn-warning">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
						@endif
                        
                    </td>

                    
                    </tr>
                     
                @endforeach
            </tbody>
    </table>
@stop

@push('scripts')
<script>
    $(function () {
        $('#contacts-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('contacts.mydata') !!}',
            columns: [
                {data: 'namelink', name: 'name'},
                {data: 'job_title', name: 'job_title'},
                {data: 'client_name', name: 'client_name'},
                {data: 'emaillink', name: 'email'},
                {data: 'primary_number', name: 'primary_number',},
                {data: 'actions', name: 'actions', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush
