@extends('layouts.master')
@section('heading')
{{-- cdn for extraction --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
{{--  --}}
    <h1>{{__('Facturations')}}</h1>

@stop

@section('content')
<div style="display:flex;">
<div cass="forms">
<form action="{{route('filterfac')}}" method="get" style="margin-top:5px;">
            <select name="filterfacp" style="width:500px;">
                <option value="" disabled selected>Filtrer Par pays</option>
                <?php $pays = DB::table('pays')->get() ?>
                @foreach($pays as $conta)
                <option value="{{$conta->id}}">{{$conta->nom}}</option>
                @endforeach
            </select>



           <select name="filterfacc" style="width:200px;" required>
                <option value="" disabled selected>Filtrer par Charged to</option>
               
                @foreach($charged as $charge)
                <option value="{{$charge->nom}}">{{$charge->nom}}</option>
                @endforeach
            </select>
            

                <button class="btn btn-warning" style="color:noir;" type="submit">Filtrer</button>
                
            </form>    
            </div>
         <div cass="button" style="margin-left:10px;margin-top:5px;"> 
            <button class="btn btn-default" style="color:noir;"><a href="/contacts/filterfac?filterfacc=No">Reset</a></button>
            </div>
          </div>
    <table id="example" class="table table-striped table-bordered" style="width:100%; margin-top:20px;">
        <thead> 
    <tr>
            <!--<th>{{ __('Pays') }}</th>-->
           <!-- <th>{{ __("ID interne client") }}</th>
            <th>{{ __('ID ext 1 client') }}</th>
            <th>{{ __('ID ext 2 client') }}</th>-->
            <th>{{ __("Charged To") }}</th>
            <th>{{ __("Nombre d'equipements facturés") }}</th>
            <th>{{ __("Montant d'equipements facturés") }}</th>
           
        </tr>
        </thead>
            <tbody>
          <?php $today = date('Y-m-d'); $i=0; ?>
                @foreach($chargeds as $contact)
                
               @php  
                if($i >= 1)
                    break; 
                @endphp
                
                    @php $abonnementsfg = 0; @endphp
                    @if($chargeds->isEmpty())
                    <p>No Result</p>
                    @else
                    
               
                  
                 
             
                    <tr>
                      
                   
                   
                   
                    <td>{{$contact->nom}}</td>
                     <td>{{count($chargeds)}}</td>
                     <td>{{$tarif}}</td>
                  
                    </tr>
                      @php  $i++; @endphp
                
                    
                    @endif   
                @endforeach
            </tbody>
    </table>
     
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
       
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

        </script>
    
@stop

@push('scripts')
<script>
    
   
</script>



@endpush

