@extends('layouts.master')

@section('heading')
    <h1>{{ __('Update Equipement') }} ({{ $contact->constructeur }})</h1>
@stop

@section('content')
    {!! Form::model($contact, ['method' => 'PATCH', 'route' => ['contacts.update', $contact->id]]) !!}
      @include('contacts.form', ['submitButtonText' => __('Update equipement')])
    {!! Form::close() !!}
@stop