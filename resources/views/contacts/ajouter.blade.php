@extends('layouts.master_form')
@section('heading')
    <h1>{{ __('Create Equipment') }}</h1>
@stop
@section('content')    
    <form action="{{route('equipement.abonnement')}}" method="post" class="form-horizontal" id="demo1-upload">
        {{ csrf_field() }}
        @include('contacts.equipform')    
    </form>
@stop