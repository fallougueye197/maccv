@extends('layouts.master')

@section('heading')
    <h1>{{ __('Edite Equipement') }}</h1>
@stop

@section('content')
{{-- <head>
    <title>Laravel 5.6 Import Export to Excel and csv Example - ItSolutionStuff.com</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
</head> --}}

        <div class="panel panel-default">
          <div class="panel-heading">
          <h1>1) Download the excel template
2) Fill in the excel file with at least the mandatory fields (in bold) <br>
3) Save the excel file <br>
4) Click on the button Upload file to choose the excel spreadsheet <br>
5) Click on the button Import File.</h1>
          </div>
          <div class="panel-body">

 
            {{-- <a href="{{ url('downloadExcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
            <a href="{{ url('downloadExcel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
            <a href="{{ url('downloadExcel/csv') }}"><button class="btn btn-success">Download CSV</button></a>
  --}}
            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('contacts/updateMasse') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

 
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
 
                @if (Session::has('success'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif
 
                <input type="file" name="import_file" id="date" onfocus="checkEmpty()" onblur="checkEmpty()" onkeyup="checkEmpty()" />
                <button class="btn btn-primary" id="soumettre" disabled="disabled">Import File</button>
            </form>
 
          </div>
        </div>
        <script>

        function checkEmpty(){
            if(document.getElementById('date').value != ""){
               document.getElementById('soumettre').disabled = "";
            }
            else{
               document.getElementById('soumettre').disabled = "disabled";
            }
         }
        </script>
        @if (Session::has('get_html'))
        <script type = "text/javascript">
        
        $(document).ready(function(){
        var download_data = '<?= Session::get("download_data")?>' ;
        $('#error_show').html(get_html);
        toast.error('There were some errors', {
        "showDuration":"100",
        "timeOute" : "800",
        "inconClass": "toast toast-success",
        "closeButton": true,
        "position" : "toast-top-right"
        });
        setTimeout(function(){
        $('#error_data').modal('show');
        $('#download_data').val(download_data);
        }, 3000);
        setTimeout(function(){
        $('#error_data').modal('hide');
        var redrawtable = $('#'+table).dataTable();
        redrawtable.fnStandingRedraw();
        location.reload();
        },15000);
        });
        </script>
        @endif
   
@stop