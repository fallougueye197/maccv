@extends('layouts.master')

@section('heading')
    <h1>{{ __('Create Equipment') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'contacts.store']) !!}
        @include('contacts.form', ['submitButtonText' => __('Create New Equipment')])
    
    {!! Form::close() !!}
    
@stop
