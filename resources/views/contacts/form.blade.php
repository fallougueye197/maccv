<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('constructeur', __('Builder'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
        {!! Form::text('constructeur', $data['constructeur'] ?? null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('famille', __('Family'), ['class' => 'control-label']) !!} <span class="text-danger">*</span>
        {!! Form::text('famille', $data['famille'] ?? null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    {{-- <div class="form-group col-md-12">
        {!! Form::label('address1', __('Address 1'), ['class' => 'control-label']) !!}
        {!! Form::text('address1', $data['address1'] ?? null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-12">
        {!! Form::label('address2', __('Address 2'), ['class' => 'control-label']) !!}
        {!! Form::text('address2', $data['address2'] ?? null, ['class' => 'form-control']) !!}
    </div> --}}
    <div class="form-group col-md-6 col-sm-6">
        {!! Form::label('sous_famille', __('subfamily'), ['class' => 'control-label']) !!}
        {!! Form::text('sous_famille', $data['sous_famille'] ?? null, ['class' => 'form-control']) !!}
    </div>
     <div class="form-group col-md-6 col-sm-6">
        {!! Form::label('numero_serie', __('Serial number'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::text('numero_serie', $data['numero_serie'] ?? null, ['class' => 'form-control']) !!}
    </div> 
    <div class="form-group col-md-6 col-sm-6">
        {!! Form::label('segment', __('Segment'), ['class' => 'control-label']) !!}
        {!! Form::text('segment', $data['segment'] ?? null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6 col-sm-6">
        {!! Form::label('compteur', __('Hours'), ['class' => 'control-label']) !!}
        {!! Form::text('compteur', $data['compteur'] ?? null, ['class' => 'form-control']) !!}
    </div> 
</div> 
<div class="row">
   
   
    <div class="form-group col-md-6">
        {!! Form::label('modele', __('Model'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::text('modele', $data['modele'] ?? null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('pays', __('Country'), ['class' => 'control-label']) !!} <span class="text-danger">*</span> 
        {!! Form::select('pays', $pays , null, ['class' => 'form-control', 'placeholder' => 'Selectionnez un pays']) !!}
    </div>
    
    
</div>
    @if(Request::get('client') != "")
        {!! Form::hidden('client_id', Request::get('client')) !!}
    @else
        <div class="row">
            <div class="form-group col-md-6">
        {!! Form::label('date_mise_en_service', __('Date of commissioning'), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::date('date_mise_en_service', $data['date_mise_en_service'] ?? null, ['class' => 'form-control']) !!}
    </div>
         
            <div class="form-group col-md-6">
                {!! Form::label('client_id', __('Assign Client'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
                {!! Form::select('client_id', $clients, $data['client_id'] ?? null, ['class' => 'form-control ui search selection top right pointing search-select']) !!}
            </div>
        </div>
    @endif
  <div class="row">
  
        <div class="form-group col-sm-6">
            {!! Form::label('boitier_id', __('PL'), ['class' => 'control-label']) !!}<span class="text-danger"></span>
            {!! Form::select('boitier_id',$boitiers ,$data['boitier_id'] ?? null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('abonnement', __('Subscriptions'), ['class' => 'control-label']) !!}<span class="text-danger"></span>
            {!! Form::select('abonnement_id', $abonnements,$data['abonnement_id'] ?? null, ['class' => 'form-control']) !!}
        {{-- {!!Form::select('abonnements',$abonnements,isset($contact->abonnement->abonnement_id)? $contact->abonnement->abonnement_id : null,['class' => 'form-control']) !!} --}}
        </div>
  </div>
  <div class="row">

        <div class="form-group col-sm-6">
            {!! Form::label( 'datedebut', __("Start date"), ['class' => 'control-label']) !!}
            {!! Form::date('datedebut', null,['class' => 'form-control']) !!}
        </div>
        <div class="form-group  col-sm-6">
            {!! Form::label( 'datefin', __("End date"), ['class' => 'control-label']) !!}
            {!! Form::date('datefin', null,['class' => 'form-control']) !!}
        </div>
  </div>
  <div class="form-group">
        {!! Form::label( 'type', __("Type of Equipment "), ['class' => 'control-label']) !!}<span class="text-danger">*</span>
        {!! Form::select('type',['Groupe' => 'Groupe','Machine'=>'Machine','Moteur'=>'Moteur'],null, ['class'=>'form-control']) !!}
    </div>
  {{-- <div class="form-group">
{!! Form::Label('abonnement', 'Abonnement:') !!}
<select class="form-control" name="abonnement_id">
@foreach($abonnements as $abonnement)
  <option value="{{$abonnement->abonnement_id}}">{{$abonnement->id}}</option>
@endforeach
</select> 
{{-- {{ Form::select('abonnement_id', \App\Models\Abonnement::all()->pluck('nom', 'abonnement_id')->toArray(), null,['class'=>'select2 form-control', 'multiple'=>'multiple','id' => 'abonnement_id','placeholder' => 'Selectionnez']) }} --}}
{{-- </div>  --}}



   {{-- @if(Request::get('boitier') != "")
        {!! Form::hidden('boitier_id', Request::get('boitier')) !!}
    @else
        <div class="row">
         
            <div class="form-group col-md-6">
                {!! Form::label('boitier_id', __('Assign Boitier'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
                {!! Form::select('boitier_id', $boitiers, $data['boitier_id'] ?? null, ['class' => 'form-control','placeholder' => 'Selectionnez un Boitier']) !!}

            </div> 
        </div>
    @endif  

   
    @if(Request::get('abonnement') != "")
    {!! Form::hidden('abonnement_id', Request::get('abonnement')) !!}
    @else
        <div class="row">
        
            <div class="form-group col-md-6">
                {!! Form::label('abonnement_id', __('Assign Abonnement'), ['class' => 'control-label']) !!}  <span class="text-danger">*</span>
                {!! Form::select('abonnement_id', $abonnements, $data['abonnement_id'] ?? null, ['class' => 'form-control','placeholder' => 'Selectionnez un abonnement']) !!}

            </div> 
        </div>
    @endif   --}}
   


    {!! Form::submit($submitButtonText, ['class' => 'btn btn-lg btn-primary']) !!}