@extends('layouts.master')
@section('heading')
{{-- cdn for extraction --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
{{--  --}}
    <h1>{{__('equipments.Allequipments')}}</h1>

<!--     <h3><a href="/contacts/facturation" class="button" type="button">Facturation</a></h3>
 -->@stop

@section('content')
<div style="display:flex;">
<div cass="forms">
<form action="{{route('filter')}}" method="get" style="margin-top:5px;">
             
            <select name="filter" style="width:200px;">
                <option value="" disabled selected>Filtrer Par pays</option>
                <?php $pays = DB::table('pays')->get() ?>
                @foreach($pays as $conta)
                <option value="{{$conta->id}}">{{$conta->nom}}</option>
                @endforeach
            </select>

            <select name="filters" style="width:200px;">
                <option value="" disabled selected>Filtrer Par type</option>
              
                <option value="Moteur">Moteur</option>
                <option value="Machine">Machine</option>
                
            </select>

            <select name="filt" style="width:200px;">
                <option value="" disabled selected>Filtrer Par PL</option>
                <?php $boitiers = DB::table('boitiers')->get() ?>
                @foreach($boitiers as $boitier)
                <option value="{{$boitier->id}}">{{$boitier->reference}}</option>
                @endforeach
            </select>
            
            <select name="filte" style="width:200px;">
                <option value="" disabled selected>Filtrer Par Abonnement</option>
                <?php $abonnements = DB::table('abonnements')->get() ?>
                @foreach($abonnements as $abonnement)
                <option value="{{$abonnement->id}}">{{$abonnement->nom}}</option>
                @endforeach
            </select>
            

                <button class="btn btn-warning" style="color:black;" type="submit">Filtrer</button>
                
                

            </form> 
            </div>
         <div cass="button" style="margin-left:10px;margin-top:5px;">   
            <button class="btn btn-secondary" ><a href="/contacts" style="color:black">Reset</a></button>
            
            </div>
          </div>  
    <table id="example" class="table table-striped table-bordered" style="width:100%; margin-top:20px;">
        <thead> 
    <tr>
            <th>{{ __("equipments.TypeOfEquipement") }}</th>
            <th>{{ __('equipments.Dateofcommissioning') }}</th>
            <th>{{ __('equipments.Serialnumber') }}</th>
            <th>{{ __("equipments.Clientsname") }}</th>
            <th>{{ __("equipments.Subscriptionsname") }}</th>
            <th>{{ __("equipments.pl") }}</th>
            <th>{{ __('equipments.Startdate') }}</th>
            <th>{{ __('equipments.Enddate') }}</th>
            <th>{{ __('equipments.Country') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
        </thead>
            <tbody>
             {{-- {{$ize = DB::table('contacts')->pluck('client_id')}}
             {{$oze = DB::table('clients')->pluck('user_id')}}
{{  $aze = Auth::user()->name}}              --}}
                {{-- @if ($contact->client->user_id == Auth::user()->id)
                     @endif  --}}
               <?php $today = date('Y-m-d'); ?>
               
               <?php DB::table('abo_equipements')->where('date_fin', '<', $today)->update(array('end_statut' => 1)); ?>
               
                @foreach($contacts as $contact)
                <?php DB::table('abo_equipements')->where('contact_id', '=', $contact->id)->update(array('pays_id' => $contact->pay_id)); ?>
                @if($contacts->isEmpty())
                <p>No Result</p>
                @else
                  @if(Auth::user()->pays == 'Reseau')
             
                    <tr>
                    <td>{{$contact->type}}</td>
                    <td>{{$contact->date_mise_en_service}}</td>
                    <td>{{$contact->numero_serie}}</td>
                   
                    
                     <?php $clients = DB::table('clients')->where('id', $contact->client_id)->first() ?>
                     
                    
                    <td>{{ ($clients) ? $clients->name : "--"}}</td>
                    
                    
                    <?php $abonnements = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->paginate(1) ?>
                    <td>
                    @if($abonnements->isEmpty())
                    <br> <b><span style="color:red;font-size:10px;">Abonnement Expiré</span></b> 
                    @else
                    @foreach($abonnements as $abonnement)
                    <?php $abonnemen = DB::table('abonnements')->where('id', $abonnement->abonnement_id)->paginate(1) ?>
                    @foreach($abonnemen as $abo)
                    {{$abo->nom}} 
                    @endforeach
                    @if($today < $abonnement->date_debut)
                    <br> <b><span style="color:green;font-size:10px;">En Attente</span></b> 
                    @elseif($today >= $abonnement->date_debut && $today <= $abonnement->date_fin)
                    <br> <b><span style="color:green;font-size:10px;">En cours</span></b> 
                    <br> <b>
                    <form action="{{route('valider', $abonnement->id)}}" method="post" id="target" class="form">
                        <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                        <span class="d-inline-block" tabindex="0">
                            <button type="submit" id="PopoverCustomT-1" class="btn btn-warning boutton-options">
                                End
                            </button>
                        </span>
                    </form>
                    </b>
                    @endif
                    @endforeach
                    @endif
                    </td>
                     <?php $boitiers = DB::table('boitiers')->where('id', $contact->boitier_id)->get() ?>
                    @foreach($boitiers as $boitier)
                    <td>{{$boitier->reference}}</td>
                    @endforeach
                    
                  <?php $abonnents = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->paginate(1) ?>
                    @if($abonnents->isEmpty())
                    <td><b><span style="color:red;font-size:10px;">Date Expirée</span></b></td>
                    <td><b><span style="color:red;font-size:10px;">Date Expirée</span></b></td>
                    @else
                    @foreach($abonnents as $abonnent)
                    <td>{{$abonnent->date_debut}}</td>
                    <td>{{$abonnent->date_fin}}</td>
                    @endforeach 
                    @endif
                    <?php $payss = DB::table('pays')->where('id',$contact->pay_id)->first() ?>
                       
                        <td>{{ ($payss) ? $payss->nom : "--"}}</td>

                    <td>
                        @if (Auth::user()->can('equipement-update'))
                        <a href="{{ route('equipement.edit', $contact->id) }}" class="btn btn-sm btn-warning">
					
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                        <a href="{{ route('contacts.show',  $contact->id) }}" class="btn btn-sm btn-warning">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
						@endif
                        
                    </td>
                    </tr>
                    @endif
                    <?php $pa = DB::table('pays')->where('id', $contact->pay_id)->get(); ?>
                    @foreach($pa as $p)
                    @if($p->nom == Auth::user()->pays)
             
                        <tr>
                            <td>{{$contact->type}}</td>
                            <td>{{$contact->date_mise_en_service}}</td>
                            <td>{{$contact->numero_serie}}</td>
                                   <?php $clients = DB::table('clients')->where('id', $contact->client_id)->get() ?>
                                    @foreach($clients as $client)
                                <td>{{$client->name}}</td>
                                @endforeach
                    
                                <?php $abonnements = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->paginate(1) ?>
                                <td>
                                @if($abonnements->isEmpty())
                                <br> <b><span style="color:red;font-size:10px;">Abonnement Expiré</span></b> 
                                @else
                                        @foreach($abonnements as $abonnement)
                                                <?php $abonnemen = DB::table('abonnements')->where('id', $abonnement->abonnement_id)->paginate(1) ?>
                                                @foreach($abonnemen as $abo)
                                                {{$abo->nom}} 
                                                @endforeach
                                        @if($abonnement->date_debut > $today)
                                        <br> <b><span style="color:green;font-size:10px;">En Attente</span></b> 
                                       @elseif($today >= $abonnement->date_debut && $today <= $abonnement->date_fin)
                                        <br> <b><span style="color:green;font-size:10px;">En cours</span></b> 
                                        <br> <b>
                                        <form action="{{route('valider', $abonnement->id)}}" method="post" id="target" class="form">
                                            <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                                            <span class="d-inline-block" tabindex="0">
                                                <button type="submit" id="PopoverCustomT-1" class="btn btn-danger boutton-options">
                                                    End
                                                </button>
                                            </span>
                                        </form>
                                        </b>
                                        @endif
                                        @endforeach
                                @endif
                                </td>
                                    <?php $boitiers = DB::table('boitiers')->where('id', $contact->boitier_id)->get() ?>
                                    @foreach($boitiers as $boitier)
                                    <td>{{$boitier->reference}}</td>
                                    @endforeach
                        
                                    <?php $abonnents = DB::table('abo_equipements')->where('end_statut', '=', 0)->where('contact_id', $contact->id)->orderBy('date_fin', 'ASC')->paginate(1) ?>
                                    @if($abonnents->isEmpty())
                                    <td><b><span style="color:red;font-size:10px;">Date Expirée</span></b></td>
                                    <td><b><span style="color:red;font-size:10px;">Date Expirée</span></b></td>
                                    @else
                                            @foreach($abonnents as $abonnent)
                                            <td>{{$abonnent->date_debut}}</td>
                                            <td>{{$abonnent->date_fin}}</td>
                                            @endforeach 
                                                    @endif
                                    <?php $payss = DB::table('pays')->where('id',$contact->pay_id)->get() ?>
                                        @foreach($payss as $pa)
                                        <td>{{ $pa->nom }}</td>
                                        @endforeach
                                    <td>
                                        @if (Auth::user()->can('equipement-update'))
                						<a href="{{ route('equipement.edit', $contact->id) }}" class="btn btn-sm btn-warning">
                					
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                        <a href="{{ route('contacts.show', $contact->id) }}" class="btn btn-sm btn-warning">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                						@endif
                        
                                   </td>
                    </tr>
                     @endif
                     @endforeach
                   
                    
                    @endif  
                    
                @endforeach
            </tbody>
    </table>
     <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="{{ route('equipement.abonn')}}">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
       
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

        </script>
    
@stop

@push('scripts')
<script>
    // $(function () {
    //     $('#contacts-table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax: '{!! route('contacts.data') !!}',
    //         columns: [
    //             {data: 'numero_serie', name: 'numero_serie'},
    //             {data: 'modele', name: 'modele'},
    //             {data: 'client_name', name: 'client_name'},
    //             {data: 'pays', name: 'pays',},
              

    //             {data: 'actions', name: 'actions', orderable: false, searchable: false},
    //         ]
    //     });
    // });
    
   

</script>



@endpush

