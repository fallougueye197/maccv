@extends('layouts.master')

@section('content')
    <div class="col-lg-12 currenttask">
                    <h3>{{ __('header.AllRoles') }}</h3>


        <table class="table table-striped table-bordered">
            <thead>
            <thead>
            <tr>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
            </thead>
            <tbody>

            @foreach($roles as $role)
                <tr>
                    <td>{{$role->display_name}}</td>
                    <td>{{Str_limit($role->description, 50)}}</td>

                    <td>   {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id]]); !!}
                        @if($role->id !== 1)
                        @if(!$roleUser = DB::table('role_user')->pluck('role_id')->contains($role->id))

                            {!! Form::submit(__('Delete'), ['class' => 'btn btn-lg btn-ptimary', 'onclick' => 'return confirm("Are you sure?")']); !!}
                        @endif
                        @endif
                        {!! Form::close(); !!}</td>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

         <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="{{ route('roles.create')}}">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        </button>
        <a href="{{ route('settings.index')}}"
                       class="btn btn-sm btn-warning">{{ __('Settings') }}</a>

    </div>

@stop