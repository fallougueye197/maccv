@extends('layouts.master')
@section('heading')
    <h1>{{ __('All users') }}</h1>
@stop

@section('content')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href=" https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css "> --}}
<link href="{{ URL::asset('frontend/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>{{ __('ID') }}</th>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Profil') }}</th>
            <th>{{ __('Country') }}</th>
            <th>{{ __('status') }}</th>
              @if(Entrust::hasRole('administrator'))
                <th>{{__('Action')}}</th>
                @endif 
        </tr>
        </thead>
        <tbody>
               @foreach($users as $user)

                  <tr>
                    <td>{{ $user->id }}</td>
                     <td>{{ $user->name }}</td>
                
                     <td >{{$user->roles->pluck('name')}}</td>
                     <td>{{ $user->pays }}</td>
                     <td class="center">
							@if($user->statut==0)
							<span class="label label-warning">Active</span>
							@else
                                <span class="label label-default">Unactive</span>
							@endif
					 </td> 
                                @if(Entrust::hasRole('administrator'))
                     <td class="center">
                            @if($user->statut==1)
							<a class="btn btn btn-default" href="{{URL::to('/unactive_user/'.$user->id)}}">
                                <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></a>
                            @else
                             <a class="btn btn-warning" href="{{URL::to('/active_user/'.$user->id)}}">
                                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></a>
                            @endif
                            <a class="btn btn-sm btn-warning" href="/users/{{$user->id}}/edit"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                          <a class="btn btn-sm btn-warning"  href="/users/{{$user->id}}/delete"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    
                      {{-- <a class="btn btn-sm btn-danger" href="/users/{{$user->id}}"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> --}}
                    </td>
                     @endif
                  </tr>
               @endforeach

            </tbody>
    </table>
    <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
        <a class="btn btn-lg btn-warning" href="/users/create">
            <font style = "vertical-align: inherit;">
            <font style = "vertical-align: inherit;">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
            </font>
            </font> 
        </a> 
    
    </button>



    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        {!! Form::open(['route' => ['users.destroy', 'delete'], 'method' => 'delete']) !!} <!-- and invalid ID is intentionally set here -->
        {!! Form::hidden('id', '', ['id' => 'client-id']) !!}
        <div class="modal-dialog" role="document">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding:35px 50px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-lock"></span> Handle deletion of user</h4>
                </div>
                <div class="modal-body" style="padding:40px 50px;">

                    <!--HANDLE TASKS-->
                    <div class="form-group">
                        {{ Form::label('user_clients', __('Choose a new user to assign the clients')) }} <span class="glyphicon glyphicon-exclamation-sign text-danger" data-toggle="tooltip" title="Deleting all clients also deletes ALL tasks and leads assigned to that client, regardless of the user they are assigned to."></span>
                        {{ Form::select('user_clients', $users, null, ['class' => 'form-control']) }}
                    </div>

                    <!--HANDLE LEADS-->
                    <div class="form-group">
                        {{ Form::label('user_leads', __('Choose a new user to assign the leads')) }}
                        {{ Form::select('user_leads', $users, null, ['class' => 'form-control', 'placeholder' => 'Delete All Leads']) }}
                    </div>

                    <!--HANDLE CLIENTS-->
                    <div class="form-group">
                        {{ Form::label('user_tasks', __('Choose a new user to assign the tasks')) }}
                        {{ Form::select('user_tasks', $users, null, ['class' => 'form-control', 'placeholder' => 'Delete All Tasks']) }}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                    <button type="submit" id="confirm_delete" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-off"></span> Delete</button>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div> 
   
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
   

    <script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
    </script> 
   
    {{-- <script>
        $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
    </script> --}}

@stop

@push('scripts')
{{-- <script>

$(function () {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('users.data') !!}',
        columns: [
            {data: 'namelink', name: 'name'},
            {data: 'matricule', name: 'matricule'},
            {data: 'pays', name: 'pays'},
            {data:'statut': statut, 'user_id': user_id}
            
            // {data: 'matricule', name: 'matricule'},
           

            @if(Entrust::can('user-update'))
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
            @endif
            @if(Entrust::can('user-delete'))
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            @endif
        ]
    });
});

$(function() {
    $('#myModal').on("show.bs.modal", function (e) {
         $("#client-id").val($(e.relatedTarget).data('client_id'));
    });
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script> --}}


@endpush

 