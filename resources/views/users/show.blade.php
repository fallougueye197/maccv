@extends('layouts.master')

@section('content')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href=" https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css "> --}}
<link href="{{ URL::asset('frontend/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('frontend/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<div class="col-sm-12">
     <h3>{{ __('Customers assigned') }}</h3>
         <table class="table table-striped" id="clients-table">
               
                <thead>
                <tr>
                    <th>{{ __(" Customers' name") }}</th>
                    <th>{{ __('Email') }}</th>                    
                    <th>{{ __('Country') }}</th>
                   <th>{{ __('BU') }}</th>
                   <th>{{ __('Identification Number') }}</th>



                </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->pays }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            
    
  </div>
 <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
   

    <script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
    </script> 
   
    {{-- <script>
        $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
    </script> --}}

@stop

@push('scripts')
{{-- <script>

$(function () {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('users.data') !!}',
        columns: [
            {data: 'namelink', name: 'name'},
            {data: 'matricule', name: 'matricule'},
            {data: 'pays', name: 'pays'},
            {data:'statut': statut, 'user_id': user_id}
            
            // {data: 'matricule', name: 'matricule'},
           

            @if(Entrust::can('user-update'))
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
            @endif
            @if(Entrust::can('user-delete'))
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            @endif
        ]
    });
});

$(function() {
    $('#myModal').on("show.bs.modal", function (e) {
         $("#client-id").val($(e.relatedTarget).data('client_id'));
    });
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script> --}}


@endpush

 
