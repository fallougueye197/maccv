@extends('layouts.master')

@section('content')
<h1>{{ __("header.Addchargedto") }}</h1>

    {!! Form::open([
            'route' => 'chargeds.store',
            ]) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label( 'nom', __('Charged name'), ['class' => 'control-label']) !!}
        {!! Form::text('nom', null,['class' => 'form-control']) !!}
    </div>
    {!! Form::submit(__("Create"), ['class' => 'btn btn-md btn-ptimary']) !!}

    {!! Form::close() !!}

@endsection
