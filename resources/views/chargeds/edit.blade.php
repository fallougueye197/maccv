@extends('layouts.master')
@section('content')

<h1>{{ __("header.Update") }}</h1>
{!! Form::open(['action' => ['ChargedController@update', $chargeds->id],'method'=> 'POST']) !!}
<div class="form-group">
    {{ Form::label('nom', 'Charged name') }}
    {{ Form::text('nom', $chargeds->nom, ['class'=>'form-control']) }}
    


    
</div>
{{Form::hidden('_method', 'PUT')}}
{!! Form::submit('Update', ['class'=>'btn btn-lg btn-primary']) !!}
{!! Form::close() !!}
@endsection
   