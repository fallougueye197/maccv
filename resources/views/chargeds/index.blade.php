
 @extends('layouts.master')

 @section('content')
     <div class="col-lg-12 currenttask">
         <table class="table table-striped table-bordered">
             <h3>{{ __("header.AllChargedto") }}</h3>
             <thead>
             <thead>
             <tr>
                 <th>{{ __('Name') }}</th>
                 @if(Entrust::hasRole('administrator'))
                     <th>{{ __('Update') }}</th>
                     <th>{{ __('Delete') }}</th>
                 @endif
             </tr>
             </thead>
             <tbody>
 
             @foreach($chargeds as $chargeds)
                 <tr>
                     <td>{{$chargeds->nom}}</td>
                     @if(Entrust::hasRole('administrator')) 
                     <td><a class="btn btn-md btn-warning" href="/chargeds/{{$chargeds->id}}/edit"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                     <td>   {!! Form::open(['method' => 'DELETE','route' => ['chargeds.destroy', $chargeds->id]]); !!}
                        {!! Form::submit( __('Delete'), ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure?")']); !!}
                        {!! Form::close(); !!}
                    </td>
                     @endif
                 </tr>
             @endforeach
 
             </tbody>
         </table>
         <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/chargeds/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>
 
     </div>
 
 @stop
 
    