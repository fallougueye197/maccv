{{-- <div class="container">
    <div class="row justify-content-center">
        <h3>{{__('menu.welcome')}}</h3>
    </div>
</div> --}}

    
<nav id="myNavmenu" style="background-image: url('{{ asset('images/back.png')}}'); color:black" class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm" role="navigation">
       
    <div class="list-group panel" style="background-image: url('{{ asset('images/back.png')}}'); color:black" >
        
        <p class="list-group-item siderbar-top" title="" style="background-image: url('{{ asset('images/back.png')}}');" ><img style="width:100%" src="{{url('images/flarepoint_logo.png')}}" alt="" style="display: block; margin-left: auto; margin-right: auto;"></p>
        <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" 
        href="{{route('users.show', \Auth::id())}}" 
        class=" list-group-item" data-parent="#MainMenu">
        <i class="glyphicon sidebar-icon glyphicon-user"></i><span id="menu-txt">{{__('menu.Profile')}}</span> </a>
        @if(Entrust::hasRole('administrator'))
        <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#settings" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
                    class="glyphicon sidebar-icon glyphicon-cog"></i><span id="menu-txt">{{__('menu.Role')}}</span>
        <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a>
        <div class="collapse" id="settings">
            <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('settings.index')}}"
               class="list-group-item childlist">{{__('menu.Settings')}}</a> 
            <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('roles.index')}}"  
               class="list-group-item childlist">{{__('menu.AllRole')}}</a>
      
        </div>
    @endif
    @if(Entrust::hasRole('administrator'))

       <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#pays" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
            class="sidebar-icon glyphicon glyphicon-globe"></i><span id="menu-txt">{{ __('menu.Country') }}</span>
            <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a> 
            <div class="collapse" id="pays">
                <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('pays.index')}}"
                class="list-group-item childlist">{{ __('menu.AllCountry') }}</a>
                    <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('pays.create')}}"
                    class="list-group-item childlist">{{ __('menu.NewCountry') }}</a> 
             
            </div>
            @endif
            
                @if(Entrust::hasRole('administrator'))

       <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#charged" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
            class="sidebar-icon glyphicon glyphicon-globe"></i><span id="menu-txt">{{ __('menu.Charged') }}</span>
            <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a> 
            <div class="collapse" id="charged">
                <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('chargeds.index')}}"
                class="list-group-item childlist">{{ __('menu.AllCharged') }}</a>
                    <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('chargeds.create')}}"
                    class="list-group-item childlist">{{ __('menu.NewCharged') }}</a> 
             
            </div>
            @endif

            @if(Entrust::hasRole('administrator')) 
      <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#departments" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
                    class="sidebar-icon glyphicon glyphicon-list-alt"></i><span id="menu-txt">{{ __('menu.Departments') }}</span>
        <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a> 
        <div class="collapse" id="departments">
             <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('departments.index')}}"
               class="list-group-item childlist">{{ __('menu.AllDepartments') }}</a> 
                    <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('departments.create')}}"
                   class="list-group-item childlist">{{ __('menu.NewDepartment') }}</a> 
            
          

        </div>
        @endif
        @if(Entrust::can('user-read'))

       <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#user" class="list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
                    class="sidebar-icon fa fa-users"></i><span id="menu-txt">{{ __('menu.Users') }}</span>
        <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a> 
        <div class="collapse" id="user">
            <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('users.index')}}" class="list-group-item childlist">{{ __('menu.AllUsers') }}</a> 
            @if(Entrust::can('user-create'))
                 <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('users.create')}}"
                   class="list-group-item childlist">{{ __('menu.NewUser') }}</a>
            @endif
        </div>
        @endif
        @if(Entrust::can('boitier-read'))

        <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#boitiers" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
            class="glyphicon sidebar-icon glyphicon-scale"></i><span id="menu-txt">{{ __('menu.ProductLink') }}</span>
            <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a> 
            <div class="collapse" id="boitiers">
                <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('boitiers.index')}}" class="list-group-item childlist">{{ __('menu.AllProductLink') }}</a>
                @if(Entrust::can('boitier-create'))
                <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('boitiers.create')}}" class="list-group-item childlist">{{ __('menu.NewPL') }}</a> 
                @endif
            </div>
            @endif
            @if(Entrust::can('abonnement-read'))

            <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#abonnements" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
                        class="glyphicon sidebar-icon glyphicon-file"></i><span id="menu-txt">{{ __('menu.Subscriptions') }}</span>
            <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a>
            <div class="collapse" id="abonnements">
                <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('abonnements.index')}}" class="list-group-item childlist">{{ __('menu.AllSubscriptions') }}</a>
                @if(Entrust::can('abonnement-create'))
    
                 <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('abonnements.create')}}" class="list-group-item childlist">{{ __('menu.NewSubscriptions') }}</a> 
                @endif
    
            </div>
            @endif
            @if(Entrust::can('client-read'))

            <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#clients" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
                        class="glyphicon sidebar-icon glyphicon-tag"></i><span id="menu-txt">{{ __('menu.Customers') }}</span>
            <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a>
            <div class="collapse" id="clients">
    
               <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('clients.index')}}" class="list-group-item childlist">{{ __('menu.AllCustomers') }}</a> 
                @if(Entrust::can('client-create'))
                    <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('clients.create')}}"
                       class="list-group-item childlist">{{ __('menu.NewCustomer') }}</a> 
                       <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="https://masse.wintech.sn/public/client/import-export"
                       class="list-group-item childlist">{{ __('menu.ImportCustomer') }}<span class="pcoded-badge label label-danger">NEW</span></a> 
                @endif
            </div>
            @endif
            @if(Entrust::can('equipement-read'))

            <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="#contacts" class=" list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i
                        class="glyphicon sidebar-icon glyphicon glyphicon-bed"></i><span id="menu-txt">{{ __('menu.Equipments') }}</span>
            <i class="ion-chevron-up  arrow-up sidebar-arrow"></i></a> 
            <div class="collapse" id="contacts">
    
                 <a style="background-image: url('{{ asset('images/back.png')}}'); color:black"href="{{ route('contacts.index')}}" class="list-group-item childlist">{{ __('menu.AllEquipments') }}</a>
                @if(Entrust::can('equipement-create'))
                    <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ route('equipement.abonn')}}"
                       class="list-group-item childlist">{{ __('menu.Newequipment') }}</a>
                       <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="https://masse.wintech.sn/public/equipement/file-import-export"
                       class="list-group-item childlist">{{ __('menu.ImportEquipment') }}<span class="pcoded-badge label label-danger">NEW</span></a>
                       <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="/contacts/filterfac?filterfacc=No" class="list-group-item childlist">{{ __('menu.Billing') }}</a>

                @endif
    
            </div>
            @endif
      <a style="background-image: url('{{ asset('images/back.png')}}') ; color:black" href="{{route('dashboard', \Auth::id())}}" class=" list-group-item" data-parent="#MainMenu"><i
            class="glyphicon sidebar-icon glyphicon-dashboard"></i><span id="menu-txt">{{ __('menu.Dashboard') }}</span> </a>  

  <a style="background-image: url('{{ asset('images/back.png')}}'); color:black" href="{{ url('/logout') }}" class=" list-group-item impmenu" data-parent="#MainMenu"><i
                    class="glyphicon sidebar-icon glyphicon-off"></i><span id="menu-txt">{{ __('menu.SignOut') }}</span> </a> 

    </div>
</nav>

