<!DOCTYPE html>
<html lang="en">
<head>  
    <meta charset="UTF-8">
    <title>{{ config('app.name') }} v{{ config('app.version') }}</title>
    <link href="{{ URL::asset('css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/datatables.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('frontend/plugins/morris.js/morris.css') }}">



    <link href="{{ URL::asset('css/dropzone.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/jquery.atwho.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(elixir('css/app.css')) }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    
    <link href="{{ URL::asset('frontend/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">

    {{-- <link href="{{ URL::asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"> --}}
    <link href="{{ URL::asset('frontend/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/pages.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/menu.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/responsive.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ URL::asset('frontend/js/modernizr.min.js') }}"></script>
</head>
<body style="">

<div id="wrapper"  style=" ">

    <button style="background-color:#ffcd11; color: black" type="button" class="navbar-toggle menu-txt-toggle" ><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>

    <div class="navbar navbar-default navbar-top" style="background-color:white;  color:bla">
        <!--NOTIFICATIONS START-->
<div class="menu" style="background-color:black">

    <div class="notifications-header" style="background-color:#ffcd11; color: black"><p>{{__('Notifications')}}</p> </div>
  <!-- Menu -->
 <ul>
 <?php $notifications = auth()->user()->unreadNotifications; ?>

    @foreach($notifications as $notification)

    <a href="{{ route('notification.read', ['id' => $notification->id])  }}" onClick="postRead({{ $notification->id }})">
    <li>
 {{-- <img src="/{{ auth()->user()->avatar }}" class="notification-profile-image"> --}}
    <p>{{ $notification->data['message']}}</p></li>

    </a>
    @endforeach
  </ul>
</div>

       <div class="dropdown" id="nav-toggle">

            <a id="notification-clock" role="button" data-toggle="dropdown">
                <i  class="glyphicon glyphicon-bell " style= "color:#FBB732"><span  id="notifycount">{{ $notifications->count() }}</span></i>
            </a>
        </div>
        <div>
            <li class="nav-item dropdown">
                <a style="color : black" class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ Config::get('languages')[App::getLocale()] }} 
                </a>
                
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                @foreach (Config::get('languages') as $lang => $language)
                    @if ($lang != App::getLocale())
                            <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}"> {{$language}}</a>
                    @endif
                @endforeach
                </div> 
            </li>
        </div>
                    @push('scripts')
                    <script>
                        $('#notification-clock').click(function(e) {
                        e.stopPropagation();
                        $(".menu").toggleClass('bar')
                        });
                        $('body').click(function(e) {
                        if ($('.menu').hasClass('bar')) {
                            $(".menu").toggleClass('bar')
                        }
                        })
                        id = {};
                                function postRead(id) {
                                    $.ajax({
                                        type: 'post',
                                        url: '{{url('/notifications/markread')}}',
                                        data: {
                                            id: id,
                                        },
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                }

                    </script>
                @endpush
            <!--NOTIFICATIONS END-->
            <button type="button" id="mobile-toggle" class="navbar-toggle mobile-toggle" data-toggle="offcanvas" data-target="#myNavmenu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


    <!-- /#sidebar-wrapper -->
    <!-- Sidebar menu -->

      @include('layouts.menu')

    <!-- Page Content -->
    <div id="page-content-wrapper">
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(Session::has('flash_message_warning'))
             <message message="{{ Session::get('flash_message_warning') }}" type="warning"></message>
        @endif
        @if(Session::has('flash_message'))
            <message message="{{ Session::get('flash_message') }}" type="success"></message>
        @endif
        <div class="container-fluid"   style="height: 100%; ">
            <div class="row">
                <div class="col-lg-12">
                        <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="pull-left page-title">@yield('heading') </h4>
                                   <br><br>
                                </div>
                            </div>
                  
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>

        <script type="text/javascript" src="{{ URL::asset('js/datatables.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/datatables.js') }}"></script>


    <script type="text/javascript" src="{{ URL::asset('js/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.caret.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.atwho.min.js') }}"></script>

@stack('scripts')
<script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/detect.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/fastclick.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/jquery.blockUI.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/waves.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/wow.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ URL::asset('frontend/js/jquery.scrollTo.min.js') }}"></script>

    {{-- <script src="{{ URL::asset('frontend/js/jquery.app.js') }}"></script> --}}
    <link href="{{ URL::asset('frontend/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('frontend/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('frontend/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('frontend/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('frontend/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/plugins/moment/moment.js') }}"></script>
    
    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/counterup/jquery.counterup.min.js') }}"></script>
    
    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
    
    
   

    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/pages/jquery.todo.js') }}"></script>
    
    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/pages/jquery.chat.js') }}"></script>
    
    <!-- jQuery  -->
    <script src="{{ URL::asset('frontend/pages/jquery.dashboard.js') }}"></script>
    
    <script type="text/javascript">
        /* ==============================================
        Counter Up
        =============================================== */
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 100,
                time: 1200
            });
        });
    </script>
    <script src="{{ URL::asset('frontend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('frontend/plugins/datatables/dataTables.scroller.min.js') }}"></script>

    <!-- Datatable init js -->
    <script src="{{ URL::asset('frontend/pages/datatables.init.js') }}"></script>

    {{-- <script src="{{ URL::asset('frontend/js/jquery.app.js') }}"></script> --}}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable( { keys: true } );
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
            var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
        } );
        TableManageButtons.init();
    </script>

 <!--Morris Chart-->
 <script src="{{ URL::asset('frontend/plugins/morris.js/morris.min.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/raphael/raphael-min.js') }}"></script>
 <script src="{{ URL::asset('frontend/pages/morris.init.js') }}"></script>

 <!--flot Chart-->
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.time.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.resize.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.pie.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.selection.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.stack.js') }}"></script>
 <script src="{{ URL::asset('frontend/plugins/flot-chart/jquery.flot.crosshair.js') }}"></script>
 <script src="{{ URL::asset('frontend/pages/jquery.flot.init.js') }}"></script>

 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"> </script> -->

    <script type="text/javascript">
    $('tbody').delegate('.quantity,.budget','keyup',function(){
        var tr=$(this).parent().parent();
        var quantity=tr.find('.quantity').val();
        var budget=tr.find('.budget').val();
        var amount=(quantity*budget);
        tr.find('.amount').val(amount);
        total();
    });
    function total(){
        var total=0;
        $('.amount').each(function(i,e){
            var amount=$(this).val()-0;
        total +=amount;
    });
    $('.total').html(total+".00 tk");
    }
    $('.addRow').on('click',function(){
        addRow();
    });
    function addRow()
    {
        var tr='<tr>'+
            '<td><?php $abonnementss = DB::table('abonnements')->where('statut', '=', 0)->get() ?><select id="inputState" name="abonnement_id[]" class="form-select"><option value="">Selectionnez un abonnement</option>@foreach($abonnementss as $abon)<option value="{{$abon->id}}">{{$abon->nom}}</option>@endforeach</select> </td>'+
            '<td> <?php $chargeds = DB::table('chargeds')->where('nom', '!=', 'pas')->where('nom', '!=', 'No')->get() ?><select id="inputState" name="chargerto[]" class="form-select"><option value="">Selectionnez</option>@foreach($chargeds as $charged)<option value="{{$charged->nom}}">{{$charged->nom}}</option>@endforeach</select> </td>'+      
            '<td>  <div class="controls"><input name="date_debut[]" type="date" class="form-control" placeholder="Date de début"> </div></td>'+
            '<td>  <div class="controls"><input name="date_fin[]" type="date" class="form-control" placeholder="Date de fin"> </div></td>'+     
            '<td><a href="#" class="btn btn-danger remove"><i class="bi bi-trash"></i></a></td>'+
        '</tr>';
        $('tbody').append(tr);
    };
    $('.remove').live('click',function(){
        var last=$('tbody tr').length;
        if(last==1){
            alert("Désolé vous ne pouvez pas supprimer cette ligne !");
        }
        else{
             $(this).parent().parent().remove();
        }    });
</script>                                <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    form.classList.add('was-validated');
                                                }, false);
                                            });
                                        }, false);
                                    })();
                                </script>

<script>
var form_shareabo = document.getElementById('demo-upload').action;
var taskabo = $("#task").val();
function shareabo(){
    $.post(form_shareabo,
    {
        id: taskabo
    },
    function(data, status){
        console.log('data: ', data);
        console.log('status: ', status);
    });
}
</script>


</body>

</html>
