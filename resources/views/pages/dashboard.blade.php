@extends('layouts.master')

@section('content')
@push('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip(); //Tooltip on icons top

            $('.popoverOption').each(function () {
                var $this = $(this);
                $this.popover({
                    trigger: 'hover',
                    placement: 'left',
                    container: $this,
                    html: true,

                });
            });
        });
    </script>
@endpush
{{-- <h1>
{{$name = \Auth::user()->name}}</h1> --}}

         <!--Widget-4 -->
         <div class="row " style="height: max; ">
      

            <div class="col-sm-12 col-lg-4">
                <div class="mini-stat clearfix bx-shadow bg-white">
                    <span class="mini-stat-icon bg-warning"><i style="color:black" class="fa fa-truck

                        "></i></span>
                    <div class="mini-stat-info text-right text-dark" >
                        <span class="counter " style="color:black">{{$totalContacts}}</span>
                        {{ __('dashboard.Allequipments') }} 
                    </div>
                    <div class="tiles-progress">
                        <div class="m-t-20">
                            <a href="{{route('contacts.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; ">{{ __('dashboard.Allequipments') }} </h5></a>
                            {{-- <div class="progress progress-sm m-0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                                    <span class="sr-only">57% Complete</span>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="mini-stat clearfix bx-shadow bg-white">
                    <span class="mini-stat-icon bg-warning"><i style="color:black" class="ion-android-contacts"></i></span>
                    <div class="mini-stat-info text-right text-dark" >
                        <span class="counter " style="color:black">{{$totalClients}}</span>
                        {{ __('dashboard.AllCustomer') }} 
                    </div>
                    <div class="tiles-progress">
                        <div class="m-t-20">
                            <a href="{{route('clients.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; ">{{ __('dashboard.AllCustomer') }} </h5></a>
                            {{-- <div class="progress progress-sm m-0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                                    <span class="sr-only">57% Complete</span>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
           
            @if(Entrust::hasRole('administrator')||Entrust::hasRole('Admin'))

                <div class="col-sm-12 col-lg-4">
                    <div class="mini-stat clearfix bx-shadow bg-white">
                        <span class="mini-stat-icon bg-warning"><i style="color:black" class="ion-android-social-user"></i></span>
                        <div class="mini-stat-info text-right text-dark" >
                            <span class="counter " style="color:black">{{$totalUsers}}</span>
                            {{ __('dashboard.AllUsers') }} 
                        </div>
                        <div class="tiles-progress">
                            <div class="m-t-20">
                                <a href="{{route('users.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; "> {{ __('dashboard.AllUsers') }} </h5></a>
                                {{-- <div class="progress progress-sm m-0">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                                        <span class="sr-only">57% Complete</span>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="col-sm-12 col-lg-4">
                    <div class="mini-stat clearfix bx-shadow bg-white">
                        <span class="mini-stat-icon bg-warning"><i style="color:black" class="fa fa-money	
                            "></i></span>
                        <div class="mini-stat-info text-right text-dark" >
                            <span class=" " style="color:black">80000 FR CFA</span>
                            Turnover
                        </div>
                        <div class="tiles-progress">
                            <div class="m-t-20">
                                {{-- <a href="{{  }}route('users.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; ">Tous les utilisteurs </h5></a> --}}
                                {{-- <div class="progress progress-sm m-0">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                                        <span class="sr-only">57% Complete</span>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                
                @endif 





               
            </div> <!-- End row-->
 <!--Widget-4 -->
 <div class="row " style="height: max; ">
      

    <div class="col-sm-12 col-lg-4">
        <div class="mini-stat clearfix bx-shadow bg-white">
            <span class="mini-stat-icon bg-warning"><i style="color:black" class="fa fa-truck

                "></i></span>
            <div class="mini-stat-info text-right text-dark" >
            @if($totalContacts == 0)
                <span class=" " style="color:black">0% </span>
                @else
                <span class=" " style="color:black">{{intval(($totalEC *100)/$totalContacts)}}% </span>
                @endif
                {{ __('dashboard.Percentageofconnectedequipment') }}
            </div>
            <div class="tiles-progress">
                <div class="m-t-20">
                    {{-- <a href="{{route('contacts.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; ">{{ __('dashboard.Percentageofconnectedequipment') }} </h5></a> --}}
                    {{-- <div class="progress progress-sm m-0">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                            <span class="sr-only">57% Complete</span>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-lg-4">
        <div class="mini-stat clearfix bx-shadow bg-white">
            <span class="mini-stat-icon bg-warning"><i style="color:black" class="ion-android-contacts"></i></span>
            <div class="mini-stat-info text-right text-dark" >
                <span class="counter " style="color:black">{{$totalEPLNC}}</span>
                {{ __('dashboard.NumberofmachineswithPLnotconnected') }}            </div>
            <div class="tiles-progress">
                <div class="m-t-20">
                    {{-- <a href="{{route('clients.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; ">All clients </h5></a> --}}
                    {{-- <div class="progress progress-sm m-0">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                            <span class="sr-only">57% Complete</span>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

        <div class="col-sm-12 col-lg-4">
            <div class="mini-stat clearfix bx-shadow bg-white">
                <span class="mini-stat-icon bg-warning"><i style="color:black" class="ion-android-social-user"></i></span>
                <div class="mini-stat-info text-right text-dark" >
                    <span class="counter " style="color:black">{{($totalEGPLNC+$totalEMPLNC)}}</span>
                    {{ __('dashboard.NumberofgroupswithPLnotconnected') }}            </div>
                <div class="tiles-progress">
                    <div class="m-t-20">
                        {{-- <a href="{{route('users.index')}}"><h5 class="text-uppercase"style="color:black; text-align: center; ">All Users </h5></a> --}}
                        {{-- <div class="progress progress-sm m-0">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                                <span class="sr-only">57% Complete</span>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>



       
    </div> <!-- End row-->

    






    <!-- <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Breakdown of the machine fleet by type of PL</h3> 
                </div> 
                <div  id="piechartmtpl"></div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Distribution of the group fleet by type of PL</h3> 
                </div> 
                <div  id="piechartgtpl"></div> 
            </div>
        </div>
        
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Breakdown of the machine fleet by type of subscription</h3> 
                </div> 
                <div  id="piechartmtab"></div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Breakdown of the group fleet by type of subscription</h3> 
                </div> 
                <div  id="piechartgtab"></div> 
            </div>
        </div>
        
    </div>
            
       <!--Widget-4 -->
       <div class="row "  >
           
        {{-- <div class="col-lg-4">
            <div class="panel panel-default" >
                <div class="panel-heading">
                    <h4 class="panel-title" style="color:black">Utilisateurs en ligne   </h4>
                </div>
                
                <div class="panel-body">
                    <div class="inbox-widget nicescroll mx-box">
                            @foreach($users as $user)
                        <a href="#">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img class="small-profile-picture" data-toggle="tooltip" title="{{$user->name}}"
                                    data-placement="left"
                                    @if($user->image_path != "")
                                    src="images/{{$companyname}}/{{$user->image_path}}"
                                    @else
                                    src="images/default_avatar.jpg"
                                       @endif />                                
                                </div>
                                <p class="inbox-item-author">{{$user->name}}  @if($user->isOnline())
                                        <i class="dot-online" data-toggle="tooltip" title="Online" data-placement="left"></i>
                                    @else
                                        <i class="dot-offline" data-toggle="tooltip" title="Offline" data-placement="left"></i>
                                    @endif
                                </p>
                                <p class="inbox-item-text">{!!$user->department->pluck('name')!!}</p>

                            </div>
                        </a>
                        @endforeach
                        
                    </div>
                </div>
               
            </div>
        </div> <!-- end col --> --}}









        {{-- <div class="col-lg-8">
                <div class="panel panel-border panel-primary">
                    <div class="panel-heading"> 
                        <h3 class="panel-title">Evolutionary curve of subscriptions</h3> 
                    </div> 
                    <div class="panel-body"> 
                        <div id="morris-line-example" style="height: 355px"></div>
                    </div> 
            </div>
        </div> <!-- end col --> --}}

{{-- {{$gmStarter}}
            {{$gmInform}}
            {{$gmInforms}} --}}
          
        
       
        </div> <!-- End row-->

        {{-- <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            @foreach($taskCompletedThisMonth as $thisMonth)
                                {{$thisMonth->total}}
                            @endforeach
                        </h3>

                        <p>{{ __('Tasks completed this month') }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-book-outline"></i>
                    </div>
                    <a href="{{route('tasks.index')}}" class="small-box-footer">{{ __('All Tasks') }} <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col --> --}}


            {{--thiss}}
            {{-- <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green" >
                    <div class="inner" style="background-color:#808080">
                        <h3 style="background-color:##808080">{{$totalContacts}}</h3>


                        <p>{{ __('Tous les equipments') }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a style="background-color:black" href="{{route('contacts.index')}}" class="small-box-footer" >{{ __('Tous les equipements') }} <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner" style="background-color:#808080">
                        <h3>{{$totalClients}}</h3>

                        <p>{{ __('All Clients') }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                    <a  style="background-color:black" href="{{route('clients.index')}}" class="small-box-footer">{{ __('All Clients') }} <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner" style="background-color:#808080">
                        <h3 >{{$totalUsers}}</h3>

                        <p>{{ __('Total Utilisateurs') }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-people"></i>
                    </div>
                    <a style="background-color:black" href="{{route('users.index')}}" class="small-box-footer">{{ __('Tous les utilisteurs') }}  <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row --> --}}

        <?php $createdTaskEachMonths = array(); $taskCreated = array();?>
        @foreach($createdTasksMonthly as $task)
            <?php $createdTaskEachMonths[] = date('F', strTotime($task->created_at)) ?>
            <?php $taskCreated[] = $task->month;?>
        @endforeach

        <?php $completedTaskEachMonths = array(); $taskCompleted = array();?>

        @foreach($completedTasksMonthly as $tasks)
            <?php $completedTaskEachMonths[] = date('F', strTotime($tasks->updated_at)) ?>
            <?php $taskCompleted[] = $tasks->month;?>
        @endforeach

        <?php $completedLeadEachMonths = array(); $leadsCompleted = array();?>
        @foreach($completedLeadsMonthly as $leads)
            <?php $completedLeadEachMonths[] = date('F', strTotime($leads->updated_at)) ?>
            <?php $leadsCompleted[] = $leads->month;?>
        @endforeach

        <?php $createdLeadEachMonths = array(); $leadCreated = array();?>
        @foreach($createdLeadsMonthly as $lead)
            <?php $createdLeadEachMonths[] = date('F', strTotime($lead->created_at)) ?>
            <?php $leadCreated[] = $lead->month;?>
        @endforeach
        {{-- <div class="row">

            @include('partials.dashboardone')


        </div> --}}
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],
  ['PLE641', <?php echo $PLE641; ?>],
  ['PLE631', <?php echo $PLE631; ?>],
  ['PLE601', <?php echo $PLE601; ?>],
  ['PLE641+PL631', <?php echo $PLE641_PL631; ?>],
  ['NPL1', <?php echo $NPL1; ?>],
  ['NPL2', <?php echo $NPL2; ?>],
  ['NPL3', <?php echo $NPL3; ?>],
  ['NPL4', <?php echo $NPL4; ?>],
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = { 'width':'100%', 'height':355};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechartmtpl'));
  chart.draw(data, options);
  
}
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],
['PLE641', <?php echo $gmPLE641; ?>],
['PLE631', <?php echo $gmPLE631; ?>],
['PLE601', <?php echo $gmPLE601; ?>],
['PLE641+PL631', <?php echo $gmPLE641_PL631; ?>],
['NPL1', <?php echo $gmNPL1; ?>],
['NPL2', <?php echo $gmNPL2; ?>],
['NPL3', <?php echo $gmNPL3; ?>],
['NPL4', <?php echo $gmNPL4; ?>],
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = { 'width':'100%', 'height':355, colors: ['#e0440e', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'] };

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechartgtpl'));
  chart.draw(data, options);
  
}
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],
  ['Starter', <?php echo $Starter; ?>],
  ['Inform', <?php echo $Inform; ?>],
  ['Inform+', <?php echo $Informs; ?>],
  ['NS1', <?php echo $NS1; ?>],
  ['NS2', <?php echo $NS2; ?>],
  ['NS3', <?php echo $NS3; ?>],
  ['NS4', <?php echo $NS4; ?>],
  ['NS5', <?php echo $NS5; ?>],
  
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = { 'width':'100%', 'height':355};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechartmtab'));
  chart.draw(data, options);
  
}
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],
 ['Starter', <?php echo $gmStarter; ?>],
  ['Inform', <?php echo $gmInform; ?>],
  ['Inform+', <?php echo $gmInforms; ?>],
  ['NS1', <?php echo $gmNS1; ?>],
  ['NS2', <?php echo $gmNS2; ?>],
  ['NS3', <?php echo $gmNS3; ?>],
  ['NS4', <?php echo $gmNS4; ?>],
  ['NS5', <?php echo $gmNS5; ?>],
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = { 'width':'100%', 'height':355, colors: ['#000000', '#FEC535', '#808080', '#f3b49f'] };

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechartgtab'));
  chart.draw(data, options);
  //parseInt('<?php echo $totalContacts; ?>')
}
</script>

@endsection                                   