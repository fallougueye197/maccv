@extends('layouts.master')
@section('heading')
{{-- cdn for extraction --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
{{--  --}}
    <h1>{{ __('All PL') }}</h1>
@stop
@section('content')
<div class="col-lg-12 currenttask">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        
        
        <thead>
        <tr>
            <th>{{ __("ID") }}</th>
            <th>{{ __("PL Reference") }}</th>
            <th>{{ __("Network") }}</th>
                 <th>{{ __('Status') }}</td>


            @if(Entrust::hasRole('administrator'))
                <th>{{__('Action')}}</th>
                @endif 

        </tr>
    </thead>
    <tbody>

        @foreach($boitiers as $boitier)
        @if($boitier->id != 1)

            <tr>
                <td>{{$boitier->id}}</td>
                <td>{{$boitier->reference}}</td>
                <td>{{$boitier->communication_type}}</td>
                
                <td class="center">
							@if($boitier->statut==0)
							<span class="label label-warning">Active</span>
							@else
                                <span class="label label-default">Unactive</span>
							@endif
					</td> 
                                @if(Entrust::hasRole('administrator'))
                    <td class="center">
                            @if($boitier->statut==1)
							<a class="btn btn btn-default" href="{{URL::to('/unactive_boitier/'.$boitier->id)}}">
                                <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span>							</a>
                            @else
                             <a class="btn btn-warning" href="{{URL::to('/active_boitier/'.$boitier->id)}}">
                                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>							</a>
                            @endif            
            
            {{-- <a href="/boitiers/{{$boitier->id}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a> --}}
            <a class="btn btn-sm btn-warning" href="/boitiers/{{$boitier->id}}/edit"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                     @if(!$eqipboit = DB::table('contacts')->pluck('boitier_id')->contains($boitier->id))

                    {{-- {!!Form::open(['action' => ['BoitiersController@destroy', $boitier->id], 'method'=> 'POST'])!!}    
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Supprimer', ['class'=> 'btn btn-sm btn-warning'])}}
                    {!!Form::close()!!} --}}
                    @endif
                </td>
                    @endif
     

                {{-- @if(Entrust::hasRole('administrator'))
                    <td>   {!! Form::open([
        'method' => 'DELETE',
        'route' => ['boitiers.destroy', $abon->id]
    ]); !!}
                        {!! Form::submit( __('Delete'), ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure?")']); !!}

                        {!! Form::close(); !!}</td></td>
                @endif --}}
            </tr>
            @endif
                                                           


        @endforeach
     </tbody>
    </table>
     <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
            <a class="btn btn-lg btn-warning" href="/boitiers/create">
                <font style = "vertical-align: inherit;">
                <font style = "vertical-align: inherit;">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                </font>
                </font> 
            </a> 
        
        </button>

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
   

    <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
</div>        
@endsection