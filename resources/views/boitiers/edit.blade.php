@extends('layouts.master')
@section('content')

<h1>Update PL</h1>
{!! Form::open(['action' => ['BoitiersController@update', $boitier->id],'method'=> 'POST']) !!}
<div class="form-group">
    {{ Form::label('reference', 'Reference PL') }}
    {{ Form::text('reference', $boitier->reference, ['class'=>'form-control']) }}
    {{ Form::label('communication_type', "Communication's Type") }}
    {{ Form::text('communication_type', $boitier->communication_type, ['class'=>'form-control']) }}


    
</div>
{{Form::hidden('_method', 'PUT')}}
{!! Form::submit('Update PL', ['class'=>'btn btn-lg btn-primary']) !!}
{!! Form::close() !!}
@endsection
   