<?php

use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('abonnements')->insert([
            0 =>
            array(
            'id' => 1,
            'nom' => 'Select Subscription',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            1 =>
            array(
            'id' => 2,
            'nom' => 'Starter',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            2 =>
            array(
            'id' => 3,
            'nom' => 'Inform',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            3 =>
            array(
            'id' => 4,
            'nom' => 'Inform+',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            4 =>
            array(
            'id' => 5,
            'nom' => 'New subcription1',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            5 =>
            array(
            'id' => 6,
            'nom' => 'New subcription2',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            6 =>
            array(
            'id' => 7,
            'nom' => 'New subcription3',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            7 =>
            array(
            'id' => 8,
            'nom' => 'New subcription 4',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),
            8 =>
            array(
            'id' => 9,
            'nom' => 'New subcription 5',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2019-06-04 13:42:19',
            'updated_at' => '2019-09-04 13:42:19',),



        ]);

        DB::table('boitiers')->insert([
            0 =>
            array(
            'id' => 1,  
            'reference' => 'Select PL',
            'communication_type' => "null",
            'statut' => 0,
            'created_at' => '2016-06-04 13:42:19',
            'updated_at' => '2016-06-04 13:42:19',
            ),
            1 =>
            array(
                'id' => 2,
                'reference' =>'PLE641',
                'communication_type' => "Sm",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),

            2 =>
            array(
                'id' => 3,
                'reference' => 'PLE631',
                'communication_type' => "Sm",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),
            3 =>
            array(
                'id' => 4,
                'reference' => 'PLE601',
                'communication_type' => "SM",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),
            4 =>
            array(
                'id' => 5,
                'reference' => 'PLE641+PL631',
                'communication_type' => "SM",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),

            5 =>
            array(
                'id' => 6,
                'reference' => 'New PL1',
                'communication_type' => "SM",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),
            6 =>
            array(
                'id' => 7,
                'reference' => 'New PL2',
                'communication_type' => "SM",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),
           7 =>
            array(
                'id' => 8,
                'reference' => 'New PL3',
                'communication_type' => "SM",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),
            8 =>
            array(
                'id' => 9,
                'reference' => 'New PL4',
                'communication_type' => "SM",
                'statut' => 0,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            ),
           





        ]);
       
        


    }
}
