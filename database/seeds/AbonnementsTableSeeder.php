<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AbonnementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abonnements')->insert([
            'nom' => 'Selectionnez un abonnement',
            'type' => "null",
            'interface1' => "null",
            'interface2' => null,
            'interface3' => null,
            'tarif' => 0,
            'statut' => 0,
            'created_at' => '2016-06-04 13:42:19',
            'updated_at' => '2016-06-04 13:42:19',




        ]);
    }
}
