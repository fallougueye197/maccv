<?php

use Illuminate\Database\Seeder;
use App\Models\PermissionRole;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * ADMIN ROLES
         *
         */
        $createUser = new PermissionRole;
        $createUser->role_id = '1';
        $createUser->permission_id = '1';
        $createUser->timestamps = false;
        $createUser->save();

        $readUser = new PermissionRole;
        $readUser->role_id = '1';
        $readUser->permission_id = '2';
        $readUser->timestamps = false;
        $readUser->save();

        $updateUser = new PermissionRole;
        $updateUser->role_id = '1';
        $updateUser->permission_id = '3';
        $updateUser->timestamps = false;
        $updateUser->save();

        $deleteUser = new PermissionRole;
        $deleteUser->role_id = '1';
        $deleteUser->permission_id = '4';
        $deleteUser->timestamps = false;
        $deleteUser->save();

        $createClient = new PermissionRole;
        $createClient->role_id = '1';
        $createClient->permission_id = '5';
        $createClient->timestamps = false;
        $createClient->save();

        $readClient = new PermissionRole;
        $readClient->role_id = '1';
        $readClient->permission_id = '6';
        $readClient->timestamps = false;
        $readClient->save();

        $updateClient = new PermissionRole;
        $updateClient->role_id = '1';
        $updateClient->permission_id = '7';
        $updateClient->timestamps = false;
        $updateClient->save();

        $deleteClient = new PermissionRole;
        $deleteClient->role_id = '1';
        $deleteClient->permission_id = '8';
        $deleteClient->timestamps = false;
        $deleteClient->save();

        // $createTask = new PermissionRole;
        // $createTask->role_id = '1';
        // $createTask->permission_id = '9';
        // $createTask->timestamps = false;
        // $createTask->save();

        // $updateTask = new PermissionRole;
        // $updateTask->role_id = '1';
        // $updateTask->permission_id = '10';
        // $updateTask->timestamps = false;
        // $updateTask->save();

        // $createLead = new PermissionRole;
        // $createLead->role_id = '1';
        // $createLead->permission_id = '11';
        // $createLead->timestamps = false;
        // $createLead->save();

        // $updateLead = new PermissionRole;
        // $updateLead->role_id = '1';
        // $updateLead->permission_id = '12';
        // $updateLead->timestamps = false;
        // $updateLead->save();

        $createContact = new PermissionRole;
        $createContact->role_id = '1';
        $createContact->permission_id = '9';
        $createContact->timestamps = false;
        $createContact->save();

        $readContact = new PermissionRole;
        $readContact->role_id = '1';
        $readContact->permission_id = '10';
        $readContact->timestamps = false;
        $readContact->save();


        $updateContact = new PermissionRole;
        $updateContact->role_id = '1';
        $updateContact->permission_id = '11';
        $updateContact->timestamps = false;
        $updateContact->save();

        $deleteContact = new PermissionRole;
        $deleteContact->role_id = '1';
        $deleteContact->permission_id = '12';
        $deleteContact->timestamps = false;
        $deleteContact->save();

        $createAbonnement = new PermissionRole;
        $createAbonnement->role_id = '1';
        $createAbonnement->permission_id = '13';
        $createAbonnement->timestamps = false;
        $createAbonnement->save();

        $readAbonnement = new PermissionRole;
        $readAbonnement->role_id = '1';
        $readAbonnement->permission_id = '14';
        $readAbonnement->timestamps = false;
        $readAbonnement->save();


        $updateAbonnement = new PermissionRole;
        $updateAbonnement->role_id = '1';
        $updateAbonnement->permission_id = '15';
        $updateAbonnement->timestamps = false;
        $updateAbonnement->save();

        $deleteAbonnement = new PermissionRole;
        $deleteAbonnement->role_id = '1';
        $deleteAbonnement->permission_id = '20';
        $deleteAbonnement->timestamps = false;
        $deleteAbonnement->save();

        $createBoitier = new PermissionRole;
        $createBoitier->role_id = '1';
        $createBoitier->permission_id = '21';
        $createBoitier->timestamps = false;
        $createBoitier->save();

        $readBoitier = new PermissionRole;
        $readBoitier->role_id = '1';
        $readBoitier->permission_id = '22';
        $readBoitier->timestamps = false;
        $readBoitier->save();


        $updateBoitier = new PermissionRole;
        $updateBoitier->role_id = '1';
        $updateBoitier->permission_id = '23';
        $updateBoitier->timestamps = false;
        $updateBoitier->save();

        $deleteBoitier = new PermissionRole;
        $deleteBoitier->role_id = '1';
        $deleteBoitier->permission_id = '24';
        $deleteBoitier->timestamps = false;
        $deleteBoitier->save();
    }
}
