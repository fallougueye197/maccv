<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = new Role;
        $adminRole->display_name = 'Administrator';
        $adminRole->name = 'administrator';
        $adminRole->description = 'Administrators have superuser access';
        $adminRole->save();

        $salesrepRole = new Role;
        $salesrepRole->display_name = 'Admin';
        $salesrepRole->name = 'Admin';
        $salesrepRole->description = 'Admin du systeme';
        $salesrepRole->save();


        $salesrepRole = new Role;
        $salesrepRole->display_name = 'Support CM';
        $salesrepRole->name = 'Suport CM';
        $salesrepRole->description = '';
        $salesrepRole->save();

        $employeeRole = new Role;
        $employeeRole->display_name = 'Support RA';
        $employeeRole->name = 'Support RA';
        $employeeRole->description = '';
        $employeeRole->save();
    }
}
