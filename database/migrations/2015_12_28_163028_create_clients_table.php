<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
          
          
          
            $table->increments('id'); 
            $table->string('name');
            $table->string('pays');
            $table->string('business');
            $table->string('primary_email')->unique();
            $table->string('numidint')->unique()->nullable();
            $table->string('numidext')->unique()->nullable();
            $table->string('numidext2');            
            $table->string('primary_number')->unique()->nullable();
            $table->string('secondary_number')->unique()->nullable();
            $table->string('company_name')->nullable();
            $table->string('industry')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();         
            $table->string('vat')->nullable();     
            $table->string('company_type')->nullable();            
            //$table->integer('user_id')->unsigned()->nullable();
            //$table->foreign('user_id')->references('id')->on('users')->nullable();
            $table->integer('industry_id')->unsigned()->nullable();
            $table->foreign('industry_id')->references('id')->on('industries')->nullable();

            // $table->integer('pays_id')->unsigned()->nullable();
            // $table->foreign('pays_id')->references('id')->on('pays')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('clients');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
