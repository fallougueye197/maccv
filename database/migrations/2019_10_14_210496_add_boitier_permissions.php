<?php

use App\Models\Permissions;
use Illuminate\Database\Migrations\Migration;

class AddBoitierPermissions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        /**
         * add Boitiers permissions.
         */
        $createBoitier               = new Permissions();
        $createBoitier->display_name = 'Create PL';
        $createBoitier->name         = 'boitier-create';
        $createBoitier->description  = 'Permission to create boitier';
        $createBoitier->save();

        $readBoitier               = new Permissions();
        $readBoitier->display_name = 'Read PL';
        $readBoitier->name         = 'boitier-read';
        $readBoitier->description  = 'Permission to read boitier';
        $readBoitier->save();

        $updateBoitier               = new Permissions();
        $updateBoitier->display_name = 'Update PL';
        $updateBoitier->name         = 'boitier-update';
        $updateBoitier->description  = 'Permission to update boitier';
        $updateBoitier->save();

        $deleteBoitier               = new Permissions();
        $deleteBoitier->display_name = 'Delete PL';
        $deleteBoitier->name         = 'boitier-delete';
        $deleteBoitier->description  = 'Permission to delete boitier';
        $deleteBoitier->save();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {

        Permissions::where('name', 'boitier-read')->delete();
        Permissions::where('name', 'boitier-create')->delete();
        Permissions::where('name', 'boitier-update')->delete();
        Permissions::where('name', 'boitier-delete')->delete();
    }
}
