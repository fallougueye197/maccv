<?php

use App\Models\Permissions;
use Illuminate\Database\Migrations\Migration;

class AddAbonnementPermissions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        /**
         * add Abonnements permissions.
         */
        $createAbonnement               = new Permissions();
        $createAbonnement->display_name = 'Create subscription';
        $createAbonnement->name         = 'abonnement-create';
        $createAbonnement->description  = 'Permission to create abonnement';
        $createAbonnement->save();
       

        $readAbonnement               = new Permissions();
        $readAbonnement->display_name = 'Read subscription';
        $readAbonnement->name         = 'abonnement-read';
        $readAbonnement->description  = 'Permission to read abonnement';
        $readAbonnement->save();

        $updateAbonnement               = new Permissions();
        $updateAbonnement->display_name = 'Update subscription';
        $updateAbonnement->name         = 'abonnement-update';
        $updateAbonnement->description  = 'Permission to update abonnement';
        $updateAbonnement->save();

        $deleteAbonnement               = new Permissions();
        $deleteAbonnement->display_name = 'Delete subscription';
        $deleteAbonnement->name         = 'abonnement-delete';
        $deleteAbonnement->description  = 'Permission to delete abonnement';
        $deleteAbonnement->save();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Permissions::where('name', 'abonnement-read')->delete();
        Permissions::where('name', 'abonnement-create')->delete();
        Permissions::where('name', 'abonnement-update')->delete();
        Permissions::where('name', 'abonnement-delete')->delete();
    }
}
