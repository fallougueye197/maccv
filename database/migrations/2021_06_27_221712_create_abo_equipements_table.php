<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboEquipementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abo_equipements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chargerto')->nullable();
            $table->boolean('end_statut')->default(0);
            $table->date('date_debut')->nullable();
            $table->date('date_fin')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('abonnement_id')->nullable();
            $table->integer('pays_id')->nullable();
            $table->integer('charged_id')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abo_equipements');
    }
}
