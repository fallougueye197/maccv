<?php

use App\Models\Permissions;
use Illuminate\Database\Migrations\Migration;

class AddContactPermissions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        /**
         * add contacts permissions.
         */
        $createContact               = new Permissions();
        $createContact->display_name = 'Create equipment';
        $createContact->name         = 'equipement-create';
        $createContact->description  = 'Permission to create equipment';
        $createContact->save();

        $readContact               = new Permissions();
        $readContact->display_name = 'Read equipment';
        $readContact->name         = 'equipement-read';
        $readContact->description  = 'Permission to read equipment';
        $readContact->save();

        $updateContact               = new Permissions();
        $updateContact->display_name = 'Update equipment';
        $updateContact->name         = 'equipement-update';
        $updateContact->description  = 'Permission to update equipment';
        $updateContact->save();

        $deleteContact               = new Permissions();
        $deleteContact->display_name = 'Delete equipment';
        $deleteContact->name         = 'equipement-delete';
        $deleteContact->description  = 'Permission to delete equipment';
        $deleteContact->save();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Permissions::where('name', 'contact-read')->delete();
        Permissions::where('name', 'contact-create')->delete();
        Permissions::where('name', 'contact-update')->delete();
        Permissions::where('name', 'contact-delete')->delete();
    }
}
