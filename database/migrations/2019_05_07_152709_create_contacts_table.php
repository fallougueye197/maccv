<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('constructeur');
            $table->string('famille')->nullable();
            $table->string('numero_serie')->unique();
            $table->string('sous_famille')->nullable();
            $table->string('segment')->nullable();
            $table->string('modele');
            $table->string('compteur');
            $table->date('date_mise_en_service');
            $table->string('pays')->nullable();
            $table->string('type');


            $table->string('zipcode')->nullable();
            $table->string('secondary_number')->nullable();
            $table->string('city')->nullable();
            $table->date('primary_number')->nullable();
            $table->string('job_title')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('boitier_id')->nullable()->unsigned()->default(null);
            $table->integer('abonnement_id')->nullable()->unsigned()->default(null);
            $table->date('datedebut')->nullable();
            $table->date('datefin')->nullable();
            $table->string('chargerto')->nullable();

            $table->foreign('client_id')->references('id')->on('clients')->nullable();
            $table->foreign('boitier_id')->references('id')->on('boitiers');
            $table->foreign('abonnement_id')->references('id')->on('abonnements');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('contacts');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
