***Contexte***
***   A. Le réseau JA Delmas***
Concessionnaire Cat depuis 1932, JA Delmas compte aujourd’hui plus de 2,000 collaborateurs répartis sur 10 pays d’Afrique de l’Ouest.
Nos champs d’intervention sont centrés sur les domaines des Mines, de la Construction, de l’Energie, des Transports et de l’Equipement industriel. 
Nous mettons à leur disposition une vaste gamme de matériel neuf, d’occasion ou de location et des stocks de pièces de rechange d’origine. Nos formateurs et nos équipes techniques, formés aux dernières technologies des constructeurs, sont à même d’intervenir sur tous les sites.

   *** B. La connectivité***

Dans le cadre de son processus de transformation digitale, JA Delmas a lancé avec le partanire Caterpillar une campagne de connectivité visant à raccorder les équipements de ses clients à son système d’information. Elle vise principalement les équipements de construction et miniers pour tous types de client.
L’objectif est double : 
    • Identifier les opportunités de vente de pièces et de service 
    • Offrir aux clients des solutions de gestion de flotte
Pour pouvoir bénéficier de ces solutions, les clients doivent : 
    • Doter chaque équipement d’un boitier spécifique permettant de récolter les données via le réseau
    • souscrire pour chaque équipement à un abonnement Cat Connect tarifié et valable pour une période déterminée. Cet abonnement permet d’accéder à des interfaces dédiés de gestion de flotte offrant une visibilité sur l’ensemble du parc client.
Le suivi de ces abonnements, jusqu’ici effectuée avec les outils office, doit évoluer avec la mise à disposition d’un outil dédié pour une meilleure efficacité des équipes en charge.Bénéfices attendus

La valeur ajoutée attendue porte sur : 
    • Amélioration du process d’activation et de déconnexion des équipements
    • Meilleur suivi des factures connectivité clients (revenus)
    • Meilleur suivi des factures connectivité fournisseur (coûts)
Il est important de noter que la solution attendue a un périmètre limité aux équipements connectés mais qui ne sont pas sous contrat CVA. Ces derniers seront gérés depuis une interface dédiée.


