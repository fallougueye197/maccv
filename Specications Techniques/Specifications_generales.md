***Spécifications techniques générales***
    ***A. Interfaces utilisateur**
La solution doit être accessible depuis une interface web et responsive 
    ***B.Environnements pays**
    Un seul environnement est nécessaire pour la gestion des abonnements sur l’ensemble du réseau.
    ***Technologies associées**
    Cette solution  fait appel à des technologies :
    Laravel version 5.4.36 associées à la 
    Vuejs version 2.0.5. 
    Bootstrap4,
    Jquery,
    Datatable,
    Chartejs,
    Ajax 



### Packages
Nous avons utilisé des package comme 
- [LaravelCollective]
- [laravel-datatables]
- [Debugbar]
- [Doctrine-Database-Adstraction-Layer]