<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boitier extends Model
{

    protected $fillable = ['reference','communication_type'];
    
    public function contact()
    {
        return $this->hasMany(Contact::class, 'boitier_id', 'id');
    }

}
