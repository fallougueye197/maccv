<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Contact extends Model
{


    protected $fillable = [


            'constructeur'        ,
            'famille'             ,
            'numero_serie'        ,
            'email'               ,
            'sous_famille'        ,
            'segment'             ,
            'modele'              ,
            'date_mise_en_service',
            'pays' ,  
            'name',
            'email',
            'client_id',
            'boitier_id',
            'abonnement_id',
    ];

    public function abonnements()
    {
        return $this->belongsTo(Abonnement::class, 'abonnement_id' );
    }

    public function boitiers()
    {
        return $this->belongsTo(Boitier::class, 'boitier_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function getFormattedAddressAttribute()
    {
        $address = '';

        if ($this->address1 || $this->city || $this->zipcode) {
            if ($this->address1) {
                $address .= htmlspecialchars($this->address1).'<br/>';
            }
            if ($this->address2) {
                $address .= htmlspecialchars($this->address2).'<br/>';
            }
            if ($this->city || $this->state || $this->zipcode) {
                if ($this->city) {
                    $address .= $this->city.'&nbsp;';
                }
                if ($this->state) {
                    $address .= $this->state.'&nbsp;';
                }
                if ($this->zipcode) {
                    $address .= $this->zipcode;
                }
            }
            if ($this->country) {
                $address .= '<br/>'.$this->country;
            }

            return $address;
        } else {
            return null;
        }
    }

  

    public function scopeMy($query)
    {
        return $query->whereHas('client', function ($q) {
            $q->where('user_id', '=', Auth::id());
        });
    }
}
