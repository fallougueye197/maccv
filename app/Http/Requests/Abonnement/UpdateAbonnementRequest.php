<?php 

namespace App\Http\Requests\Abonnement;

use Illuminate\Foundation\Http\FormRequest;


class  UpdateAbonnementRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
        return auth()->user()->can('abonnement-update');
     }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom'        => 'required',
            'type' => '',
            'interface1' => '',
            'interface2' => '',
            'interface3' => '',
            'tarif_mensuel' => '',
            'statut' => '',

        ];
    }
    
}
