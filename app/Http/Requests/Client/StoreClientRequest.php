<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;




class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('client-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required',
            'pays'              => 'required',
            'business'          => 'required',
            'primary_email'     => 'required|unique:clients',
            'numidint'          => 'required|unique:clients',
            'numidext'        => 'required|unique:clients,numidext,',
            'numidext2'        => 'required',
            'user_id'           => 'required',



            'vat'               => 'max:12',
            'billing_address1'  => '',
            'billing_address2'  => '',
            'billing_city'      => '',
            'billing_state'     => '',
            'billing_zipcode'   => 'max:6',
            'billing_country'   => '',
            'shipping_address1' => '',
            'shipping_address2' => '',
            'shipping_city'     => '',
            'shipping_state'    => '',
            'shipping_zipcode'  => 'max:6',
            'shipping_country'  => '',
            'primary_number'    => 'max:10',
            'secondary_number'  => 'max:10',
            'industry_id'       => '',
            'pays_id'       => '',
            'company_type'      => '',
            
            
        ];
    }
}
