<?php 
namespace App\Http\Requests\Boitier;

use Illuminate\Foundation\Http\FormRequest;

class  UpdateBoitierRequest extends FormRequest
{

 /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
        return auth()->user()->can('boitier-update');
     }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [

            'reference'          => 'required|unique:boitiers',
            'communication_type' => 'required',
 
         ];
     }


}
