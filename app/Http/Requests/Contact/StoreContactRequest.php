<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('equipement-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [


            'constructeur'                => 'required',
            'famille'                     => 'required',
            'numero_serie'                => 'required|unique:contacts',
            'sous_famille'                => '',
            'segment'                     => '',
            'modele'                      => 'required',
            'date_mise_en_service'        => 'required',
            'pays'                        => 'required',
            'boitier_id'                  => '',
            'abonnement_id'               => '',
            'compteur'                        => 'numeric',



            'name'                => '',
            'job_title'           => '',
            'email'               => '',
            'address1'            => '',
            'address2'            => '',
            'city'                => '',
            'state'               => '',
            'zipcode'             => '',
            'country'             => '',
            'primary_number'      => '',
            'secondary_number'    => '',
            'client_id'           => 'required',
            'type'           => 'required',
            'datedebut' => '',
            'datefin' => '',

           

        ];
    }
}
