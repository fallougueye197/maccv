<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;


class UpdateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('equipement-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [


            'constructeur'                => 'required',
            'famille'                     => 'required',
            'numero_serie'                => 'required',
            'sous_famille'                => '',
            'segment'                     => '',
            'modele'                      => 'required',
            'date_mise_en_service'        => 'required' ,
            'pays'                        => 'required',
            'compteur'                        => '',



            'name'                => '',
            'job_title'           => '',
            'address1'            => '',
            'address2'            => '',
            'city'                => '',
            'state'               => '',
            'zipcode'             => '',
            'country'             => '',
            'primary_number'      => '',
            'secondary_number'    => '',
            'client_id'           => 'required',
            'boitier_id'          => 'nullable',
            'abonnement_id'       => 'nullable',
            'type'           => 'required',
            'datedebut' => '' ,
            'datefin' => '',

        ];
    }
}
