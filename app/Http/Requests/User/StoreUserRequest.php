<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('user-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users',
            'matricule'             => 'required|unique:users',
            'pays'                  => 'required',
            'statut'                => '',
            'personal_number'       => 'numeric',
            'password'              => 'sometimes',
            'password_confirmation' => 'sometimes',
            'roles'                 => 'required',
            'departments'           => 'required',
         
        ];
    }
}
