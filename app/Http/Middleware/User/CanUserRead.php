<?php

namespace App\Http\Middleware\User;

use Closure;

class CanUserRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if (!auth()->user()->can('user-read')) {
            Session()->flash('flash_message_warning', 'Not allowed to read users');

            return redirect()->route('dashboard');
        }
    return $next($request);
    }
}
