<?php

namespace App\Http\Middleware\Contact;

use Closure;

class CanContactRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->can('equipement-read')) {
            Session()->flash('flash_message_warning', 'Not allowed to read equipement');

            return redirect()->route('dashboard');
        }
        return $next($request);
    }
}
