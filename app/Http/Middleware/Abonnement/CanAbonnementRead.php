<?php

namespace App\Http\Middleware\Abonnement;

use Closure;

class CanAbonnementRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->can('abonnement-read')) {
            Session()->flash('flash_message_warning', 'Not allowed to read abonnement');

            return redirect()->route('dashboard');
        }

       

        return $next($request);
    }
}
