<?php

namespace App\Http\Middleware\Abonnement;

use Closure;

class CanAbonnementUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!auth()->user()->can('abonnement-update')) {
            Session()->flash('flash_message_warning', 'Not allowed to update abonnement');

            return redirect()->route('abonnements.index');
        }

        return $next($request);
    }
}
