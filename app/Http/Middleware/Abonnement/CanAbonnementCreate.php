<?php

namespace App\Http\Middleware\Abonnement;

use Closure;

class CanAbonnementCreate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!auth()->user()->can('abonnement-create')) {
            Session()->flash('flash_message_warning', 'Not allowed to create Abonnement!');

            return redirect()->route('abonnements.index');
        }

        return $next($request);
    }
}
