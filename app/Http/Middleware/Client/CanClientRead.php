<?php

namespace App\Http\Middleware\Client;

use Closure;

class CanClientRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->can('client-read')) {
            Session()->flash('flash_message_warning', 'Not allowed to read clients');

            return redirect()->route('dashboard');
        }

        return $next($request);
    }
}
