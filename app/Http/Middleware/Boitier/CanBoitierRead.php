<?php

namespace App\Http\Middleware\Boitier;

use Closure;

class CanBoitierRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->can('boitier-read')) {
            Session()->flash('flash_message_warning', 'Not allowed to read Boitier!');

            return redirect()->route('boitiers.index');
        }
        return $next($request);
    }
}
