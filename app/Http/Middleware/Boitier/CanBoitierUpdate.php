<?php

namespace App\Http\Middleware\Boitier;

use Closure;

class CanBoitierUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->can('boitier-update')) {
            Session()->flash('flash_message_warning', 'Not allowed to update boiter!');

            return redirect()->route('boiters.index');
        }
        return $next($request);
    }
}
