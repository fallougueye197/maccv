<?php

namespace App\Http\Middleware\Boitier;

use Closure;

class CanBoitierCreate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->can('boitier-create')) {
            Session()->flash('flash_message_warning', 'Not allowed to create Boitier!');

            return redirect()->route('boitiers.index');
        }

        return $next($request);
    }
}
