<?php

namespace App\Http\Middleware;

use Closure;

class Lindor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!auth()->user()->can('abonnement-read')) {
            Session()->flash('flash_message_warning', 'Not allowed to update abonnement');

            return redirect()->route('dashboard');
        }

    
        return $next($request);
    }
}
