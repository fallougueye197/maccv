<?php

namespace App\Http\Controllers;

use Session;
use App\Http\Requests\Department\StoreDepartmentRequest;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    protected $departments;

    /**
     * DepartmentsController constructor.
     *
     * @param DepartmentRepositoryContract $departments
     */
    public function __construct(DepartmentRepositoryContract $departments)
    {
        $this->departments = $departments;
        $this->middleware('user.is.admin', ['only' => ['create', 'destroy']]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('departments.index')
            ->withDepartment($this->departments->getAllDepartments());
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * @param StoreDepartmentRequest $request
     *
     * @return mixed
     */
    public function store(StoreDepartmentRequest $request)
    {
        $this->departments->create($request);
        Session::flash('flash_message', 'Successfully created New Department');

        return redirect()->route('departments.index');
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        return view ('departments.edit')->with('department', $department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $department = Department::find($id);
       $department->name = $request->input('name');
       $department->description = $request->input('description');
       $department->save();
       return redirect ('/departments');

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        $this->departments->destroy($id);

        return redirect()->route('departments.index');
    }
}
