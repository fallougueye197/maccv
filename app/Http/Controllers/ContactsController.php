<?php

namespace App\Http\Controllers;
use App\Models\Abonnement;

use App\Http\Requests\Contact\StoreContactRequest;
use App\Http\Requests\Contact\UpdateContactRequest;
use App\Models\Contact;
use App\Models\Boitier;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Boitier\BoitierRepositoryContract;
use App\Repositories\Abonnement\AbonnementRepositoryContract;
use App\Repositories\Contact\ContactRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Excel;


class ContactsController extends Controller
{
    protected $users;
    protected $contacts;
    protected $clients;
    protected $boitiers;
    protected $abonnements;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        ContactRepositoryContract $contacts,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings,
        BoitierRepositoryContract $boitiers,
        AbonnementRepositoryContract $abonnements


    ) {
        $this->users    = $users;
        $this->contacts = $contacts;
        $this->clients  = $clients;
        $this->boitiers  = $boitiers;
        $this->abonnements  = $abonnements;


        $this->settings = $settings;
        $this->middleware('contact.read', ['only' => ['read']]);
        $this->middleware('contact.create', ['only' => ['create']]);
        $this->middleware('contact.update', ['only' => ['edit']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $contact = Contact::all();
        $contacts = DB::table('contacts')->select('contacts.*')->get();
        //return view('contacts.index', ['contacts' => Contact::all()]);
        return view('contacts.index', compact('contact','contacts'));
    }

    /**
     * Display a listing of the resource.
     */
    public function my()
    {
        return view('contacts.my');
    }

    /**
     * Make json respnse for datatables.
     *
     * @return mixed
     */
    public function anyData()
    {
        $contacts = Contact::select(['contacts.*', DB::raw('clients.name AS client_name','boitiers.reference AS boitier_reference','abonnements.nom AS abonnement_nom')])
        ->join('clients', 'contacts.client_id', '=', 'clients.id')
        ->join('boitiers', 'contacts.boitier_id', '=', 'boitiers.id')
        ->join('abonnements', 'contacts.abonnement_id', '=', 'abonnements.id')
        ;
        // $contacts = Contact::with('client')->select('contacts.*');

        $dt = Datatables::of($contacts)
            ->addColumn('namelink', function ($contacts) {
                return '<a href="'.route('contacts.show', $contacts->id).'">'.$contacts->name.'</a>';
            })
            ->addColumn('emaillink', function ($contacts) {
                return '<a href="mailto:'.$contacts->email.'">'.$contacts->email.'</a>';
            });
            
           
        // this looks wierd, but in order to keep the two buttons on the same line
        // you have to put them both within the form tags if the Delete button is
        // enabled
        $actions = '';
        if (Auth::user()->can('contact-delete')) {
            $actions .= '<form action="{{ route(\'contacts.destroy\', $id) }}" method="POST">
            ';
        }
        if (Auth::user()->can('contact-update')) {
            $actions .= '<a href="{{ route(\'contacts.edit\', $id) }}" class="btn btn-xs btn-success" >Edit</a>';
        }
        if (Auth::user()->can('contact-delete')) {
            $actions .= '
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" name="submit" value="Delete" class="btn btn-xs btn-danger" onClick="return confirm(\'Are you sure?\')"">
                {{csrf_field()}}
            </form>';
        }

        return $dt->addColumn('actions', $actions)->make(true);
    }

    /**
     * Make json respnse for datatables.
     *
     * @return mixed
     */
    public function myData()
    {
        $contacts = Contact::select(['contacts.*', DB::raw('clients.name AS client_name','boitiers.reference AS boitier_reference','abonnements.nom AS abonnement_nom')])
        ->join('clients', 'contacts.client_id', '=', 'clients.id')
        ->join('boitiers', 'contacts.boitier_id', '=', 'boitiers.id')
        ->join('abonnements', 'contacts.abonnement_id', '=', 'abonnements.id')
        ->my();

        $dt = Datatables::of($contacts)
            ->addColumn('namelink', function ($contacts) {
                return '<a href="'.route('contacts.show', $contacts->id).'">'.$contacts->name.'</a>';
            })
            ->addColumn('emaillink', function ($contacts) {
                return '<a href="mailto:'.$contacts->email.'">'.$contacts->email.'</a>';
            });

        // this looks wierd, but in order to keep the two buttons on the same line
        // you have to put them both within the form tags if the Delete button is
        // enabled
        $actions = '';
        if (Auth::user()->can('contact-delete')) {
            $actions .= '<form action="{{ route(\'contacts.destroy\', $id) }}" method="POST">
            ';
        }
        if (Auth::user()->can('contact-update')) {
            $actions .= '<a href="{{ route(\'contacts.edit\', $id) }}" class="btn btn-xs btn-success" >Edit</a>';
        }
        if (Auth::user()->can('contact-delete')) {
            $actions .= '
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" name="submit" value="Delete" class="btn btn-xs btn-danger" onClick="return confirm(\'Are you sure?\')"">
                {{csrf_field()}}
            </form>';
        }

        return $dt->addColumn('actions', $actions)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
       
        $pays = \App\Pay::pluck('nom', 'nom');
        // $boitiers= \App\Models\Boitier::pluck('reference', 'id');
        // $abonnements= \App\Models\Abonnement::pluck('nom', 'id');

        // $abonnements= Abonnement::pluck('nom');
        return view('contacts.create')
            ->withClients($this->clients->listAllClients())
            ->withBoitiers($this->boitiers->listAllBoitiers())
            ->withAbonnements($this->abonnements->listAllAbonnements())
            // ->with('abonnements',$abonnements)
            // ->with('boitiers',$boitiers)
            ->with('pays',$pays);
    }

    /**
     * @param StoreContactRequest $request
     *
     * @return mixed
     */
    public function store(StoreContactRequest $request)
    {
        $this->validate($request, [
           
            'constructeur'                => 'required',
            'famille'                     => 'required',
            'numero_serie'                => 'required|unique:contacts',
            'sous_famille'                => '',
            'segment'                     => '',
            'modele'                      => 'required',
            'date_mise_en_service'        => 'required',
            'pays'                        => '',
            'abonnement_id'               => 'nullable',
            'boitier_id'                  => 'nullable',
            'type'                        => 'required',
            'datedebut' => '',
            'datefin' => '',




         ]);
        $this->contacts->create($request->all());
        Session()->flash('flash_message', 'equipment successfully created');


        return redirect()->route('contacts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function show(Contact $contact)
    {
        return view('contacts.show', [
            'contact' => $contact,
            'users'   => $this->users->getAllUsersWithDepartments(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        $pays = \App\Pay::pluck('nom', 'nom');
        $client = \App\Models\Client::find($id);
        $boitier = \App\Models\Boitier::find($id);
        $abonnement = \App\Models\Abonnement::find($id);
        return view('contacts.edit')
            ->withContact($this->contacts->find($id))
            ->withClients($this->clients->listAllClients())
            ->withBoitiers($this->boitiers->listAllBoitiers())
            ->withAbonnements($this->abonnements->listAllAbonnements())

            ->with('pays',$pays)
            ->with('client',$client)
            ->with('boitier',$boitier)
            ->with('abonnement',$abonnement);
    }

    /**
     * @param $id
     * @param UpdateContactRequest $request
     *
     * @return mixed
     */
    public function update($id, UpdateContactRequest $request)
    {
        $this->validate($request, [
           
            'constructeur'                => 'required',
            'famille'                     => 'required',
            'numero_serie'                => 'required|unique:contacts,numero_serie,' . $id,
            'sous_famille'                => '',
            'segment'                     => '',
            'modele'                      => 'required',
            'date_mise_en_service'        => 'required',
            'pays'                        => 'required',
            'compteur'                    => '',
            'type'                    => 'required',
            'datedebut' => '',
            'datefin' => '',


        ]);
        $this->contacts->update($id, $request);

        Session()->flash('flash_message', 'equipment successfully updated');

        return redirect()->route('contacts.index', ['id' => $id]);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        $this->contacts->destroy($id);

        return redirect()->route('contacts.index');
    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function downloaContact($type)
     {
         $contact = Contact::get()->toArray();
 
         return Excel::create('excel_data', function($excel) use ($contact) {
             $excel->sheet('mySheet', function($sheet) use ($contact)
             {
                 $sheet->fromArray($contact);
             });
         })->download($type);
     }
 
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function importContact(Request $request)
     {
        $request->validate([
            'import_file' => 'required'
        ]);

        $path = $request->file('import_file')->getRealPath();
        $contact = Excel::load($path)->get();
         if($contact->count()){
             foreach ($contact as $key => $value) {
                 $arr[] = [
           'constructeur'                => $value->constructeur,
           'famille'                     => $value->famille,
           'numero_serie'                => $value->numero_serie,
           'sous_famille'                => $value->sous_famille,
           'segment'                     => $value->segment,
           'modele'                      => $value->modele,
           'date_mise_en_service'        => $value->date_mise_en_service,
           'pays'                        => $value->pays,
           'type'                        => $value->type,

           
                    
                    ];
             }
 
             if(!empty($arr)){
                 Contact::insert($arr);
             }
         }
 
         return back()->with('success', 'Insert Record successfully.');
     }

     /**
     * Display a listing of the resource.
     */
    public function teaser()
    {
        return view('contacts.teaser', ['contacts' => Contact::all()]);
    }
}
