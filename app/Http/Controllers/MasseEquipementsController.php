<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use DB;
use Excel;
use Illuminate\Http\Request;
use Validator;


 

class MasseEquipementsController extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function masseEquipement()
    {
        return view('contacts.masseequipement');
    }
    public function importExcelme(Request $request)
    {
        // $request->validate([
        //    'import_file' => 'required'
        //  ]);
        if ($request->file('import_file'))
        {
        $this->validate($request,
        [
        'import_file' => 'mimes:csv,xls,xlsx',
        ]);
        $err_msg_array= array();
        $path = $request->file('import_file')->getRealPath();
        $import_data = Excel::load($path, function($reader){
        $reader->select(array('constructeur','famille','numero_serie','sous_famille', 'segment','modele','date_mise_en_service','pays' , 'abonnement_id','boitier_id' ,'client_id', 'compteur','type','datedebut','datefin'))->get();
        })->get();
        $import_data_filter= array_filter($import_data->toArray());
        if(!empty($import_data_filter && sizeof($import_data_filter)))
        {
        $export_error_data= $import_data_filter;
        $err_msg_array = [];
        $datacount = sizeof($export_error_data);
        $contact_data= Contact::all()->toArray();
        for($i =0; $i<$datacount; $i++){
        if(isset($export_error_data[$i]['numero_serie']))
        {
        foreach ($contact_data as $key => $value)
        {
        if($value['numero_serie'] == $export_error_data[$i]['numero_serie']){
        $arr_msg_array[$i][]= 'This serial number is already exist';
        }
        else
        {
        $first_numero_serie= $export_error_data[$i]['numero_serie'];
        }
        }
        }
        else {
        $first_numero_serie='';
        }
        $rules= 
                [
                'constructeur' => 'required',
                'famille' => 'required',
                'numero_serie' => 'required|unique:contacts',
                'sous_famille' => '',
                'segment' => '',
                'compteur'=>'',
                'modele' => 'required',
                'date_mise_en_service' => 'required',
                'pays' => 'required',
                'boitier_id' => '',
                'abonnement_id' => '',
                'client_id' => 'required',
                'type'           => 'required',
                'datedebut' => '',
                'datefin' => '',
                ];
        
                $v = validator::make($export_error_data[$i],$rules);
                
                $messages= $v->messages();
                foreach ($messages->all() as $message) {
                $err_msg_array[$i][] = $message;
                }
                $errormessage =[];
                $errorrow = [];
                foreach ($err_msg_array as $key => $value)
                {
                for($k=0; $k<count($value); $k++)
                {
                $errorlist= $value[$k]. 'in row' . ($key+2). "\r\n";
                array_push($errormessage, $errorlist);
                array_push($errorrow, $key+2);
                }
                }
                if(empty($err_msg_array[$i])){
                $dataImported = $export_error_data[$i];
                unset($export_error_data[$i]);
                Contact::insert($dataImported);
            }
        }

            if(empty($errormessage)){
            Session()->flash('flash_message', 'Insert Record successfully');

            return redirect()->route('contacts.index');
            }
            else{
            // remove space and convert into array
            $error_list = preg_replace("/\r|\n/", "", htmlentities(implode(" , ",$errormessage)));
            
            //explode data with comma saprate
            $errors = explode(',', $error_list);
            //convert array in json format
            $download_data = json_encode($export_error_data);
            $get_html = view('contacts.error',compact('errors','download_data'))->render();
            $error_html_list = preg_replace("/\r|\n/","", $get_html);
            
            return back()->with('success', $error_list);
            // ->withInput()
            // ->with('get_html',$error_html_list)
            // ->with('download_data', $download_data);
        } 
        }}}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function masseUpdate()
    {
        return view('contacts.masseedit');
    }
    public function updateMasse(Request $request)
        {
        $path = $request->file('import_file')->getRealPath();
        $import_data = Excel::load($path)->get();
 
        if($import_data->count()){
            foreach ($import_data as $key => $value) {
                $arra[] = [
                    'constructeur' => $value->constructeur,
                    'famille' => $value->famille,
                    'numero_serie' => $value->numero_serie, 
                    'sous_famille' => $value->sous_famille,
                    'segment' => $value->segment,
                    'modele' => $value->modele,
                    'pays' => $value->pays,
                    'date_mise_en_service' => $value->date_mise_en_service, 
                    'client_id'=> $value->client_id,
                    'boitier_id'=> $value->boitier_id,
                    'abonnement_id'=> $value->abonnement_id,
                    'type'=> $value->type,
                    'datedebut' => $value->datedebut,
                    'datefin' => $value->datefin,

                ];
            }
 
            // if(!empty($arra)){
            //     DB::table('contacts')
            //     ->where('constructeur=> $value->constructeur')
            //     ->update($arra);
            // }
            // if(!empty($arra)){
            //          DB::table('contacts')
            //           ->where('id', 3)
            //           ->update(['constructeur' => $value->constructeur,]);
            //    }
            if(!empty($arra)){
                foreach ($import_data as $key => $value) {
                $massee = Contact::updateOrCreate(

                    ['numero_serie' =>  $value->numero_serie],
                    ['constructeur' => $value->constructeur,
                    'famille' => $value->famille,
                    'sous_famille' => $value->sous_famille,
                    'segment' => $value->segment,
                    'modele' => $value->modele,
                    'pays' => $value->pays,
                    'date_mise_en_service' => $value->date_mise_en_service, 
                    'client_id'=> $value->client_id,
                    'boitier_id'=> $value->boitier_id,
                    'abonnement_id'=> $value->abonnement_id,
                    'type'=> $value->type,
                    'datedebut' => $value->datedebut,
                    'datefin' => $value->datefin,
                    ]

                );
                }                               
            // }
            // if(!empty($arra)){
            //     Contact::insert($arra);
            // }
        }
        Session()->flash('flash_message', 'Update successfully');

     return redirect()->route('contacts.index');
    }

     }
    } 
//  if(!empty($arra)){
//     DB::table('contacts')
//      ->where('id', 3)
//      ->update(['constructeur' => $value->constructeur,]);
// }
