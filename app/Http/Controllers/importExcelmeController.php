<?
namespace App\Http\Controllers;
use App\Models\Abonnement;
use App\Http\Requests\Contact\StoreContactRequest;
use App\Http\Requests\Contact\UpdateContactRequest;
use App\Models\Contact;
use App\Models\Boitier;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Boitier\BoitierRepositoryContract;
use App\Repositories\Abonnement\AbonnementRepositoryContract;
use App\Repositories\Contact\ContactRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Excel;
class importExcelmeController extends Controllers
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function masseEquipement()
     {
         return view('contacts.masseequipement');
     }


    public function getExport(Request $request)
    {
    $request_data = $request->all();
    $data = json_decode($request_data['download_data'], true);
    Excel::create('excel name', function($excel)use ($data){
        $excel->sheet('sheet name', function ($sheet)use ($data){
            $sheet->formarray($data);
            $sheet->row(1 ,array(
                'constructeur',
                'famille',
                'numero_serie',
                'sous_famille',
                 'segment',
                 'modele',
                 'date_mise_en_service',
                 'pays' ,
                  'abonnement_id'
                  ,'boitier_id' 
            ));
        });
    })->export('xls');
}



     public function importExcel(Request $request)
     {
         if ($request->file('import_file'))
         {
             $this->validate($request,
             [
            'import_file' => 'mimes:csv,xls,xlsx',
            ]);
                $err_msg_array= array();
                $path = $request->file('import_file')->getRealPath();

                $data = Excel::load($path, function($reader){
                    $reader->select(array('constructeur','famille','numero_serie','sous_famille', 'segment' ,'modele','date_mise_en_service','pays' , 'abonnement_id','boitier_id' ,'client_id'))->get();
                })->get();

                $data_filter= array_filter($data->toArray());


                if(!empty($data_filter && sizeof($data_filter)))
                {
                    $export_error_data= $data_filter;
                    $err_msg_array = [];
                    $datacount = sizeof($export_error_data);
                    $contat_data= importExcel::all()->toArray();
                    for($i =0; $i<$datacount; $i++)
                    {
                        if(isset($export_error_data[$i]['numero_serie']))
                        {
                            foreach ($contat_data as $key => $value) 
                            {
                                if($value['numero_serie']== $export_error_data['numero_serie']){
                                $arr_msg_array[$i]= 'This serie number is already exist';

                            }
                            else
                            {
                            $first_numero_serie= $export_error_data[$i]['numero_serie'];
                            }
                        }
                        }
                     else {
                        $first_numero_serie='';
                        }
                         $rules= [
                            'constructeur'                => 'required',
                            'famille'                     => 'required',
                            'numero_serie'                => 'required|unique:contacts',
                            'sous_famille'                => '',
                            'segment'                     => '',
                            'modele'                      => 'required',
                            'date_mise_en_service'        => 'required',
                            'pays'                        => '',
                            'boitier_id'                  => '',
                            'abonnement_id'               => '',
                            'client_id'               => '',
                 
                         ];
                         $v = validator::make($export_error_data[$i],$rules);
                         $messages= $v->messages();
                         foreach ($message->all() as $message) {
                            $err_msg_array[$i][]= $messages;
                        }
                        $errormessages=[];
                        $errorrow= [];
                        foreach ($err_msg_array as $key => $value) 
                        {
                            for($k=0; $k<count(value); $k++)
                            {
                                $errorList= $value[$k]. 'in row' . ($key+2). "\r\n";
                                array_push($errormessages, $errorList);
                                array_push($errorrow, $key+2);
                            }
                        }
                        if(empty($err_msg_array[$i])){
                            $dataImported = $export_error_data[$i];

                            unset($export_error_data[$i]);

                            ImportExcel::insert($dataImported);
                        }
                        if(empty($errormessages)){
                            return redirect()->route('contacts')
                            ->with('message', 'Data added successfully')
                            ->with('message_type','succces');
                        }
                        else{
                            // remove space and convert into array
                            $err_list = preg("/\r|\n/", "", $get_html);

                            //explode data with comma saprate

                            $erroes = explode('', $error_List);

                            //convert array in json format
                            $download_data= json_encode($export_error_data);
                            $get_html = view('import_excel.error_list' ,compact('errors','download_data'))->render();
                            
                            $error_html_list = preg_replace("/\r|\n/","", $get_html);
                            return redirect()->back()->withInput()
                            ->with('get_html',$error_html_list)
                            ->with('download_data', $download_data);
                        }
                        
                    }
                }
        }
  
  
        //  if($data->count()){
        //      foreach ($data as $key => $value) {
        //          $arra[] = [
        //              'constructeur' => $value->constructeur,
        //              'famille' => $value->famille,
        //              'numero_serie' => $value->numero_serie, 
        //              'sous_famille' => $value->sous_famille,
        //              'segment' => $value->segment,
        //              'modele' => $value->modele,
        //              'pays' => $value->pays,
        //              'date_mise_en_service' => $value->date_mise_en_service, 
        //              'client_id'=> $value->client_id,
        //              'boitier_id'=> $value->boitier_id,
        //              'abonnement_id'=> $value->abonnement_id,
                   
        //          ];
        //      }
  
        //      if(!empty($arra)){
        //         foreach($data as $key => $value)
        //             {
        //          $massee = Contact::updateOrCreate(
        //             ['numero_serie' =>  $value->numero_serie],
        //             ['constructeur' => $value->constructeur,
        //             'famille' => $value->famille,
        //             'sous_famille' => $value->sous_famille,
        //             'segment' => $value->segment,
        //             'modele' => $value->modele,
        //             'pays' => $value->pays,
        //             'date_mise_en_service' => $value->date_mise_en_service, 
        //             'client_id'=> $value->client_id,
        //             'boitier_id'=> $value->boitier_id,
        //             'abonnement_id'=> $value->abonnement_id]
                    
        //         );
        //         }
        //      }
        //  }
  
        //  return back()->with('success', 'Insert Record successfully.');
     }

}
