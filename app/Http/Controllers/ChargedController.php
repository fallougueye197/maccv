<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charged;
use DB;
use App\Http\Requests;
use Session;
// session_start();
class ChargedController extends Controller
{
    public $Charged;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $chargeds = Charged::all();

        return view('chargeds.index', ['chargeds' => Charged::where('nom', '!=', 'pas')->where('nom', '!=', 'No')->get()]);  
        
        //->with('chargeds', $chargeds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chargeds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom'          => 'required',
        ]);

        $chargeds = new Charged;
        $chargeds->nom = $request->input('nom');
        $chargeds->save();

        return redirect('/chargeds'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chargeds = Charged::find($id);
        return view('chargeds.edit')->with('chargeds', $chargeds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom'       => 'required',
        ]);

        $chargeds = Charged::find($id);
        $chargeds->nom = $request->input('nom');
        $chargeds->save();

        return redirect('/chargeds'); 
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chargeds = Charged::find($id);
        $chargeds ->delete();
        return redirect('/chargeds');
    }

   

}
