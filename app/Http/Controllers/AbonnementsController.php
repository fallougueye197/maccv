<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Abonnement;
use App\Exports\AbonnementExport;
use App\Imports\AbonnementImport;
use App\Http\Requests\Abonnement\StoreAbonnementRequest;
use App\Http\Requests\Abonnement\UpdateAbonnementRequest;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Abonnement\AbonnementRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Session;
use  Maatwebsite\Excel\Facades\Excel;


session_start();

class AbonnementsController extends Controller
{
    protected $users;
    protected $abonnements;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        AbonnementRepositoryContract $abonnements,
        SettingRepositoryContract $settings
    ) {
        $this->users    = $users;
        $this->abonnements  = $abonnements;
        $this->settings = $settings;
        $this->middleware('abonnement.read', ['only' => ['read']]);
        $this->middleware('abonnement.create', ['only' => ['create']]);
        $this->middleware('abonnement.update', ['only' => ['edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abonnements = Abonnement::all();
        return view('abonnements.index')->with('abonnements', $abonnements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('abonnements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom'          => 'required',
            'type'          => 'required',
            'interface1' => 'required',
            'interface2' => '',
            'interface3' => '',
            'tarif' => 'required | numeric:abonnements'
             ]);

        $abonnement = new Abonnement;
        $abonnement->nom = $request->input('nom');
        $abonnement->type = $request->input('type');
        $abonnement->interface1 = $request->input('interface1');
        $abonnement->interface2 = $request->input('interface2');
        $abonnement->interface3 = $request->input('interface3');
        $abonnement->tarif = $request->input('tarif');

        

        $abonnement->save();

        return redirect('/abonnements'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $boitier = Abonnement::find($id);
        // return view('boitiers.show')->with('boitier', $boitier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $abonnement = Abonnement::find($id);
        return view('abonnements.edit')->with('abonnement', $abonnement);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 

        $this->validate($request, [
            'nom'          => 'required',
            'type'          => 'required',
            'interface1' => 'required',
            'interface2' => '',
            'interface3' => '',
            'tarif' => 'required'





        ]);
        $abonnement = Abonnement::find($id);
        $abonnement->nom = $request->input('nom');
        $abonnement->type = $request->input('type');
        $abonnement->interface1 = $request->input('interface1');
        $abonnement->interface2 = $request->input('interface2');
        $abonnement->interface3 = $request->input('interface3');
        $abonnement->tarif = $request->input('tarif');
        $abonnement->save();

        return redirect('/abonnements');  
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $abonnements = Abonnement::find($id);
        $abonnements ->delete();
        return redirect('/abonnements');
    }
    public function unactive_abonnement($id)
    {
          DB::table('abonnements')
               ->where('id',$id)
               ->update(['statut' => 0]);
           Session::put('message','Abonnement Unactive successfully !! ');
           return redirect('/abonnements');
        }
 
    public function active_abonnement($id)
    {
          DB::table('abonnements')
               ->where('id',$id)
               ->update(['statut' => 1]);
           Session::put('message','Abonnement Active successfully !! ');
           return redirect('/abonnements');
        }
        public function exporte()
        {
           return Excel::download(new AbonnementExport(), 'demos.xlsx');
        }
}
