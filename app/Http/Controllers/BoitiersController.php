<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Boitier;
use DB;
use App\Http\Requests;
use App\Http\Requests\Boitier\StoreBoitierRequest;
use App\Http\Requests\Boitier\UpdateBoitierRequest;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Boitier\BoitierRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Session;
// session_start();
class BoitiersController extends Controller
{



    protected $users;
    protected $boitiers;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        BoitierRepositoryContract $boitiers,
        SettingRepositoryContract $settings
    ) {
        $this->users    = $users;
        $this->boitiers  = $boitiers;
        $this->settings = $settings;
        $this->middleware('boitier.read', ['only' => ['read']]);
        $this->middleware('boitier.create', ['only' => ['create']]);
        $this->middleware('boitier.update', ['only' => ['edit']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boitiers = Boitier::all();
        return view('boitiers.index')->with('boitiers', $boitiers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('boitiers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reference'          => 'required|unique:boitiers',
            'communication_type' => 'required'
        ]);

        $boitier = new Boitier;
        $boitier->reference = $request->input('reference');
        $boitier->communication_type = $request->input('communication_type');
        $boitier->save();

        return redirect('/boitiers'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $boitier = Boitier::find($id);
        return view('boitiers.show')->with('boitier', $boitier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boitier = Boitier::find($id);
        return view('boitiers.edit')->with('boitier', $boitier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'reference' => 'unique:boitiers,reference,' . $id,

            'communication_type' => 'required'
        ]);

        $boitier = Boitier::find($id);
        $boitier->reference = $request->input('reference');
        $boitier->communication_type = $request->input('communication_type');
        $boitier->save();

        return redirect('/boitiers'); 
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boitiers = Boitier::find($id);
        $boitiers ->delete();
        return redirect('/boitiers');
    }

    public function unactive_boitier($id)
    {
          DB::table('boitiers')
               ->where('id',$id)
               ->update(['statut' => 0]);
           Session::put('message','boitier Unactive successfully !! ');
           return redirect('/boitiers');
        }
 
    public function active_boitier($id)
    {
          DB::table('boitiers')
               ->where('id',$id)
               ->update(['statut' => 1]);
           Session::put('message','Boitier Active successfully !! ');
           return redirect('/boitiers');
        }

}
