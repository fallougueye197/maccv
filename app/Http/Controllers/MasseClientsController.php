<?php
 
namespace App\Http\Controllers;
 
use App\Models\Client;
use DB;
use Excel;
use Illuminate\Http\Request;
use Validator;

 
class MasseClientsController extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function masseClient()
    {
        return view('clients.masseclient');
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcelClients($type)
    {
        $data = Client::get()->toArray();
            
        return Excel::create('Tous les clients', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {
        // $request->validate([
        //    'import_file' => 'required'
        //  ]);
        if ($request->file('import_file'))
        {
        $this->validate($request,
        [
        'import_file' => 'mimes:csv,xls,xlsx',
        ]);
        $err_msg_array= array();
        $path = $request->file('import_file')->getRealPath();
        $import_data = Excel::load($path, function($reader){
        $reader->select(array('name','pays','business','primary_email', 'numidint','numidext','numidext2','user_id' ))->get();
        })->get();
        $import_data_filter= array_filter($import_data->toArray());
        if(!empty($import_data_filter && sizeof($import_data_filter)))
        {
        $export_error_data= $import_data_filter;
        $err_msg_array = [];
        $datacount = sizeof($export_error_data);
        $client_data= Client::all()->toArray();
        for($i =0; $i<$datacount; $i++){
        if(isset($export_error_data[$i]['primary_email']))
        {
        foreach ($client_data as $key => $value)
        {
        if($value['primary_email'] == $export_error_data[$i]['primary_email']){
        $arr_msg_array[$i][]= 'This serial number is already exist';
        }
        else
        {
        $first_numero_serie= $export_error_data[$i]['primary_email'];
        }
        }
        }
        else {
        $first_numero_serie='';
        }
        $rules= 
                [
                    'name'              => 'required',
                    'pays'              => 'required',
                    'business'          => 'required',
                    'primary_email'     => 'required|unique:clients',
                    'numidint'          => 'required|unique:clients',
                    'numidext'        =>    'required|unique:clients,numidext,',
                    'numidext2'        =>   'required',
                    'user_id'           =>  'required',
                ];
        
                $v = validator::make($export_error_data[$i],$rules);
                
                $messages= $v->messages();
                foreach ($messages->all() as $message) {
                $err_msg_array[$i][] = $message;
                }
                $errormessage =[];
                $errorrow = [];
                foreach ($err_msg_array as $key => $value)
                {
                for($k=0; $k<count($value); $k++)
                {
                $errorlist= $value[$k]. 'in row' . ($key+2). "\r\n";
                array_push($errormessage, $errorlist);
                array_push($errorrow, $key+2);
                }
                }
                if(empty($err_msg_array[$i])){
                $dataImported = $export_error_data[$i];
                unset($export_error_data[$i]);
                Client::insert($dataImported);
            }
        }

            if(empty($errormessage)){
            Session()->flash('flash_message', 'Insert Record successfully');

            return redirect()->route('clients.index');
            }
            else{
            // remove space and convert into array
            $error_list = preg_replace("/\r|\n/", "", htmlentities(implode(" , ",$errormessage)));
            
            //explode data with comma saprate
            $errors = explode(',', $error_list);
            //convert array in json format
            $download_data = json_encode($export_error_data);
            $get_html = view('contacts.error',compact('errors','download_data'))->render();
            $error_html_list = preg_replace("/\r|\n/","", $get_html);
            
            return back()->with('success', $error_list);
            // ->withInput()
            // ->with('get_html',$error_html_list)
            // ->with('download_data', $download_data);
        } 
        
        
        // $path = $request->file('import_file')->getRealPath();
        // $import_data = Excel::load($path)->get();
 
        // if($import_data->count()){
        //     foreach ($import_data as $key => $value) {
        //         $arra[] = [
        //             'constructeur' => $value->constructeur,
        //             'famille' => $value->famille,
        //             'numero_serie' => $value->numero_serie, 
        //             'sous_famille' => $value->sous_famille,
        //             'segment' => $value->segment,
        //             'modele' => $value->modele,
        //             'pays' => $value->pays,
        //             'date_mise_en_service' => $value->date_mise_en_service, 
        //             'client_id'=> $value->client_id,
        //             'boitier_id'=> $value->boitier_id,
        //             'abonnement_id'=> $value->abonnement_id,
                  
        //         ];
        //     }
 
//             // if(!empty($arra)){
//             //     DB::table('contacts')
//             //     ->where('constructeur=> $value->constructeur')
//             //     ->update($arra);
//             // }
//             // if(!empty($arra)){
//             //          DB::table('contacts')
//             //           ->where('id', 3)
//             //           ->update(['constructeur' => $value->constructeur,]);
//             //    }
            // if(!empty($arra)){
            //     foreach ($import_data as $key => $value) {
            //     $massee = Contact::updateOrCreate(

            //         ['numero_serie' =>  $value->numero_serie],
            //         ['constructeur' => $value->constructeur,
            //         'famille' => $value->famille,
            //         'sous_famille' => $value->sous_famille,
            //         'segment' => $value->segment,
            //         'modele' => $value->modele,
            //         'pays' => $value->pays,
            //         'date_mise_en_service' => $value->date_mise_en_service, 
            //         'client_id'=> $value->client_id,
            //         'boitier_id'=> $value->boitier_id,
            //         'abonnement_id'=> $value->abonnement_id]
                    
            //     );
            //     }
                
                                
            // }
            // if(!empty($arra)){
            //     Contact::insert($arra);
            // }
                
        }
 
        // return back()->with('failure', 'Insert Record successfully.');
    }
  
     }} 
//  if(!empty($arra)){
//     DB::table('contacts')
//      ->where('id', 3)
//      ->update(['constructeur' => $value->constructeur,]);
// }

//         $path = $request->file('import_file')->getRealPath();
//         $data = Excel::load($path)->get();
 
//         if($data->count()){
//             foreach ($data as $key => $value) {
//                 $arra[] = [
//                     'pays' => $value->pays,
//                     'business' => $value->business,
//                     'primary_email' => $value->primary_email, 
//                     'numIdInt' => $value->numidint,
//                     'numIdExt_1' => $value->numidext,
//                     'numIdExt_2' => $value->numidext2,
//                     'name' => $value->name,
//                     'user_id' => $value->user_id,                   
                  
//                 ];
//             }
 
//             if(!empty($arra)){
//                 foreach ($data as $key => $value) {
//                 $massee = Client::updateOrCreate(

//                     ['primary_email' => $value->primary_email,
//                     'numIdInt' => $value->numidint,
//                     'numIdExt_1' => $value->numidext,
//                 ],
//                     [ 'pays' => $value->pays,
//                     'business' => $value->business,
                    
//                     'numIdExt_2' => $value->numidext2,
//                     'name' => $value->name,
//                     'user_id' => $value->user_id]
                    
//                 );
//                 }
                
                                
//             }
                
//         }
 
//         return back()->with('success', 'Insert Record successfully.');
//     }
// }