<?php

namespace App\Http\Controllers;

use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Contact\ContactRepositoryContract;

class PagesController extends Controller
{
    protected $users;
    protected $clients;
    protected $contacts;
    protected $settings;
    protected $tasks;
    protected $leads;

    public function __construct(
        UserRepositoryContract $users,
        ClientRepositoryContract $clients,
        ContactRepositoryContract $contacts,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads
    ) {
        $this->users    = $users;
        $this->clients  = $clients;
        $this->contacts  = $contacts;
        $this->settings = $settings;
        $this->tasks    = $tasks;
        $this->leads    = $leads;
    }

    /**
     * Dashobard view.
     *
     * @return mixed
     */
    public function dashboard()
    {
        /**
         * Other Statistics.
         */
        $companyname    = $this->settings->getCompanyName();
        $users          = $this->users->getAllUsers();
        $totalUsers     = $this->users->getAllUsersCount();
        $totalClients   = $this->clients->getAllClientsCount();
        $myClients      = $this->clients->getMyClientsCount();
        $totalContacts  = $this->contacts->getAllContactsCount();
        $myContacts     = $this->contacts->getMyContactsCount();

        $totalTimeSpent = $this->tasks->totalTimeSpent();
        $totalEC        = $this->contacts->getAllContactsCCount();
        $totalEPLNC     = $this->contacts->getAllContactsPLNCCount();
        $totalEGPLNC   = $this->contacts->getAllContactsGPLNCCount();
        $totalEMPLNC   = $this->contacts->getAllContactsMPLNCCount();
        $PLE641        = $this->contacts->getPLE641Count();
        $PLE631        = $this->contacts->getPLE631Count();
        $PLE601        = $this->contacts->getPLE601Count();
        $PLE641_PL631  = $this->contacts->getPLE641_PL631Count();
        $NPL1             = $this->contacts->getNPL1Count();
        $NPL2            = $this->contacts->getNPL2Count();
        $NPL3            = $this->contacts->getNPL3Count();
        $NPL4            = $this->contacts->getNPL4Count();
        $gmPLE641        = $this->contacts->getgmPLE641Count();
        $gmPLE631        = $this->contacts->getgmPLE631Count();
        $gmPLE601        = $this->contacts->getgmPLE601Count();
        $gmPLE641_PL631  = $this->contacts->getgmPLE641_PL631Count();
        $gmNPL1             = $this->contacts->getgmNPL1Count();
        $gmNPL2            = $this->contacts->getgmNPL2Count();
        $gmNPL3            = $this->contacts->getgmNPL3Count();
        $gmNPL4            = $this->contacts->getgmNPL4Count();
        $Starter         = $this->contacts->getStarterCount();
        $Inform         = $this->contacts->getInformCount();
        $Informs         = $this->contacts->getInformsCount();
        $gmStarter         = $this->contacts->getgmStarterCount();
        $gmInform         = $this->contacts->getgmInformCount();
        $gmInforms         = $this->contacts->getgmInformsCount();
        $NS1               = $this->contacts->getNS1Count();
        $NS2               = $this->contacts->getNS2Count();
        $NS3               = $this->contacts->getNS3Count();
        $NS4               = $this->contacts->getNS4Count();
        $NS5               = $this->contacts->getNS5Count();
        $gmNS1               = $this->contacts->getgmNS1Count();
        $gmNS2               = $this->contacts->getgmNS2Count();
        $gmNS3               = $this->contacts->getgmNS3Count();
        $gmNS4               = $this->contacts->getgmNS4Count();
        $gmNS5               = $this->contacts->getgmNS5Count();


        /**
         * Statistics for all-time tasks.
         */
        $alltasks             = $this->tasks->tasks();
        $allCompletedTasks    = $this->tasks->allCompletedTasks();
        $totalPercentageTasks = $this->tasks->percantageCompleted();

        /**
         * Statistics for today tasks.
         */
        $completedTasksToday = $this->tasks->completedTasksToday();
        $createdTasksToday   = $this->tasks->createdTasksToday();

        /**
         * Statistics for tasks this month.
         */
        $taskCompletedThisMonth = $this->tasks->completedTasksThisMonth();

        /**
         * Statistics for tasks each month(For Charts).
         */
        $createdTasksMonthly   = $this->tasks->createdTasksMothly();
        $completedTasksMonthly = $this->tasks->completedTasksMothly();

        /**
         * Statistics for all-time Leads.
         */
        $allleads             = $this->leads->leads();
        $allCompletedLeads    = $this->leads->allCompletedLeads();
        $totalPercentageLeads = $this->leads->percantageCompleted();
        /**
         * Statistics for today leads.
         */
        $completedLeadsToday = $this->leads->completedLeadsToday();
        $createdLeadsToday   = $this->leads->createdLeadsToday();

        /**
         * Statistics for leads this month.
         */
        $leadCompletedThisMonth = $this->leads->completedLeadsThisMonth();

        /**
         * Statistics for leads each month(For Charts).
         */
        $completedLeadsMonthly = $this->leads->createdLeadsMonthly();
        $createdLeadsMonthly   = $this->leads->completedLeadsMonthly();

        return view('pages.dashboard', compact(
            'completedTasksToday',
            'completedLeadsToday',
            'createdTasksToday',
            'createdLeadsToday',
            'createdTasksMonthly',
            'completedTasksMonthly',
            'completedLeadsMonthly',
            'createdLeadsMonthly',
            'taskCompletedThisMonth',
            'leadCompletedThisMonth',
            'totalTimeSpent',
            'totalClients',
            'totalContacts',
            'users',
            'totalUsers',

            'companyname',
            'alltasks',
            'allCompletedTasks',
            'totalPercentageTasks',
            'allleads',
            'allCompletedLeads',
            'totalPercentageLeads',
            'totalEC',
            'totalEPLNC',
            'totalEGPLNC',
            'totalEMPLNC',
            'myClients',
            'myContacts',
            'PLE641',
            'PLE631',
            'PLE601',
            'PLE641_PL631',
            'gmPLE641',
            'gmPLE631',
            'gmPLE601',
            'gmPLE641_PL631',
            'Starter',
            'Inform',
            'Informs',
            'gmStarter',
            'gmInform',
            'gmInforms',
            'NPL1',
            'NPL2',
            'NPL3',
            'NPL4',
            'gmNPL1',
            'gmNPL2',
            'gmNPL3',
            'gmNPL4',
            'NS1',
            'NS2',
            'NS3',
            'NS4',
            'NS5',
            'gmNS1',
            'gmNS2',
            'gmNS3',
            'gmNS4',
            'gmNS5'
        ));
    }
}
