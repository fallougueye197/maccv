<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pay;
use DB;
use App\Http\Requests;
use Session;
// session_start();
class PaysController extends Controller
{
    public $pay;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $pays = Pay::all();

        return view('pays.index', ['pays' => Pay::all()]);  
        
        //->with('Pays', $pays);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pays.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom'          => 'required',
        ]);

        $pays = new Pay;
        $pays->nom = $request->input('nom');
        $pays->save();

        return redirect('/pays'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pays = Pay::find($id);
        return view('pays.edit')->with('pays', $pays);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom'       => 'required',
        ]);

        $pays = Pay::find($id);
        $pays->nom = $request->input('nom');
        $pays->save();
        
        //$pay = DB::table('pays')->where('nom', $pays->nom)->first();
        
        //DB::table('contacts')->where('pays', $pay->nom)->update(['pays' => $pays->nom]);
       // DB::table('clients')->where('pays', $pay->nom)->update(['pays' => $pays->nom]);
        
        return redirect('/pays'); 
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pays = Pay::find($id);
        $pays ->delete();
        return redirect('/pays');
    }

   

}
