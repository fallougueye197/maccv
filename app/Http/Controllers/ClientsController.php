<?php

namespace App\Http\Controllers;

use App\Http\Requests\Client\StoreClientRequest;
use App\Http\Requests\Client\UpdateClientRequest;
use App\Models\Client;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Pay;
use Excel;



class ClientsController extends Controller
{
    protected $users;
    protected $clients;
    protected $settings;

    public function __construct(
        UserRepositoryContract $users,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings
    ) {
        $this->users    = $users;
        $this->clients  = $clients;
        $this->settings = $settings;
        $this->middleware('client.read', ['only' => ['read']]);
        $this->middleware('client.create', ['only' => ['create']]);
        $this->middleware('client.update', ['only' => ['edit']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('clients.index',['clients' => Client::all()]);
    }

    /**
     * Display a listing of the resource.
     */
    public function my()
    {
        return view('clients.my');
    }

    /**
     * Make json respnse for datatables.
     *
     * @return mixed
     */
    public function anyData()
    {
        $clients = Client::select(['clients.*', DB::raw('users.name AS salesperson')])->join('users', 'clients.user_id', '=', 'users.id');

        // $clients = Client::with('user')->select('clients.*');

        $dt = Datatables::of($clients)
            ->addColumn('namelink', function ($clients) {
                return '<a href="'.route('clients.show', $clients->id).'">'.$clients->name.'</a>';
            })
            ->addColumn('emaillink', function ($clients) {
                return '<a href="mailto:'.$clients->primary_email.'">'.$clients->primary_email.'</a>';
            });

        // this looks wierd, but in order to keep the two buttons on the same line
        // you have to put them both within the form tags if the Delete button is
        // enabled
        $actions = '';
        if (Auth::user()->can('client-delete')) {
            $actions .= '<form action="{{ route(\'clients.destroy\', $id) }}" method="POST">
            ';
        }
        if (Auth::user()->can('client-update')) {
            $actions .= '<a href="{{ route(\'clients.edit\', $id) }}" class="btn btn-xs btn-success" >Edit</a>';
        }
        if (Auth::user()->can('client-delete')) {
            $actions .= '
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-xs" onClick="return confirm(\'Are you sure?\')"">
                {{csrf_field()}}
            </form>';
        }

        return $dt->addColumn('actions', $actions)->make(true);
    }

    /**
     * Make json respnse for datatables.
     *
     * @return mixed
     */
    public function myData()
    {
        $clients = Client::with('user')->select('clients.*')->my();

        $dt = Datatables::of($clients)
            ->addColumn('namelink', function ($clients) {
                return '<a href="'.route('clients.show', $clients->id).'">'.$clients->name.'</a>';
            })
            ->addColumn('emaillink', function ($clients) {
                return '<a href="mailto:'.$clients->primary_email.'">'.$clients->primary_email.'</a>';
            });

        // this looks wierd, but in order to keep the two buttons on the same line
        // you have to put them both within the form tags if the Delete button is
        // enabled
        $actions = '';
        if (Auth::user()->can('client-delete')) {
            $actions .= '<form action="{{ route(\'clients.destroy\', $id) }}" method="POST">
            ';
        }
        if (Auth::user()->can('client-update')) {
            $actions .= '<a href="{{ route(\'clients.edit\', $id) }}" class="btn btn-xs btn-success" >Edit</a>';
        }
        if (Auth::user()->can('client-delete')) {
            $actions .= '
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-xs" onClick="return confirm(\'Are you sure?\')"">
                {{csrf_field()}}
            </form>';
        }

        return $dt->addColumn('actions', $actions)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $pays = \App\Pay::pluck('nom', 'nom');
        return view('clients.create')
            ->withUsers($this->users->getAllUsersWithDepartments())->with ('pays',$pays);
            
            // ->withPays($this->clients->listAllPays());
    }  

    /**
     * @param StoreClientRequest $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required',
            'business'          => 'required',
            'primary_email'     => 'required|unique:clients',
            'numidint'          => 'required|unique:clients',
            'numidext'        => 'required|unique:clients,numidext,',
            'numidext2'        => 'required',
            'pay_id'        => '',
           /*  'user_id'           => 'required', */
        ]);
       /*  $this->clients->create($request->all()); */
       $client = new Client;
       $client->name = $request->get('name');
       //$client->pays = $request->get('pays');
       $client->business = $request->get('business');
       $client->primary_email = $request->get('primary_email');
       $client->numidint = $request->get('numidint');
       $client->numidext = $request->get('numidext');
       $client->numidext2 = $request->get('numidext2');
       $client->pay_id = $request->get('pay_id');
       $client->save();
       
        return redirect()->route('clients.index');
    }

    /**
     * @param Request $vatRequest
     *
     * @return mixed
     */
    public function cvrapiStart(Request $vatRequest)
    {
        return redirect()->back()
            ->with('data', $this->clients->vat($vatRequest));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function show($id)
    {
        return view('clients.show')
            ->withClient($this->clients->find($id))
            ->withInvoices($this->clients->getInvoices($id))
            ->withUsers($this->users->getAllUsersWithDepartments());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        $pays = \App\Pay::pluck('nom', 'nom');
        $client = Client::find($id);
        return view('clients.edit', compact('pays','client'));
            //->withClient($this->clients->find($id))
            //->withUsers($this->users->getAllUsersWithDepartments())
            //->withIndustries($this->clients->listAllIndustries())->with ('pays',$pays);
            // ->withPays($this->clients->listAllPays());

    }

    /**
     * @param $id
     * @param UpdateClientRequest $request
     *
     * @return mixed
     */
    public function update($id, Request $request)
    {

        $this->validate($request, [
            'name'              => 'required',
            //'pays'              => 'required',
            'business'          => 'required',
            'primary_email'     => 'required|unique:clients,primary_email,' . $id,
            'numidint'          => 'required|unique:clients,numidint,' . $id,
            'numidext'          => 'required|unique:clients,numidext,' . $id,
            'numidext2'          => 'required',
            'user_id'           => '',
            'pay_id'            => '',

        ]);
        //$this->clients->update($id, $request);
        
        
       $client = Client::find($id);
       $client->name = $request->get('name');
       $client->pays = $request->get('pays');
       $client->business = $request->get('business');
       $client->primary_email = $request->get('primary_email');
       $client->numidint = $request->get('numidint');
       $client->numidext = $request->get('numidext');
       $client->numidext2 = $request->get('numidext2');
       $client->pay_id = $request->get('pay_id');
       $client->update();

        Session()->flash('flash_message', 'Client successfully updated');

        return redirect()->route('clients.index');
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        $this->clients->destroy($id);

        return redirect()->route('clients.index');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return mixed
     */
    public function updateAssign($id, Request $request)
    {
        $this->clients->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');

        return redirect()->back();
    }
   





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function masseUpdate()
     {
         return view('clients.masseedit');
     }
     public function updateMasse(Request $request)
         {
            $this->validate($request,
            [
            'import_file' => 'mimes:csv,xls,xlsx',
            ]);
         $path = $request->file('import_file')->getRealPath();
         $import_data = Excel::load($path)->get();
         if($import_data->count()){
             foreach ($import_data as $key => $value) {
                 $arra[] = [
                     
                    'name'              => $value->name,
                    'pays'              => $value->pays,
                    'business'          => $value->business,
                    'primary_email'     => $value->primary_email,
                    'numidint'          => $value->numidint,
                    'numidext'        => $value->numidext1,
                    'numidext2'        => $value->numidext2,
                    'user_id'           =>$value->user_id,
                   
                 ];
             }
  
             
             if(!empty($arra)){
                 foreach ($import_data as $key => $value) {
                 $massee = Client::updateOrCreate(
 
                     ['numidint'          => $value->numidint],
                     [
                        'name'              => $value->name,
                        'pays'              => $value->pays,
                        'business'          => $value->business,
                        'primary_email'     => $value->primary_email,
                        'numidext'        => $value->numidext,
                        'numidext2'        => $value->numidext2,
                        'user_id'           =>$value->user_id,
                     
                 ]);
                 }                               
         }
         Session()->flash('flash_message', 'Update successfully');
 
      return redirect()->route('clients.index');
     }
 
      }
}

