<?php 
namespace App\Http\Controllers;
use App\Models\Abonnement;
use App\Models\Contact;
use App\Models\Boitier;
use App\Charged;
//use Datatables;
use App\AboEquipement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Excel;

class V2Controller extends Controller   
{    // 
    
    
    public function filter(Request $request){
        $contact = Contact::all();
        $filter = $request->get('filter');
        $filters = $request->get('filters');
        $filt = $request->get('filt');
        $filte = $request->get('filte');
        $abos = DB::table('abo_equipements')->get();
        
        $contactfgs =  DB::table('contacts')->select('contacts.*','abo_equipements.abonnement_id')
        ->join('abo_equipements','abo_equipements.contact_id', 'contacts.id')
        ->where('pay_id', 'like', '%'.$filter.'%')
        ->where('type', 'like', '%'.$filters.'%')
        ->where('boitier_id', 'like', '%'.$filt.'%')
        ->where('abo_equipements.abonnement_id', 'like', '%'.$filte.'%')
        //->distinct('id')
        ->get();
        
        $contacts = $contactfgs->unique('id'); $contacts->values()->all();
       //$collection = collect($contacts);

       //$unique_data = $collection->unique()->values()->all(); 

       //dd($contacts);
         return view('contacts.index', compact('contacts','contact'));
         
      }

      public function filterfac(Request $request){
       
       $filterfacp = $request->get('filterfacp');
       $filterfacc = $request->get('filterfacc');
       $today = date('Y-m-d');
       $charged = Charged::where('nom', '!=', 'No')->where('nom', '!=', 'pas')->get();
       
       
       
        $chargeds =  DB::table('chargeds')
        ->select('chargeds.id','abo_equipements.chargerto','chargeds.nom','abo_equipements.contact_id','abo_equipements.abonnement_id','abonnements.tarif','abo_equipements.pays_id','abo_equipements.end_statut',
        'abo_equipements.date_debut','abo_equipements.date_fin')
        ->join('abo_equipements','abo_equipements.charged_id', '=', 'chargeds.id')
        ->join('abonnements','abonnements.id', '=', 'abo_equipements.abonnement_id')
        ->where('abo_equipements.pays_id', 'like', '%'.$filterfacp.'%')
         ->where('abo_equipements.chargerto', 'like', '%'.$filterfacc.'%')
          ->where('abo_equipements.end_statut', '=', 0)
        ->where('abo_equipements.date_debut', '<=', $today)
        ->where('abo_equipements.date_fin', '>=', $today)
        ->get();
        $tarif = 0;
        foreach($chargeds as $charge)
        {
           $tarif += $charge->tarif ;
        }
     // dd($chargeds);
        
     // $chargeds = array();
      
         return view('contacts.facturation', compact('chargeds','charged','tarif'));
         
      }

      /**
     * Display a listing of the resource.
     */
    public function facturation()
    {
         $today = date('Y-m-d');
        $contactfg = Charged::all();
        foreach($contactfg as $contactf)
        {
            DB::table('abo_equipements')->where('chargerto', $contactf->nom)->update(array('charged_id' => $contactf->id));
        }
        
        $tarif = 0;

        $charged = Charged::all();
        
        $chargeds =  DB::table('chargeds')
        ->select('chargeds.id','abo_equipements.chargerto','abo_equipements.abonnement_id','abonnements.tarif','chargeds.nom','abo_equipements.contact_id','abo_equipements.pays_id',
        'abo_equipements.end_statut','abo_equipements.date_debut','abo_equipements.date_fin')
        ->join('abo_equipements','abo_equipements.charged_id', '=', 'chargeds.id')
         ->join('abonnements','abonnements.id', '=', 'abo_equipements.abonnement_id')
          ->where('abo_equipements.end_statut', '=', 0)
        ->where('abo_equipements.date_debut', '<=', $today)
        ->where('abo_equipements.date_fin', '>=', $today)
        ->get();
        foreach($chargeds as $charge)
        {
           $tarif += $charge->tarif ;
        }
        return view('contacts.facturation', compact('chargeds','charged','tarif'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function valider(Request $request, $id)
    {
      //
        $abonnement = "Abonnement valider";
        $abonnement = AboEquipement::find($id);
        $abonnement->end_statut = 1; //Approved
        $abonnement->save();
        Session()->flash('flash_message', 'equipment successfully expired');
        return redirect()->back()->with(['abonnement' => $abonnement]);
    }    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function refuser(Request $request, $id)
    {
      //
      $abonnement = "Abonnement valider";
      $abonnement = AboEquipement::find($id);
      $abonnement->end_statut = 0; //Approved
      $abonnement->save();
      Session()->flash('flash_message', 'equipment successfully expired');
      return redirect()->back()->with(['abonnement' => $abonnement]);
    }    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {        return view('contacts.ajouter');
    }    
    /**
     * @param StoreContactRequest $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {       
            $message = "Ajouté avec succès";     
            $date = date('y-m-d h:i:s');       
            $contact = new Contact;
            $contact->constructeur = $request->get('constructeur');
            $contact->famille = $request->get('famille');
            $contact->numero_serie = $request->get('numero_serie');
            $contact->sous_famille = $request->get('sous_famille');
            $contact->modele = $request->get('modele');
            $contact->date_mise_en_service = $request->get('date_mise_en_service');
            $contact->boitier_id = $request->get('boitier_id');
            $contact->type = $request->get('type');
            $contact->pays = $request->get('pays');
            $contact->client_id = $request->get('client_id');
            $contact->pay_id = $request->get('pay_id');
            $contact->save();
            
                $chargerto = $request->chargerto;
                
                $date_debut = $request->date_debut;
                $date_fin = $request->date_fin;
                $abonnement_id = $request->abonnement_id;
                for($i=0; $i < count($chargerto); $i++){            
                $abo_equipements = [
                    'chargerto' => $chargerto[$i],
                    'date_debut' => $date_debut[$i],
                    'date_fin' => $date_fin[$i],
                    'abonnement_id' => $abonnement_id[$i],
                    'contact_id' => $contact->id,
                    'client_id' => $contact->client_id,
                    'created_at' => $date,
                    'updated_at' => $contact->updated_at
                    ];

            
            DB::table('abo_equipements')->insert($abo_equipements);
            }
        
            Session()->flash('flash_message', 'equipment successfully created');
        return redirect()->back();
    } 


    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function edit($id)
    {  
        $contact = Contact::find($id);
        return view('contacts.editer', compact('contact'));
    }    
    
    /**
     * @param StoreContactRequest $request
     *
     * @return mixed
     */
    public function update(Request $request,$id)
    {       
            $message = "Ajouté avec succès";   
            $date = date('y-m-d h:i:s');           
            $contact = Contact::find($id);
            $contact->constructeur = $request->get('constructeur');
            $contact->famille = $request->get('famille');
            $contact->numero_serie = $request->get('numero_serie');
            $contact->sous_famille = $request->get('sous_famille');
            $contact->modele = $request->get('modele');
            $contact->date_mise_en_service = $request->get('date_mise_en_service');
            $contact->boitier_id = $request->get('boitier_id');
            $contact->type = $request->get('type');
            $contact->pays = $request->get('pays');
            $contact->client_id = $request->get('client_id');
            $contact->pay_id = $request->get('pay_id');
            $contact->update();
           
            $chargerto = $request->chargerto;
            $date_debut = $request->date_debut;
            $date_fin = $request->date_fin;
            $abonnement_id = $request->abonnement_id;
            for($i=0; $i < count($chargerto); $i++){            
            $abo_equipements = [
                'chargerto' => $chargerto[$i],
                'date_debut' => $date_debut[$i],
                'date_fin' => $date_fin[$i],
                'abonnement_id' => $abonnement_id[$i],
                'contact_id' => $contact->id,
                'client_id' => $contact->client_id,
                'created_at' => $date,
                'updated_at' => $contact->updated_at
                ];

                DB::table('abo_equipements')->where('contact_id', $contact->id)->where('created_at','!=', $date)->delete();

                DB::table('abo_equipements')->insert($abo_equipements);
/*             DB::table('abo_equipements')->where('contact_id', $contact->id)->where('updated_at', '!=', $contact->updated_at)->delete();
 */
            }
        //}
            Session()->flash('flash_message', 'equipment successfully updated');
        return redirect('/contacts');
    }

    public function destroyabo($id)
    {  
        $abo = AboEquipement::find($id);
        $abo->delete();

        Session()->flash('flash_message', 'equipment successfully deleted');
        return redirect()->back();
    }    

   
}