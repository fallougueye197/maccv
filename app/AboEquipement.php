<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboEquipement extends Model
{
    protected $fillable =
        [

            'nom',
            'date_debut',
            'date_fin',
            'contact_id',
            'abonnement_id',
            'end_statut',
            
        ];
}
