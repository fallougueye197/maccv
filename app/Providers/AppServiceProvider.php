<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use App\Repositories\Abonnement\AbonnementRepository;
use App\Repositories\Abonnement\AbonnementRepositoryContract;
use App\Repositories\Boitier\BoitierRepository;
use App\Repositories\Boitier\BoitierRepositoryContract;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->bind(
            AbonnementRepositoryContract::class,
            AbonnementRepository::class
        );
        $this->app->bind(
            BoitierRepositoryContract::class,
            BoitierRepository::class
        );
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
            
    }
}
