<?php

namespace App\Repositories\Boitier;

interface BoitierRepositoryContract
{
    public function getAllBoitiers();

    public function find($id);

    public function update($id, $requestData);

    public function listAllBoitiers();

    public function create($requestData);

    public function destroy($id);
}
