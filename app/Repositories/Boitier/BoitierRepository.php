<?php

namespace App\Repositories\Boitier;
use App\Models\Boitier;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BoitierRepository.
 */
class BoitierRepository implements BoitierRepositoryContract
{

    const CREATED        = 'created';
    const UPDATED_ASSIGN = 'updated_assign';



    /**
     * @param $id
     * @param $requestData
     */
     public function update($id, $requestData)
     {
         $boitier = Boitier::findOrFail($id);
         $boitier->fill($requestData->all())->save();
     }

    /**
     * @param $id
     *
     * @return mixed
     */
     public function find($id)
     {
         return Boitier::findOrFail($id);
     }

    /**
     * @return Collection|static[]
     */
    public function getAllBoitiers()
    {
        return Boitier::all();
    }

    /**
     * @return mixed
     */
    public function listAllBoitiers()
    {
        return Boitier::where('statut', 0)->pluck('reference', 'id');
    }

    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        Boitier::create($requestData->all());
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        Boitier::findorFail($id)->delete();
    }


   
}
