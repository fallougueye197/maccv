<?php

namespace App\Repositories\Abonnement;

interface AbonnementRepositoryContract
{
    public function getAllAbonnements();

    public function find($id);

    public function update($id, $requestData);

    public function listAllAbonnements();

    public function create($requestData);

    public function destroy($id);
}
