<?php

namespace App\Repositories\Abonnement;
use App\Models\Abonnement;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AbonnementRepository.
 */
class AbonnementRepository implements AbonnementRepositoryContract
{
    const CREATED        = 'created';
    const UPDATED_ASSIGN = 'updated_assign';

    /**
     * @return Collection|static[]
     */
    public function getAllAbonnements()
    {
        return Abonnement::all();
    }
      /**
     * @param $id
     *
     * @return mixed
     */
     public function find($id)
     {
         return Abonnement::findOrFail($id);
     }

    /**
     * @return mixed
     */
    public function listAllAbonnements()
    {
        return Abonnement::where('statut', 0)->pluck('nom', 'id');
    }

    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        Abonnement::create($requestData->all());
    }

     /**
     * @param $id
     * @param $requestData
     */
     public function update($id, $requestData)
     {
         $abonnement = Abonnement::findOrFail($id);
         $abonnement->fill($requestData->all())->save();
     }

    /**
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        Abonnement::findorFail($id)->delete();
    }


    
}
