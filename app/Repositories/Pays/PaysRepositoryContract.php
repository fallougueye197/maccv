<?php

namespace App\Repositories\Pays;

interface PaysRepositoryContract
{
    public function getAllPays();

    public function listAllPays();

    public function create($requestData);

    public function destroy($id);
}
