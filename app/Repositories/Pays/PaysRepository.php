<?php

namespace App\Repositories\Pays;

use App\Pays;

/**
 * Class PaysRepository.
 */
class PaysRepository implements PaysRepositoryContract
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllPays()
    {
        return Pays::all();
    }

    /**
     * @return mixed
     */
    public function listAllPays()
    {
        return Pays::pluck('nom', 'id');
    }

    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        Pays::create($requestData->all());
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        Pays::findorFail($id)->delete();
    }
}
