<?php

namespace App\Repositories\Contact;

use App\Models\Contact;
use Illuminate\Support\Facades\Auth;


/**
 * Class ContactRepository.
 */
class ContactRepository implements ContactRepositoryContract
{
    const CREATED        = 'created';
    const UPDATED_ASSIGN = 'updated_assign';

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return Contact::findOrFail($id);
    }

    /**
     * @return mixed
     */
    public function listAllContacts()
    {
        return Contact::pluck('constructeur', 'id');
    }

    /**
     * @return int
     */
    public function getAllContactsCount()
    {
        $id = \Auth::user()->id;
        
        return Contact::count();
    }
     public function getMyContactsCount()
    {
        $id = \Auth::user()->id;
        
        return Contact::count();
    }
    public function getPLE641Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 2)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getPLE631Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 3)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getPLE601Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 4)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getPLE641_PL631Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 5)->where('pays','=', Auth::user()->pays)->count();
    }


    public function getNPL1Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 6)->where('pays','=', Auth::user()->pays)->count();
    } 
    public function getNPL2Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 7)->where('pays','=', Auth::user()->pays)->count();
    } 
    public function getNPL3Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 8)->where('pays','=', Auth::user()->pays)->count();
    } 
    public function getNPL4Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', 9)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNPL1Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 6)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNPL2Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 7)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNPL3Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 8)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNPL4Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 9)->where('pays','=', Auth::user()->pays)->count();
    }










     public function getgmPLE641Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 2)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmPLE631Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 3)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmPLE601Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 4)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmPLE641_PL631Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('boitier_id', 5)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getAllContactsCCount()
    {
        return Contact::all()->where('abonnement_id', '!=',1)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getAllContactsPLNCCount()
    {
        return Contact::all()->where('type','=' ,'Machine')->where('boitier_id', '!=',1)->where('abonnement_id', '=',1)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getAllContactsGPLNCCount()
    {
        return Contact::all()->where('type','=' ,'Groupe')->where('boitier_id', '!=',1)->where('abonnement_id', '=',1)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getAllContactsMPLNCCount()
    {
        return Contact::all()->where('type','=' ,'Moteur')->where('boitier_id', '!=',1)->where('abonnement_id', '=',1)->where('pays','=', Auth::user()->pays)->count();
    }











    public function getStarterCount()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 2)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getInformCount()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 3)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getInformsCount()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 4)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmStarterCount()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 2)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmInformCount()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 3)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmInformsCount()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 4)->where('pays','=', Auth::user()->pays)->count();
    }

    



    public function getNS1Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 5)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getNS2Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 6)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getNS3Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 7)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getNS4Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 8)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getNS5Count()
    {        
        return Contact::all()->where('type','=' ,'Machine')->where('abonnement_id', 9)->where('pays','=', Auth::user()->pays)->count();
    }


    public function getgmNS1Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 5)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNS2Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 6)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNS3Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 7)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNS4Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 8)->where('pays','=', Auth::user()->pays)->count();
    }
    public function getgmNS5Count()
    {        
        return Contact::all()->where('type','!=' ,'Machine')->where('abonnement_id', 9)->where('pays','=', Auth::user()->pays)->count();
    }













    
    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        $contact = Contact::create($requestData);
        Session()->flash('flash_message', 'Contact successfully added');
        event(new \App\Events\ContactAction($contact, self::CREATED));
    }

    /**
     * @param $id
     * @param $requestData
     */
    public function update($id, $requestData)
    {
        $contact = Contact::findOrFail($id);
        $contact->fill($requestData->all())->save();
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        try {
            $contact = Contact::findorFail($id);
            $contact->delete();
            Session()->flash('flash_message', 'Contact successfully deleted');
        } catch (\Illuminate\Database\QueryException $e) {
            Session()->flash('flash_message_warning', 'Contact can NOT have, leads, or tasks assigned when deleted');
        }
    }
    
}
