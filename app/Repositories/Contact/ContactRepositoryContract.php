<?php

namespace App\Repositories\Contact;

interface ContactRepositoryContract
{
    public function find($id);

    public function listAllContacts();

    public function getMyContactsCount();
    
    public function getPLE641Count();

    public function getPLE631Count();

    public function getPLE601Count();

    public function getPLE641_PL631Count();

    public function getgmPLE641Count();

    public function getgmPLE631Count();

    public function getgmPLE601Count();

    public function getgmPLE641_PL631Count();

    public function getNPL1Count();

    public function getNPL2Count();

    public function getNPL3Count();

    public function getNPL4Count();

    public function getgmNPL1Count();
    
    public function getgmNPL2Count();
    
    public function getgmNPL3Count();

    public function getgmNPL4Count();


    public function getStarterCount();

    public function getInformCount();

    public function getInformsCount();

    public function getgmStarterCount();

    public function getgmInformCount();

    public function getgmInformsCount();

    public function getNS1Count();

    public function getNS2Count();

    public function getNS3Count();

    public function getNS4Count();

    public function getNS5Count();

    public function getgmNS1Count();

    public function getgmNS2Count();

    public function getgmNS3Count();

    public function getgmNS4Count();

    public function getgmNS5Count();






    public function getAllContactsCount();

    public function getAllContactsCCount();

    public function getAllContactsPLNCCount();

    public function getAllContactsGPLNCCount();

    public function getAllContactsMPLNCCount();

    public function create($requestData);

    public function update($id, $requestData);

    public function destroy($id);
}
