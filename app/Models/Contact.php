<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;
use Carbon;


use Illuminate\Support\Facades\Auth;

class Contact extends Model
{
    protected $table = 'contacts';


    protected $fillable = [


            'constructeur'        ,
            'famille'             ,
            'numero_serie'        ,
            'email'               ,
            'sous_famille'        ,
            'segment'             ,
            'modele'              ,
            'date_mise_en_service',
            'pays' ,
            'compteur' , 
            'name',
            'boitier_id',
            'abonnement_id',
            'job_title',
            'email',
            'address1',
            'address2',
            'city',
            'state',
            'zipcode',
            'country',
            'primary_number',
            'secondary_number',
            'client_id',
            'deadline',
            'type',
            'datedebut',
            'datefin',
    ];
    // protected $dates = ['datefin'];
    // public function getDaysUntilDateEndAttribute()
    // {
    //     return Carbon\Carbon::now()
    //         ->startOfDay()
    //         ->diffInDays($this->datefin, false); // if you are past your deadline, the value returned will be negative.
    // }





    public function abonnements()
    {
        return $this->belongsTo(Abonnement::class,'abonnement_id' );
    }

    public function boitiers()
    {
        return $this->belongsTo(Boitier::class, 'boitier_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function getFormattedAddressAttribute()
    {
        $address = '';

        if ($this->address1 || $this->city || $this->zipcode) {
            if ($this->address1) {
                $address .= htmlspecialchars($this->address1).'<br/>';
            }
            if ($this->address2) {
                $address .= htmlspecialchars($this->address2).'<br/>';
            }
            if ($this->city || $this->state || $this->zipcode) {
                if ($this->city) {
                    $address .= $this->city.'&nbsp;';
                }
                if ($this->state) {
                    $address .= $this->state.'&nbsp;';
                }
                if ($this->zipcode) {
                    $address .= $this->zipcode;
                }
            }
            if ($this->country) {
                $address .= '<br/>'.$this->country;
            }

            return $address;
        } else {
            return null;
        }
    }

  

    public function scopeMy($query)
    {
        return $query->whereHas('client', function ($q) {
            $q->where('user_id', '=', Auth::id());
        });
    }
}
