<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    public function contacts()
    {
        return $this->hasMany(Contact::class,'abonnement_id', 'id');
    }


    protected $fillable =
        [

            'nom',
            'type',
            'interface1',
            'interface2',
            'interface3',
            'tarif',
            'statut',
            
        ];
}
